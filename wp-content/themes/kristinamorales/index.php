<?php
/**
 * Version       : 1.0
 * Author        : Nikita Titov
 *
 **/
?>

<!DOCTYPE html>
<?php get_header(); ?>
<body>

<main class="posts_area content-index">

    <!-- Basics Blocks -->

  <?php get_template_part( '/assets/template-parts/content_blocks' ); ?>

</main>

<?php get_footer(); ?>
</body>
</html>