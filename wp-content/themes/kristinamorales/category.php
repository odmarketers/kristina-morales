<?php
/**
 * A Simple Category Template
 */

get_header(); ?>
  <body>
  <div class="posts_area">
	<section class="breadcrumb-blog" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/contact-us-bg.png);">
	  <div class="lines">
		<div class="container">
		  <div class="row">
			<div class="lines-items lines-items lines-items_white-light">
			  <div class="lines-items__item"></div>
			  <div class="lines-items__item"></div>
			  <div class="lines-items__item"></div>
			  <div class="lines-items__item"></div>
			  <div class="lines-items__item"></div>
			</div>
		  </div>
		</div>
	  </div>
	  <div class="container">
		<div class="breadcrumb-blog__wrapper">
		  <div class="breadcrumb-blog__title-block">
			<div class="breadcrumb-blog__title" data-aos="fade-down" data-aos-delay="800">Category</div>
			<div class="breadcrumb-blog__sub-title">Category page</div>
		  </div>
		  <div class="breadcrumb-blog__nav">
			<div class="breadcrumb-blog__item"><a href="/">home</a></div>
			<div class="breadcrumb-blog__item"> <span>Category</span></div>
		  </div>
		</div>
	  </div>
	</section>

	<div class="container">
	  <?php
	  if (have_posts()) :
		while (have_posts()) : the_post(); ?>
		  <div id="posts" style='font-size: 16px;color: #808080;font-weight: 400;font-family: "Montserrat", sans-serif; line-height: 22px; padding-top: 25px; margin: 10px 0 30px;'>
			<h2><a style="font-size: 20px;" class="list__title" href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
			<div style="margin-top: 10px;" id="post_info">
			  <div>Author: <?php the_author() ?></div>
			  <div>Date Added: <?php the_date() ?></div>
			  <div id="clear"></div>
			</div>
			<p><?php the_excerpt() ?></p>
			<span>Category: <?php the_category(', ') ?></span>
		  </div>

		<?php endwhile; ?>
	  <?php
	  else :
		echo "Sorry for your result: nothing found";
	  endif;
	  ?>
	  </section>
	</div>
  </div>
  </body>
<?php get_footer(); ?>