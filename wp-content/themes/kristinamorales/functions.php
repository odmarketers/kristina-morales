<?php
/**
 * Kristina Morales
 *
 * @package WordPress
 * @subpackage Kristina Morales
 * @since 1.0
 */

if ( ! function_exists( 'setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */


function setup() {

    /**
     * Add default posts and comments RSS feed links to <head>.
     */
    add_theme_support( 'automatic-feed-links' );

    /**
     * Enable support for post thumbnails and featured images.
     */
    add_theme_support( 'post-thumbnails' );

    /**
     * Add support for two custom navigation menus.
     */
    register_nav_menus( array(
        'primary'   => __( 'Primary Menu', 'theme' ),
        'secondary' => __('Secondary Menu', 'theme' )
    ) );

    /**
     * Enable support for the following post formats:
     * aside, gallery, quote, image, and video
     */
    add_theme_support( 'post-formats', array ( 'aside', 'gallery', 'quote', 'image', 'video' ) );
}
endif;

add_action( 'after_setup_theme', 'setup' );


/**
 * Load CSS files
 */
function load_css() {

	wp_enqueue_style( 'stylesheet', get_stylesheet_uri() );
	wp_enqueue_style( 'gfonts', 'https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900&subset=cyrillic,cyrillic-ext,latin-ext');
  	wp_enqueue_style( 'gfonts2', 'https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i&subset=cyrillic,latin-ext');

}

add_action( 'wp_enqueue_scripts', 'load_css' );


/**
 * Load JS files
 */
function load_js() {

    wp_enqueue_script( 'newscript', get_template_directory_uri()."/assets/js/main.min.js");
}

add_action( 'wp_enqueue_scripts', 'load_js' );


/**
 * Load Font Awesome
 */


function font_awesome() {

  if (!is_admin()) {
    wp_register_style('font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css');
    wp_enqueue_style('font-awesome');
  }
}

add_action('wp_enqueue_scripts', 'font_awesome');


/**
 * Create Breadcrumbs Blog
 */
function breadcrumbs_blog() {
  	echo '<div class="breadcrumb-blog__item" href="'.home_url().'" rel="nofollow"><a href="/">Home</a></div>
		<div class="breadcrumb-blog__item"><a href="'.home_url().'/kristina-blog/">Blog</a></div>';
  	if (is_category() || is_single()) {

    echo "<div class='breadcrumb-blog__item'> <span>";
	the_title();
	echo "</span></div>";

  }
}


/**
 * ACF Footer Option page
 */
if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'menu_title'    => 'Theme Settings',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'      => true
    ));

  acf_add_options_page(array(
	'page_title'    => 'Header',
	'menu_title'    => 'Header',
	'parent_slug'   => 'theme-general-settings',
  ));

  acf_add_options_page(array(
	'page_title'    => 'Footer',
	'menu_title'    => 'Footer',
	'parent_slug'   => 'theme-general-settings',
  ));

  acf_add_options_page(array(
	'page_title'    => 'Preset Sections',
	'menu_title'    => 'Preset Sections',
	'parent_slug'   => 'theme-general-settings',
  ));

  acf_add_options_page(array(
	'page_title'    => 'Schema',
	'menu_title'    => 'Schema',
	'parent_slug'   => 'theme-general-settings',
  ));

  acf_add_options_page(array(
	'page_title'    => 'Contact Settings',
	'menu_title'    => 'Contact',
	'parent_slug'   => 'theme-general-settings',
  ));

}


/**
 * ACF Remove <p>
 */
function my_acf_add_local_field_groups() {
  remove_filter('acf_the_content', 'wpautop' );
}

add_action('acf/init', 'my_acf_add_local_field_groups');


function new_excerpt_length($length) {
  return 20;
}
add_filter('excerpt_length', 'new_excerpt_length');



/**
 * SVG
 */
function my_myme_types($mime_types){
  $mime_types['svg'] = 'image/svg+xml';
  return $mime_types;
}
add_filter('upload_mimes', 'my_myme_types', 1, 1);

?>