<?php
/**
 * The template for displaying all single posts
 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main>

		</main>
	</div>
	<?php get_sidebar(); ?>
</div>

<?php get_footer();
