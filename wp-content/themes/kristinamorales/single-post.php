<?php
/*
 * Template Name: Blog Individual
 * Template Post Type: post
 */

get_header();  ?>
<body>

<main class="posts_area">

  <section class="breadcrumb-blog" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/breadcrumb-classic-bg.png);">
	<div class="lines">
	  <div class="container">
		<div class="row">
		  <div class="lines-items lines-items lines-items_white-light">
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
		  </div>
		</div>
	  </div>
	</div>
	<div class="container">
	  <div class="breadcrumb-blog__wrapper">
		<div class="breadcrumb-blog__title-block">
		  <div class="breadcrumb-blog__title" data-aos="fade-down" data-aos-delay="800">Blog </div>
		  <div class="breadcrumb-blog__sub-title">blog page</div>
		</div>
		<div class="breadcrumb-blog__nav">
		  <?php breadcrumbs_blog(); ?>
		</div>
	  </div>
	</div>
  </section>

  <section class="blog-list-content">
	<div class="lines">
	  <div class="container">
		<div class="row">
		  <div class="lines-items lines-items lines-items_gray-light">
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
		  </div>
		</div>
	  </div>
	</div>
	<div class="container">
	  <div class="row">
		<div class="list col-xl-9 col-lg-8">
		  <div class="list__item">
			<div class="row">
			  <div class="col-12">
				<div class="list__content">
				  <div class="list__block-img"><img class="list__img" src="<?php the_post_thumbnail_url(); ?>" style="width: 100%;" alt="" role="presentation" /></div>
				  <div class="list__title"><?php the_title(); ?></div>
				  <div class="list__info">By <?php the_author(); ?> | <?php the_time('M d, Y') ?></div>
				  <?php the_post(); the_content(); ?>
				</div>
			  </div>
			</div>
		  </div>
		</div>
		<div class="col-xl-3 col-lg-4 d-none d-lg-block d-xl-block">
		  <div class="form-search-blog">
			<div class="form-search-blog__title">Search</div>
			<form class="search" method="get" action="<?php echo home_url(); ?>" role="search">
			  <input style="width: auto;line-height: 0px;height: 20px;margin-bottom: 10px;" type="search" class="search-field form-search-blog__input" placeholder="<?php echo esc_attr_x( 'Enter Keyword', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
			  <button style="border: 2px solid #edc77c;outline: none;" type="submit" role="button" class="button button_primary button_full"/><span class="glyphicon glyphicon-search white">Go</span></button>
			</form>
		  </div>
		  <div class="category-blog">
			<div class="category-blog__title">Categories</div>
			<?php
			$args = array(
			  'show_option_all'    => '',
			  'show_option_none'   => __('No categories'),
			  'orderby'            => 'name',
			  'order'              => 'ASC',
			  'show_last_update'   => 0,
			  'style'              => '',
			  'show_count'         => 0,
			  'hide_empty'         => 1,
			  'use_desc_for_title' => 1,
			  'child_of'           => 0,
			  'hierarchical'       => true,
			  'title_li'           => __( 'Categories' ),
			  'number'             => 3,
			  'taxonomy'           => 'category',
			  'walker'             => 'Walker_Category',
			  'hide_title_if_empty' => false,
			  'separator'          => ''
			);

			wp_list_categories( $args );

			?>
		  </div>
		  <div class="related-posts">
			<div class="related-posts__title">Related Posts</div>
			<div class="related-posts__list">

			  <?php

			  $tags = wp_get_post_tags($post->ID);

			  if ($tags) {
				$first_tag = $tags[0]->term_id;
				$args = array(
				  'tag__in' => array($first_tag),
				  'post__not_in' => array($post->ID),
				  'posts_per_page'=>3,
				  'caller_get_posts'=>1
				);

				$my_query = new WP_Query($args);
				if( $my_query->have_posts() ) {
				  while ($my_query->have_posts()) : $my_query->the_post(); ?>

					<div class="related-posts__item"><img class="related-posts__icon" src="<?php the_post_thumbnail_url();?> " role="presentation" />
					  <div class="related-posts__info"><a class="related-posts__title-list" href="<?php the_permalink() ?>"><?php the_title(); ?></a>
						<div class="related-posts__date"><?php the_time('M d, Y') ?></div>
					  </div>
					</div>

				  <?php
				  endwhile;
				}
				wp_reset_query();
			  }
			  ?>

			</div>
		  </div>
		  <div class="follow-us">
			<div class="follow-us__title"><?php the_field('text_social', option); ?></div>
			<div class="follow-us__title-text"><?php the_field('description_social', option); ?></div>
			<ul class="social">
			  <li class="social__items"><a class="social__link" href="<?php the_field('footer_linkedin', option); ?>"> <svg width="17px" height="17px" viewbox="0 0 512 512"><path d="M61.538,0.44C27.531,0.44,0.005,28.015,0,61.945c0,33.962,27.526,61.532,61.543,61.532                    c33.924,0,61.488-27.57,61.488-61.532C123.032,28.01,95.462,0.44,61.538,0.44z"></path><rect x="8.462" y="170.149" , width="106.114" height="341.41"></rect><path d="M384.715,161.66c-51.618,0-86.229,28.301-100.396,55.139h-1.42v-46.65H181.125h-0.005v341.405h106.021                    V342.659c0-44.526,8.479-87.655,63.684-87.655c54.413,0,55.139,50.926,55.139,90.511v166.034H512v-187.26                    C512,232.37,492.166,161.66,384.715,161.66z"></path></svg></a></li>
			  <li class="social__items"><a class="social__link" href="<?php the_field('footer_facebook', option); ?>"><svg width="17px" height="27px" viewbox="0 0 612 792"><path d="M459,90.12L379.631,90c-89.17,0-146.784,59.168-146.784,150.609v69.448H153                    c-6.933,0-12.431,5.618-12.431,12.431v100.646c0,6.933,5.618,12.431,12.431,12.431h79.847v253.885                    c0,6.933,5.618,12.432,12.432,12.432H349.39c6.933,0,12.432-5.618,12.432-12.432V435.685h93.354                    c6.933,0,12.432-5.618,12.432-12.432V322.608c0-3.347-1.315-6.455-3.706-8.845s-5.498-3.706-8.845-3.706h-93.354v-58.81                    c0-28.329,6.694-42.672,43.629-42.672h53.431c6.933,0,12.432-5.618,12.432-12.432v-93.593C471.432,95.737,465.813,90.12,459,90.12z"></path></svg></a></li>
			  <li class="social__items"><a class="social__link" href="<?php the_field('footer_instagram', option); ?>"><svg width="17px" height="17px" viewbox="0 0 154.333 154.333"><defs><rect id="instagram1" width="154.333" height="154.333"></rect></defs><clippath id="instagram2"><use xlink:href="#instagram1" overflow="visible"></use></clippath><path clip-path="url(#instagram2)" d="M77.166,48.979c-15.542,0-28.187,12.645-28.187,28.187                    c0,15.543,12.645,28.188,28.187,28.188c15.542,0,28.188-12.645,28.188-28.188C105.354,61.624,92.708,48.979,77.166,48.979"></path><path clip-path="url(#instagram2)" d="M108.333,0H46C20.595,0,0,20.595,0,46v62.333c0,25.404,20.595,46,46,46h62.333                    c25.405,0,46-20.596,46-46V46C154.333,20.595,133.738,0,108.333,0 M77.166,119.354c-23.262,0-42.187-18.925-42.187-42.187                    c0-23.263,18.925-42.188,42.187-42.188s42.188,18.925,42.188,42.188C119.354,100.429,100.428,119.354,77.166,119.354                    M123.38,39.604c-4.694,0-8.5-3.805-8.5-8.5c0-4.694,3.806-8.5,8.5-8.5s8.5,3.806,8.5,8.5                    C131.88,35.799,128.074,39.604,123.38,39.604"></path></svg></a></li>
			</ul>
		  </div>
		  <form class="form-latest-news">
			<div class="form-latest-news__title"><?php the_field('text_form', option); ?></div>
			<div class="form-latest-news__title-text"><?php the_field('description_form', option); ?></div>
              <input class="form-latest-news__input" placeholder="E-mail" type="text" />
              <a class="button button_primary button_full" href="#">subscribe</a>
          </form>
		</div>
	  </div>
	</div>
  </section>
  <section class="contact-us" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/contact-us-bg.png);">
	<div class="lines">
	  <div class="container">
		<div class="row">
		  <div class="lines-items lines-items lines-items_white-light small-line">
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
		  </div>
		</div>
	  </div>
	</div>
	<div class="container">
	  <div class="contact-us__sub-title" data-aos="fade-down" data-aos-duration="1500">have any questions?</div>
	  <div class="contact-us__title" data-aos="fade-up" data-aos-duration="2000">Contact Us</div>
	  <div class="contact-us__title-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br> tempor incididunt ut labore et dolore magna aliqua.</div>
	  <form class="form-contact row">
		<div class="col-xl-4"><input class="form-contact__input" placeholder="Name" type="text" /></div>
		<div class="col-xl-4"><input class="form-contact__input" placeholder="E-mail" type="text" /></div>
		<div class="col-xl-4"><input class="form-contact__input" placeholder="Phone" type="text" /></div>
		<div class="col-xl-12"><textarea class="form-contact__input textarea" placeholder="Enter your massage..."></textarea></div>
		<div class="col-12"><a class="button button_primary" href="#" data-aos="flip-left" data-aos-duration="1500">contact us</a></div>
	  </form>
	</div>
  </section>

  <div class="modal-call-to-action">
	<div class="modal-call-to-action__wrapper">
	  <div class="modal-call-to-action__close">c l o s e</div>
	  <div class="modal-call-to-action__title">Call to action</div>
	  <div class="modal-call-to-action__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor </div>
	  <form class="form-call-to-action"><input class="form-call-to-action__input" placeholder="First name" type="text" /><input class="form-call-to-action__input" placeholder="Phone" type="text" /><input class="form-call-to-action__input" placeholder="E-mail" type="text" /><a class="button button_primary"
																																																																					 href="#">sumbit request</a></form>
	</div>
	<div class="bg-overlay"></div>
  </div>
  <div class="modal-video">
	<div class="modal-video__wrapper">
	  <div class="modal-video__close">c l o s e</div>
	  <div class="modal-video__iframe"><iframe width="100%" height="580" src="https://www.youtube.com/embed/pMxgov6_5TA?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe></div>
	</div>
	<div class="bg-overlay-video"></div>
  </div>
  <div class="contact-side">
	<div class="contact-side__btn button button_primary open-contact-side">have us contact you</div>
	<div class="form"><input class="form__input" placeholder="First name" type="text" /><input class="form__input" placeholder="Last name" type="text" /><input class="form__input" placeholder="E-mail" type="text" /><input class="form__input" placeholder="Phone" type="text"
	  /><textarea class="form__input textarea" type="textarea" placeholder="I would liketo get more details about"></textarea><a class="button button_primary" href="#">sumbit request</a></div>
  </div>
</main>

<!-- Footer -->
<?php get_footer(); ?>

</body>
</html>