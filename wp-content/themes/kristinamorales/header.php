<?php
/**
 * The header for our theme
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

  <?php
  if( have_rows('script_in_head',option) ):
	echo "<!-- Custom script start-->";
	while ( have_rows('script_in_head', option) ) : the_row();

	  the_sub_field('script_head');

	endwhile;
	echo "<!-- Custom script end-->";
  else : endif;
  ?>

	<?php wp_head(); ?>
</head>

<header class="header">
    <div class="header-top">
        <div class="container">
            <div class="header-top__wrapper"><a class="header-top__logo" href="/">
                    <img class="header-top__logo-icon" src="<?php the_field('desktop_header_logo', option); ?>" alt="" role="presentation"/></a>
                <div class="header-contacts">
                    <ul class="social">
                        <li class="social__items"><a class="social__link" href="<?php the_field('footer_linkedin', option); ?>"> <svg width="17px" height="17px" viewbox="0 0 512 512"><path d="M61.538,0.44C27.531,0.44,0.005,28.015,0,61.945c0,33.962,27.526,61.532,61.543,61.532c33.924,0,61.488-27.57,61.488-61.532C123.032,28.01,95.462,0.44,61.538,0.44z"></path><rect x="8.462" y="170.149" width="106.114" height="341.41"></rect><path d="M384.715,161.66c-51.618,0-86.229,28.301-100.396,55.139h-1.42v-46.65H181.125h-0.005v341.405h106.021V342.659c0-44.526,8.479-87.655,63.684-87.655c54.413,0,55.139,50.926,55.139,90.511v166.034H512v-187.26C512,232.37,492.166,161.66,384.715,161.66z"></path></svg></a></li>
                        <li class="social__items"><a class="social__link" href="<?php the_field('footer_facebook', option); ?>"><svg width="17px" height="27px" viewbox="0 0 612 792"><path d="M459,90.12L379.631,90c-89.17,0-146.784,59.168-146.784,150.609v69.448H153                    c-6.933,0-12.431,5.618-12.431,12.431v100.646c0,6.933,5.618,12.431,12.431,12.431h79.847v253.885                    c0,6.933,5.618,12.432,12.432,12.432H349.39c6.933,0,12.432-5.618,12.432-12.432V435.685h93.354                    c6.933,0,12.432-5.618,12.432-12.432V322.608c0-3.347-1.315-6.455-3.706-8.845s-5.498-3.706-8.845-3.706h-93.354v-58.81                    c0-28.329,6.694-42.672,43.629-42.672h53.431c6.933,0,12.432-5.618,12.432-12.432v-93.593C471.432,95.737,465.813,90.12,459,90.12z"></path></svg></a></li>
                        <li class="social__items"><a class="social__link" href="<?php the_field('footer_instagram', option); ?>"><svg width="17px" height="17px" viewbox="0 0 154.333 154.333"><defs><rect id="instagram1" width="154.333" height="154.333"></rect></defs><clippath id="instagram2"><use xlink:href="#instagram1" overflow="visible"></use></clippath><path clip-path="url(#instagram2)" d="M77.166,48.979c-15.542,0-28.187,12.645-28.187,28.187                    c0,15.543,12.645,28.188,28.187,28.188c15.542,0,28.188-12.645,28.188-28.188C105.354,61.624,92.708,48.979,77.166,48.979"></path><path clip-path="url(#instagram2)" d="M108.333,0H46C20.595,0,0,20.595,0,46v62.333c0,25.404,20.595,46,46,46h62.333                    c25.405,0,46-20.596,46-46V46C154.333,20.595,133.738,0,108.333,0 M77.166,119.354c-23.262,0-42.187-18.925-42.187-42.187                    c0-23.263,18.925-42.188,42.187-42.188s42.188,18.925,42.188,42.188C119.354,100.429,100.428,119.354,77.166,119.354                    M123.38,39.604c-4.694,0-8.5-3.805-8.5-8.5c0-4.694,3.806-8.5,8.5-8.5s8.5,3.806,8.5,8.5                    C131.88,35.799,128.074,39.604,123.38,39.604"></path></svg></a></li>
                    </ul><a class="header-top__tel" href="tel:<?php the_field('phone', option); ?>call"><?php the_field('phone', option); ?></a>
                    <a class="header-top__btn button button_primary click-modal" href="#">Private Consultation</a>
                </div>
                <a class="header-top__btn-main button button_primary click-modal" href="#">Private Consultation</a>
            </div>
        </div>
    </div>
  <?php
      $menu_name = "Main Menu";
      $menu_obj = get_term_by( 'name', $menu_name, 'nav_menu' );
      $menu_id = $menu_obj->term_id;
      $menuitems = wp_get_nav_menu_items( $menu_id, array( 'order' => 'DESC' ) );
  ?>
    <div class="header-nav">
        <div class="container">
            <div class="header-nav__wrapper">
                <ul class="header-nav__items">
                    <li class="header-nav__item logo-fixed"><a href="/"><svg width="54.574px" height="57px" viewbox="0 725 64.574 67"><path fill="#ECC67B" d="M25.379,750.885l-1.304,1.004c-0.114,0.083-0.196,0.146-0.248,0.197l-4.067,3.28l7.709,8.537c2.711,3.032,4.397,4.273,4.346-1.065l0.062-13.235C29.891,747.118,27.418,749.303,25.379,750.885z M25.379,750.885l-1.304,1.004c-0.114,0.083-0.196,0.146-0.248,0.197l-4.067,3.28l7.709,8.537c2.711,3.032,4.397,4.273,4.346-1.065l0.062-13.235C29.891,747.118,27.418,749.303,25.379,750.885z M32.282,728.15c-6.271,0-12.325,1.956-17.375,5.599c-1.345,0.973-2.856,2.287-4.005,3.467c-5.381,5.546-8.382,12.935-8.382,20.706c0,16.443,13.328,29.771,29.761,29.771c5.836,0,11.465-1.697,16.277-4.843c2.091-1.376,4.388-3.354,6.023-5.205c4.812-5.433,7.471-12.407,7.471-19.724C62.053,741.479,48.725,728.15,32.282,728.15z M50.68,761.058h-0.031l-0.021,5.867v12.501c-4.936,4.211-11.341,6.767-18.347,6.767c-15.605,0-28.261-12.654-28.261-28.271c0-8,3.322-15.232,8.662-20.365v30.434c-0.973,0-2.308,0.083-4.005,0.238v-0.352c0.911-0.259,1.531-0.662,1.873-1.2c0.352-0.528,0.517-1.335,0.517-2.401v-22.414c-3.374,4.461-5.391,10.027-5.391,16.061c0,14.694,11.911,26.605,26.605,26.605c6.343,0,12.169-2.226,16.733-5.92v-27.07L43.002,764.7l-8.009-17.095h1.646l6.353,13.473l6.198-13.473h1.49V761.058z M23.072,747.688v-0.082h10.328l8.837,18.781l-0.776,1.604h-0.227l-7.906-16.856v11.569c-0.031,0.982-0.042,1.479-0.042,1.479c0.011,2.235,0.88,3.467,2.608,3.694v0.113h-6.737l-11.548-12.707l4.439-3.539c1.521-1.221,2.276-2.152,2.276-2.794C24.324,748.443,23.91,748.029,23.072,747.688z M51.984,778.194v-30.589h4.015v0.082c-0.962,0.218-1.604,0.662-1.914,1.293c-0.321,0.652-0.487,2.081-0.487,4.285v20.572c3.322-4.44,5.288-9.945,5.288-15.916c0-14.694-11.911-26.605-26.605-26.605c-6.292,0-12.077,2.184-16.629,5.837v15.667h0.01l-0.01,0.321v2.152l11.6,12.696h-2.132l-9.468-10.534v6.819c0,1.066,0.176,1.862,0.528,2.401c0.331,0.538,0.962,0.941,1.873,1.2v0.352c-1.707-0.155-3.042-0.238-4.015-0.238v-31.664c4.926-4.161,11.29-6.675,18.244-6.675c15.615,0,28.271,12.654,28.271,28.271C60.553,765.869,57.272,773.051,51.984,778.194z M27.47,763.903c2.711,3.032,4.397,4.273,4.346-1.065l0.062-13.235c-1.987-2.484-4.46-0.3-6.499,1.282l-1.304,1.004c-0.114,0.083-0.196,0.146-0.248,0.197l-4.067,3.28L27.47,763.903z"></path></svg></a></li>
                    <li class="header-nav__item home"><a href="/">
                            <svg width="15px" height="14px" viewbox="0 0 512 512">
                                <path fill="#FFFFFF" d="M505.586,250.004c9.281-10.207,8.385-25.853-2.001-34.935L274.766,14.851c-10.387-9.083-27.038-8.893-37.206,0.429L7.963,225.744c-10.167,9.321-10.667,24.946-1.105,34.885l5.756,5.995c9.55,9.938,24.986,11.124,34.456,2.639l17.159-15.366v224.966c0,13.803,11.184,24.977,24.976,24.977h89.498c13.792,0,24.976-11.174,24.976-24.977V321.477h114.155v157.386c-0.198,13.792,9.67,24.967,23.463,24.967h94.845c13.793,0,24.977-11.175,24.977-24.977V257.064c0,0,4.74,4.152,10.587,9.291c5.836,5.129,18.095,1.016,27.376-9.201L505.586,250.004z"></path></svg></a></li>
                      <?php
                      $count = 0;
                      $submenu = false;
                      foreach( $menuitems as $item ):
                        $link = $item->url;
                        $title = $item->title;
                        if ( !$item->menu_item_parent ):
                          $parent_id = $item->ID;
                          ?>
                            <li class="header-nav__item"><a href="<?php echo $link; ?>"><?php echo $title; ?></a>
                        <?php endif; ?>
                        <?php if ( $parent_id == $item->menu_item_parent ): ?>
                        <?php if ( !$submenu ): $submenu = true; ?>
                              <ul class="header-nav__dropdown">
                        <?php endif; ?>
                          <li class="header-nav__li-dropdown"><a href="<?php echo $link; ?>" ><?php echo $title; ?></a></li>
                        <?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ): ?>
                              </ul>
                          <?php $submenu = false; endif; ?>
                      <?php endif; ?>
                        <?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id ): ?>
                          </li>
                        <?php $submenu = false; endif; ?>
                        <?php $count++; endforeach; ?>
                    </ul>
            </div>
        </div>
    </div>
    <div class="header-mobile">
        <div class="container">
            <div class="header-mobile__wrapper"><a class="header-mobile__logo" href="/">
                    <svg width="54.574px" height="57px" viewbox="0 725 64.574 67"><path fill="#ECC67B" d="M25.379,750.885l-1.304,1.004c-0.114,0.083-0.196,0.146-0.248,0.197l-4.067,3.28l7.709,8.537c2.711,3.032,4.397,4.273,4.346-1.065l0.062-13.235C29.891,747.118,27.418,749.303,25.379,750.885z M25.379,750.885l-1.304,1.004c-0.114,0.083-0.196,0.146-0.248,0.197l-4.067,3.28l7.709,8.537c2.711,3.032,4.397,4.273,4.346-1.065l0.062-13.235C29.891,747.118,27.418,749.303,25.379,750.885z M32.282,728.15c-6.271,0-12.325,1.956-17.375,5.599c-1.345,0.973-2.856,2.287-4.005,3.467c-5.381,5.546-8.382,12.935-8.382,20.706c0,16.443,13.328,29.771,29.761,29.771c5.836,0,11.465-1.697,16.277-4.843c2.091-1.376,4.388-3.354,6.023-5.205c4.812-5.433,7.471-12.407,7.471-19.724C62.053,741.479,48.725,728.15,32.282,728.15z M50.68,761.058h-0.031l-0.021,5.867v12.501c-4.936,4.211-11.341,6.767-18.347,6.767c-15.605,0-28.261-12.654-28.261-28.271c0-8,3.322-15.232,8.662-20.365v30.434c-0.973,0-2.308,0.083-4.005,0.238v-0.352c0.911-0.259,1.531-0.662,1.873-1.2c0.352-0.528,0.517-1.335,0.517-2.401v-22.414c-3.374,4.461-5.391,10.027-5.391,16.061c0,14.694,11.911,26.605,26.605,26.605c6.343,0,12.169-2.226,16.733-5.92v-27.07L43.002,764.7l-8.009-17.095h1.646l6.353,13.473l6.198-13.473h1.49V761.058z M23.072,747.688v-0.082h10.328l8.837,18.781l-0.776,1.604h-0.227l-7.906-16.856v11.569c-0.031,0.982-0.042,1.479-0.042,1.479c0.011,2.235,0.88,3.467,2.608,3.694v0.113h-6.737l-11.548-12.707l4.439-3.539c1.521-1.221,2.276-2.152,2.276-2.794C24.324,748.443,23.91,748.029,23.072,747.688z M51.984,778.194v-30.589h4.015v0.082c-0.962,0.218-1.604,0.662-1.914,1.293c-0.321,0.652-0.487,2.081-0.487,4.285v20.572c3.322-4.44,5.288-9.945,5.288-15.916c0-14.694-11.911-26.605-26.605-26.605c-6.292,0-12.077,2.184-16.629,5.837v15.667h0.01l-0.01,0.321v2.152l11.6,12.696h-2.132l-9.468-10.534v6.819c0,1.066,0.176,1.862,0.528,2.401c0.331,0.538,0.962,0.941,1.873,1.2v0.352c-1.707-0.155-3.042-0.238-4.015-0.238v-31.664c4.926-4.161,11.29-6.675,18.244-6.675c15.615,0,28.271,12.654,28.271,28.271C60.553,765.869,57.272,773.051,51.984,778.194z M27.47,763.903c2.711,3.032,4.397,4.273,4.346-1.065l0.062-13.235c-1.987-2.484-4.46-0.3-6.499,1.282l-1.304,1.004c-0.114,0.083-0.196,0.146-0.248,0.197l-4.067,3.28L27.47,763.903z"></path></svg></a>
                <div class="header-mobile__button-mobile button-mobile-open">
                    <div class="icon-bar"></div>
                    <div class="icon-bar"></div>
                    <div class="icon-bar"></div>
                </div>
                <div class="header-mobile__button-open button-mobile-open">menu</div>
            </div>
        </div>
    </div>
    <div class="header-mobile-nav">
        <div class="header-mobile-nav__wrapper">
            <ul class="header-mobile-nav__items">

			  <?php
			  $count = 0;
			  $submenu = false;
			  foreach( $menuitems as $item ):
				$link = $item->url;
				$title = $item->title;
				if ( !$item->menu_item_parent ):
				  $parent_id = $item->ID;
				  ?>
                    <li class="header-mobile-nav__item"><a href="<?php echo $link; ?>"><span><?php echo $title; ?></span></a>
				<?php endif; ?>
				<?php if ( $parent_id == $item->menu_item_parent ): ?>
				<?php if ( !$submenu ): $submenu = true; ?>
                      <ul class="header-mobile-nav__dropdown">
				<?php endif; ?>
                  <li class="header-mobile-nav__li-dropdown"><a href="<?php echo $link; ?>" ><?php echo $title; ?></a></li>
				<?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ): ?>
                      </ul>
				  <?php $submenu = false; endif; ?>
			    <?php endif; ?>
				<?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id ): ?>
                  </li>
				<?php $submenu = false; endif; ?>
				<?php $count++; endforeach; ?>

                <li><a class="header-mobile-nav__btn button button_primary click-modal" href="#">Private Consultation</a></li>
            </ul>
        </div>
    </div>
</header>