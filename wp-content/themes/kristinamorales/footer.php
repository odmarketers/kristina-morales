<?php
/**
 * The footer for our theme
 */
?>

    <footer class="footer">
        <div class="footer-top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/footer-bg.png);">
            <div class="container">
                <div class="footer-top__footer-logo">
                    <a href="/">
                        <img src="<?php the_field('logo_image', option); ?>">
                    </a>
                    <div class="footer-top__quote"><?php the_field('footer_text', option); ?></div>
                </div>
                <div class="footer-top__footer-center">
                    <div class="info">
                        <div class="info__item">
                            <div class="info__title-info address"> Address:</div>
                            <div class="info__text-info"><?php the_field('footer_address', option); ?></div>
                        </div>
                        <div class="info__item">
                            <div class="info__title-info phone">phone:</div>
                            <div class="info__text-info"><?php the_field('footer_phone', option); ?></div>
                        </div>
                        <div class="info__item">
                            <div class="info__title-info hours">hours:</div>
                            <div class="info__text-info"><?php the_field('footer_hours', option); ?></div>
                        </div>
                    </div>
                    <form class="form-footer"><input class="form-footer__input" placeholder="Email" type="text" /><input class="button button_primary" type="submit" value="sign up" /></form>
                </div>
                <div class="footer-top__footer-info">
                    <div class="partners">
                        <div class="partners__icons">
                            <img src="<?php the_field('image_1', option); ?>">
                            <img src="<?php the_field('image_2', option); ?>">
                        </div>
                        <div class="partners__text"><?php the_field('partners__text', option); ?></div>
                    </div>
                    <div class="footer-nav">
                        <ul class="footer-nav__wrap">
                            <li class="footer-nav__link">
                                <a href="#">Sitemap</a>
                            </li>
                            <li class="footer-nav__link">
                                <a href="#">Privacy Poilcy</a>
                            </li>
                            <li class="footer-nav__link">
                                <a href="#">Terms of Use</a>
                            </li>
                        </ul>
                    </div>

				  <?php if( get_field('text',option) ): ?>
                      <div class="container extra">
						<?php the_field('text',option); ?>
                      </div>
				  <?php endif; ?>

                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
			  <?php $check = get_field('show_copyright_2018',option); ?>
                <div class="footer-bottom__copyright">
                  <?php if ( $check == "yes" ) { ?> Copyright <?php echo date('Y'); ?> © <?php } ?>
				  <?php if ( $check == "yes" ) { ?>All rights reserved.<?php } ?>
                </div>
                <div class="footer-bottom__site"><?php the_field('company_name',option); ?></div>

                <?php $select = get_field('do_we_use_social_icons',option); ?>
                <?php if ( $select == "yes" ) { ?>
                    <ul class="social">
                        <li class="social__items"><a class="social__link" href="<?php the_field('footer_instagram', option); ?>"><svg width="17px" height="17px" viewbox="0 0 512 512"><path d="M61.538,0.44C27.531,0.44,0.005,28.015,0,61.945c0,33.962,27.526,61.532,61.543,61.532                c33.924,0,61.488-27.57,61.488-61.532C123.032,28.01,95.462,0.44,61.538,0.44z"></path><rect x="8.462" y="170.149" ,="," width="106.114" height="341.41"></rect><path d="M384.715,161.66c-51.618,0-86.229,28.301-100.396,55.139h-1.42v-46.65H181.125h-0.005v341.405h106.021                V342.659c0-44.526,8.479-87.655,63.684-87.655c54.413,0,55.139,50.926,55.139,90.511v166.034H512v-187.26                C512,232.37,492.166,161.66,384.715,161.66z"></path></svg></a></li>
                        <li class="social__items"><a class="social__link" href="<?php the_field('footer_facebook', option); ?>"><svg width="17px" height="27px" viewbox="0 0 612 792"><path d="M459,90.12L379.631,90c-89.17,0-146.784,59.168-146.784,150.609v69.448H153                c-6.933,0-12.431,5.618-12.431,12.431v100.646c0,6.933,5.618,12.431,12.431,12.431h79.847v253.885                c0,6.933,5.618,12.432,12.432,12.432H349.39c6.933,0,12.432-5.618,12.432-12.432V435.685h93.354                c6.933,0,12.432-5.618,12.432-12.432V322.608c0-3.347-1.315-6.455-3.706-8.845s-5.498-3.706-8.845-3.706h-93.354v-58.81                c0-28.329,6.694-42.672,43.629-42.672h53.431c6.933,0,12.432-5.618,12.432-12.432v-93.593C471.432,95.737,465.813,90.12,459,90.12z"></path></svg></a></li>
                        <li class="social__items"><a class="social__link" href="<?php the_field('footer_linkedin', option); ?>"><svg width="17px" height="17px" viewbox="0 0 154.333 154.333"><defs><rect id="instagram1" width="154.333" height="154.333"></rect></defs><clippath id="instagram2"><use xlink:href="#instagram1" overflow="visible"></use></clippath><path clip-path="url(#instagram2)" d="M77.166,48.979c-15.542,0-28.187,12.645-28.187,28.187                c0,15.543,12.645,28.188,28.187,28.188c15.542,0,28.188-12.645,28.188-28.188C105.354,61.624,92.708,48.979,77.166,48.979"></path><path clip-path="url(#instagram2)" d="M108.333,0H46C20.595,0,0,20.595,0,46v62.333c0,25.404,20.595,46,46,46h62.333                c25.405,0,46-20.596,46-46V46C154.333,20.595,133.738,0,108.333,0 M77.166,119.354c-23.262,0-42.187-18.925-42.187-42.187                c0-23.263,18.925-42.188,42.187-42.188s42.188,18.925,42.188,42.188C119.354,100.429,100.428,119.354,77.166,119.354                M123.38,39.604c-4.694,0-8.5-3.805-8.5-8.5c0-4.694,3.806-8.5,8.5-8.5s8.5,3.806,8.5,8.5                C131.88,35.799,128.074,39.604,123.38,39.604"></path></svg></a></li>
                    </ul>
                <?php } ?>

            </div>
        </div>
        <div class="modal-call-to-action">
            <div class="modal-call-to-action__wrapper">
                <div class="modal-call-to-action__close">c l o s e</div>
                <div class="modal-call-to-action__title">Call to action</div>
                <div class="modal-call-to-action__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor </div>
                <form class="form-call-to-action"><input class="form-call-to-action__input" placeholder="First name" type="text" /><input class="form-call-to-action__input" placeholder="Phone" type="text" /><input class="form-call-to-action__input" placeholder="E-mail" type="text" /><a class="button button_primary"
                                                                                                                                                                                                                                                                                               href="#">sumbit request</a></form>
            </div>
            <div class="bg-overlay"></div>
        </div>
    </footer>

    <?php
    if( have_rows('scripts_in_footer',option) ):
      echo "<!-- Custom script start-->";
      while ( have_rows('scripts_in_footer', option) ) : the_row();

        the_sub_field('script');

      endwhile;
      echo "<!-- Custom script end-->";
    else : endif;
    ?>

<?php wp_footer(); ?>