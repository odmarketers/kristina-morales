<?php
/**
 * Template part for content
 * @package WordPress
 * @subpackage Kristina Morales
 */
?>

<?php

  if( have_rows('content') ):
	while( have_rows('content') ) : the_row();

		if( get_row_layout() == 'blog' ):
		  $show = get_sub_field('show_hide');
		if ($show == "1"): ?>
              <section class="blog-list">
                  <div class="lines">
                      <div class="container">
                          <div class="row">
                              <div class="lines-items lines-items lines-items_gray-light">
                                  <div class="lines-items__item"></div>
                                  <div class="lines-items__item"></div>
                                  <div class="lines-items__item"></div>
                                  <div class="lines-items__item"></div>
                                  <div class="lines-items__item"></div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="container">
                      <div class="row">
                          <div class="col-xl-3 col-lg-4 d-none d-lg-block d-xl-block">
                              <div class="block-user-info">
                                  <img class="block-user-info__img" src="<?php the_sub_field('blog_image'); ?>" alt="" role="presentation" />
                                  <div class="block-user-info__name"><?php the_sub_field('blog_name'); ?></div>
                                  <a class="block-user-info__phone" href="tel: <?php the_sub_field('blog_phone'); ?>call">
                                      <svg width="10px" height="16px" viewBox="111.031 0 289.938 512">
                                          <path d="M363.71,0H148.29c-20.617,0-37.259,16.717-37.259,37.296v437.445c0,20.561,16.643,37.259,37.259,37.259h215.42c20.579,0,37.259-16.698,37.259-37.259V37.296C400.969,16.717,384.289,0,363.71,0z M202.845,22.65h106.348c2.687,0,4.87,4.011,4.87,8.974s-2.184,8.993-4.87,8.993H202.845c-2.706,0-4.851-4.03-4.851-8.993S200.139,22.65,202.845,22.65zM256.019,475.188c-13.116,0-23.788-10.672-23.788-23.807s10.672-23.77,23.788-23.77c13.079,0,23.751,10.635,23.751,23.77   S269.098,475.188,256.019,475.188z M373.058,393.674H138.961V62.932h234.096V393.674L373.058,393.674z"/>
                                      </svg><?php the_field('blog_phone'); ?><?php the_sub_field('blog_phone'); ?></a>
                                  <a class="button button_primary button_full button_high" href="/contact/">contact us</a>
                              </div>
                              <div class="category-blog">
								<?php
								$args = array(
								  'show_option_all'    => '',
								  'show_option_none'   => __('No categories'),
								  'orderby'            => 'name',
								  'order'              => 'ASC',
								  'show_last_update'   => 0,
								  'style'              => '',
								  'show_count'         => 0,
								  'hide_empty'         => 1,
								  'use_desc_for_title' => 1,
								  'child_of'           => 0,
								  'hierarchical'       => true,
								  'title_li'           => __( 'Categories' ),
								  'number'             => 3,
								  'taxonomy'           => 'category',
								  'walker'             => 'Walker_Category',
								  'hide_title_if_empty' => false,
								  'separator'          => ''
								);

								wp_list_categories( $args );

								?>
                              </div>
                          </div>
                          <div class="list col-xl-9 col-lg-8">

                              <!--BLOG WHILE-->
							<?php
							$query = new WP_Query('nopaging=1');
							if( $query->have_posts() ){
							  while( $query->have_posts() ){ $query->the_post();
								?>

                                  <div class="list__item" data-aos="fade" data-aos-delay="200">
                                      <div class="row">
                                          <div class="list__icon col-xl-7">
                                              <div class="list__block-icon"><img class="list__img" src="<?php the_post_thumbnail_url();?>" alt="" role="presentation" /></div>
                                          </div>
                                          <div class="list__info col-xl-5"><a class="list__title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                              <div class="list__sdsd">By <?php the_author(); ?> | <?php the_time('M d, Y') ?></div>
                                              <div class="list__text"><?php the_excerpt(); ?></div><a class="lines-button" href="<?php the_permalink(); ?>">read more</a></div>
                                      </div>
                                  </div>

							  <?php } wp_reset_postdata(); } ?>

                          </div>
                      </div>
                  </div>
              </section>
        <?php endif;

        elseif( get_row_layout() == 'breadcrumb' ):
		  $show = get_sub_field('show_hide');
		  if ($show == "1"): ?>

			<?php $choise = get_sub_field('we_use_color_or_image'); ?>
			<?php if ($choise == "image"): ?>
              <section class="breadcrumb-blog" style="background-image: url(<?php the_sub_field('image'); ?>)">
            <?php endif; ?>
          	<?php if ($choise == "color"): ?>
              <section class="breadcrumb-blog" style="background-color: <?php the_sub_field('color'); ?> ">
            <?php endif; ?>

                  <div class="lines">
                      <div class="container">
                          <div class="row">
                              <div class="lines-items lines-items lines-items_white-light">
                                  <div class="lines-items__item"></div>
                                  <div class="lines-items__item"></div>
                                  <div class="lines-items__item"></div>
                                  <div class="lines-items__item"></div>
                                  <div class="lines-items__item"></div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="container">
                      <div class="breadcrumb-blog__wrapper">
                          <div class="breadcrumb-blog__title-block">
                              <div <?php if ($choise == "color"): ?> style="color:<?php the_sub_field('font_color'); ?>"<?php endif; ?> class="breadcrumb-blog__title" data-aos="fade-down" data-aos-delay="800"><?php the_sub_field('title_page'); ?></div>
                              <div <?php if ($choise == "color"): ?> style="color:<?php the_sub_field('font_color'); ?>"<?php endif; ?>class="breadcrumb-blog__sub-title"><?php the_sub_field('title_page'); ?> page</div>
                          </div>
                          <div class="breadcrumb-blog__nav">
                              <div <?php if ($choise == "color"): ?> style="color:<?php the_sub_field('font_color'); ?>"<?php endif; ?>class="breadcrumb-blog__item">
                              <a <?php if ($choise == "color"): ?> style="color:<?php the_sub_field('font_color'); ?>"<?php endif; ?> href="/">home</a></div>
                              <div <?php if ($choise == "color"): ?> style="color:<?php the_sub_field('font_color'); ?>"<?php endif; ?>class="breadcrumb-blog__item"> <span><?php the_sub_field('title_page'); ?></span></div>
                          </div>
                      </div>
                  </div>
              </section>
		  <?php endif;

        elseif( get_row_layout() == 'contact' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>

          <?php $case = get_sub_field('select_contact_form'); ?>
          <?php if ($case == "standart") { ?>

                <!-- contact Standart-->
                <section class="contact-us" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/contact-us-bg.png);">
                    <div class="lines">
                        <div class="container">
                            <div class="row">
                                <div class="lines-items lines-items lines-items_white-light small-line">
                                    <div class="lines-items__item"></div>
                                    <div class="lines-items__item"></div>
                                    <div class="lines-items__item"></div>
                                    <div class="lines-items__item"></div>
                                    <div class="lines-items__item"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="contact-us__sub-title" data-aos="fade-down" data-aos-duration="1500">have any questions?</div>
                        <div class="contact-us__title" data-aos="fade-up" data-aos-duration="2000"><?php the_field('standart_title',option); ?></div>
                        <div class="contact-us__title-text"><?php the_field('standart_description',option); ?></div>
                        <form class="form-contact row">
                            <div class="col-xl-4"><input class="form-contact__input" placeholder="Name" type="text" /></div>
                            <div class="col-xl-4"><input class="form-contact__input" placeholder="E-mail" type="text" /></div>
                            <div class="col-xl-4"><input class="form-contact__input" placeholder="Phone" type="text" /></div>
                            <div class="col-xl-12"><textarea class="form-contact__input textarea" placeholder="Enter your massage..."></textarea></div>
                            <div class="col-12"><a class="button button_primary" href="#" data-aos="flip-left" data-aos-duration="1500">contact us</a></div>
                        </form>
                    </div>
                </section>

          <?php } ?>

          <?php if ($case == "custom") { ?>

                <!-- contact Custom -->
                <section class="contact-us" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/contact-us-bg.png);">
                    <div class="lines">
                        <div class="container">
                            <div class="row">
                                <div class="lines-items lines-items lines-items_white-light small-line">
                                    <div class="lines-items__item"></div>
                                    <div class="lines-items__item"></div>
                                    <div class="lines-items__item"></div>
                                    <div class="lines-items__item"></div>
                                    <div class="lines-items__item"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="contact-us__sub-title" data-aos="fade-down" data-aos-duration="1500">have any questions?</div>
                        <div class="contact-us__title" data-aos="fade-up" data-aos-duration="2000"><?php the_field('custom_title',option); ?></div>
                        <div class="contact-us__title-text"><?php the_field('custom_description',option); ?></div>
                        <form class="form-contact row">
                            <div class="col-xl-4"><input class="form-contact__input" placeholder="Name" type="text" /></div>
                            <div class="col-xl-4"><input class="form-contact__input" placeholder="E-mail" type="text" /></div>
                            <div class="col-xl-4"><input class="form-contact__input" placeholder="Phone" type="text" /></div>
                            <div class="col-xl-12"><textarea class="form-contact__input textarea" placeholder="Enter your massage..."></textarea></div>
                            <div class="col-12"><a class="button button_primary" href="#" data-aos="flip-left" data-aos-duration="1500">contact us</a></div>
                        </form>
                    </div>
                </section>

          <?php } ?>

        <?php endif;

        elseif( get_row_layout() == 'main' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="main" style="background-image: url(<?php the_sub_field('image')?>);">
                <div class="lines">
                    <div class="container">
                        <div class="row">
                            <div class="lines-items lines-items lines-items_white-light small-line">
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="main__title" data-aos="zoom-in" data-aos-duration="3000"><?php the_sub_field('title'); ?></div>
                    <div class="main__title-text" data-aos="zoom-in-down" data-aos-duration="500"><?php the_sub_field('description'); ?></div>
                    <div class="form-search" data-aos="fade-up" data-aos-duration="1500">
                        <div class="form-search__tabs">
                            <label class="radio">
                                <input type="radio" name="order" value="0">
                                <span><div class="ico">Buy</div></span>
                            </label>
                            <label class="radio">
                                <input type="radio" name="order" value="1">
                                <span><div class="ico">Sell</div></span>
                            </label>
                            <label class="radio">
                                <input type="radio" name="order" value="2" checked>
                                <span><div class="ico">Rent</div></span>
                            </label>
                            <label class="radio">
                                <input type="radio" name="order" value="3">
                                <span><div class="ico">Unlisted Properties</div></span>
                            </label>
                        </div>
                        <div class="form-search__search">
                            <input class="form-search__input" placeholder="Search by city or address..." type="text" />
                            <a class="button button_primary" href="#">search</a>
                        </div>
                        <div class="form-search__dropdown-search">
                            <div class="advanced-search">
                                <div class="advanced-search__wrapper">
                                    <input class="advanced-search__input" placeholder="City" type="text" />
                                    <input class="advanced-search__input" placeholder="State" type="text" />
                                    <div class="advanced-search__price">
                                        <label for="amount">Price </label>
                                        <input id="amount" readonly>
                                        <div id="slider-range"></div>
                                        <input id="min" type="hidden">
                                        <input id="max" type="hidden">
                                    </div>
                                    <div class="advanced-search__rooms">
                                        <select class="select-box">
                                            <option value="hide">Beds</option>
                                            <option>Beds</option>
                                            <option>Beds</option>
                                            <option>Beds</option>
                                        </select>
                                        <select class="select-box">
                                            <option value="hide">Baths</option>
                                            <option>Baths</option>
                                            <option>Baths</option>
                                            <option>Baths</option>
                                        </select>
                                    </div>
                                    <select class="select-box">
                                        <option value="hide">Area</option>
                                        <option>Area 1</option>
                                        <option>Area 2</option>
                                        <option>Area 3</option>
                                    </select>
                                    <select class="select-box">
                                        <option value="hide">Property Type</option>
                                        <option>Property Type 1</option>
                                        <option>Property Type 2</option>
                                        <option>Property Type 3</option>
                                    </select>
                                    <input class="advanced-search__input" placeholder="County" type="text" />
                                    <select class="select-box">
                                        <option value="hide">Sub-division</option>
                                        <option>Sub-division 1</option>
                                        <option>Sub-division 2</option>
                                        <option>Sub-division 3</option>
                                    </select>
                                    <input class="advanced-search__input"placeholder="Street Name" type="text" />
                                    <input class="advanced-search__input" placeholder="Street Number" type="text" />
                                    <select class="select-box">
                                        <option value="hide">School District</option>
                                        <option>School District 1</option>
                                        <option>School District 2</option>
                                        <option>School District 3</option>
                                    </select>
                                    <select class="select-box">
                                        <option value="hide">Garage</option>
                                        <option>Garage 1</option>
                                        <option>Garage 2</option>
                                        <option>Garage 3</option>
                                    </select>
                                    <input class="advanced-search__input" placeholder="Square Feet" type="text" />
                                    <input class="advanced-search__input" placeholder="Lot Size" type="text" />
                                    <select class="select-box">
                                        <option value="hide">Year Built</option>
                                        <option>Year Built 1</option>
                                        <option>Year Built 2</option>
                                        <option>Year Built 3</option>
                                    </select>
                                    <select class="select-box">
                                        <option value="hide">Levels</option>
                                        <option>Levels 1</option>
                                        <option>Levels 2</option>
                                        <option>Levels 3</option>
                                    </select>
                                    <select class="select-box">
                                        <option value="hide">Pool</option>
                                        <option>Pool 1</option>
                                        <option>Pool 2</option>
                                        <option>Pool 3</option>
                                    </select>
                                    <select class="select-box">
                                        <option value="hide">View</option>
                                        <option>View 1</option>
                                        <option>View 2</option>
                                        <option>View 3</option>
                                    </select>
                                    <select class="select-box">
                                        <option value="hide">Architecture</option>
                                        <option>Architecture 1</option>
                                        <option>Architecture 2</option>
                                        <option>Architecture 3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-search__advanced">
                                <div class="form-search__advanced-button">advanced search</div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'why_you_should_hire_me' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="advantages">
                <div class="lines">
                    <div class="container">
                        <div class="row">
                            <div class="lines-items small-line">
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="block-title">
                        <div class="block-title__title"><?php the_sub_field('title'); ?></div>
                        <div class="block-title__title-text" data-aos="fade-down" data-aos-duration="1500"><?php the_sub_field('description'); ?></div>
                    </div>
                    <div class="list row">
                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                            <div class="list__item" data-aos="fade-left" data-aos-duration="500">
                                <div class="list__block">
                                    <div class="list__title"><?php the_sub_field('title_1'); ?></div>
                                    <div class="list__chart">01.</div>
                                </div>
                                <div class="list__text"><?php the_sub_field('text_1'); ?></div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                            <div class="list__item" data-aos="fade-left" data-aos-duration="500">
                                <div class="list__block">
                                    <div class="list__title"><?php the_sub_field('title_2'); ?></div>
                                    <div class="list__chart">02.</div>
                                </div>
                                <div class="list__text"><?php the_sub_field('text_2'); ?></div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                            <div class="list__item" data-aos="fade-right" data-aos-duration="500">
                                <div class="list__block">
                                    <div class="list__title"><?php the_sub_field('title_3'); ?></div>
                                    <div class="list__chart">03.</div>
                                </div>
                                <div class="list__text"><?php the_sub_field('text_3'); ?></div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                            <div class="list__item" data-aos="fade-right" data-aos-duration="500">
                                <div class="list__block">
                                    <div class="list__title"><?php the_sub_field('title_4'); ?></div>
                                    <div class="list__chart">04.</div>
                                </div>
                                <div class="list__text"><?php the_sub_field('text_4'); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'meet_block' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="about-me" style="background-color: <?php the_sub_field('background_color'); ?>">
                <div class="lines">
                    <div class="container">
                        <div class="row">
                            <div class="lines-items lines-items lines-items_gray-light small-line">
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row align-items-end">
                        <div class="col-xl-6" data-aos="fade-right"><img class="about-me__icon" src="<?php the_sub_field('image'); ?>" alt="" role="presentation" /></div>
                        <div class="col-xl-6" data-aos="fade-left">
                            <div class="about-me__title"><?php the_sub_field('title'); ?></div>
                            <div class="about-me__title-text"><?php the_sub_field('description'); ?></div>
                            <p><?php the_sub_field('text'); ?></p>
                            <a class="button button_primary" href="<?php the_sub_field('button_url'); ?>" data-aos="flip-left" data-aos-duration="1500"><?php the_sub_field('buttton_text'); ?></a>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'featured_listings' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="listings-featured">
                <div class="lines">
                    <div class="container">
                        <div class="row">
                            <div class="lines-items">
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="block-title">
                        <div class="block-title__title"><?php the_sub_field('title'); ?></div>
                        <div class="block-title__title-text" data-aos="fade-down" data-aos-duration="1500"><?php the_sub_field('description'); ?></div>
                    </div>
                    <div class="listings-featured__list">
                        <div class="listings-featured__item row" data-aos="fade-right">
                            <div class="listings-featured__info col-xl-4">
                                <div class="listings-featured__title-list"><a href="/my-listings-page/">AVA Nob Hill</a></div>
                                <div class="listings-featured__location">7843 Durham Avenue, MD</div>
                                <div class="listings-featured__price"><span>$1,199.00/mon</span></div>
                                <div class="listings-featured__info-item">
                                    <div class="listings-featured__block">
                                        <div class="listings-featured__name">Area</div>
                                        <div class="listings-featured__info-block">2,100 sq. ft.</div>
                                    </div>
                                    <div class="listings-featured__block">
                                        <div class="listings-featured__name">Bedrooms</div>
                                        <div class="listings-featured__info-block">2</div>
                                    </div>
                                    <div class="listings-featured__block">
                                        <div class="listings-featured__name">Bathrooms</div>
                                        <div class="listings-featured__info-block">1</div>
                                    </div>
                                    <div class="listings-featured__block">
                                        <div class="listings-featured__name">Garages</div>
                                        <div class="listings-featured__info-block">2</div>
                                    </div>
                                </div>
                                <div class="listings-featured__text-item">Front-facing, single level, well-designed floor plan. Secure building in a highly DESIRABLE location!</div>
                                <a class="lines-button" href="#">more</a>
                            </div>
                            <div class="listings-featured__foto col-xl-8">
                                <div class="listings-featured__block-icon">
                                    <img class="listings-featured__icon" src="<?php echo get_template_directory_uri()?>/assets/img/list-feature-1.png" style="width: 100%;" alt="" role="presentation" />
                                </div>
                            </div>
                        </div>
                        <div class="listings-featured__item row" data-aos="fade-left">
                            <div class="listings-featured__info col-xl-4">
                                <div class="listings-featured__title-list"><a href="/my-listings-page/">AVA Nob Hill</a></div>
                                <div class="listings-featured__location">7843 Durham Avenue, MD</div>
                                <div class="listings-featured__price"><span>$1,199.00/mon</span></div>
                                <div class="listings-featured__info-item">
                                    <div class="listings-featured__block">
                                        <div class="listings-featured__name">Area</div>
                                        <div class="listings-featured__info-block">2,100 sq. ft.</div>
                                    </div>
                                    <div class="listings-featured__block">
                                        <div class="listings-featured__name">Bedrooms</div>
                                        <div class="listings-featured__info-block">2</div>
                                    </div>
                                    <div class="listings-featured__block">
                                        <div class="listings-featured__name">Bathrooms</div>
                                        <div class="listings-featured__info-block">1</div>
                                    </div>
                                    <div class="listings-featured__block">
                                        <div class="listings-featured__name">Garages</div>
                                        <div class="listings-featured__info-block">2</div>
                                    </div>
                                </div>
                                <div class="listings-featured__text-item">Front-facing, single level, well-designed floor plan. Secure building in a highly DESIRABLE location!</div>
                                <a class="lines-button" href="#">more</a>
                            </div>
                            <div class="listings-featured__foto col-xl-8">
                                <div class="listings-featured__block-icon">
                                    <img class="listings-featured__icon" src="<?php echo get_template_directory_uri()?>/assets/img/list-feature-2.png" style="width: 100%;" alt="" role="presentation" />
                                </div>
                            </div>
                        </div>
                        <div class="listings-featured__item row" data-aos="fade-right">
                            <div class="listings-featured__info col-xl-4">
                                <div class="listings-featured__title-list"><a href="/my-listings-page/">AVA Nob Hill</a></div>
                                <div class="listings-featured__location">7843 Durham Avenue, MD</div>
                                <div class="listings-featured__price"><span>$1,199.00/mon</span></div>
                                <div class="listings-featured__info-item">
                                    <div class="listings-featured__block">
                                        <div class="listings-featured__name">Area</div>
                                        <div class="listings-featured__info-block">2,100 sq. ft.</div>
                                    </div>
                                    <div class="listings-featured__block">
                                        <div class="listings-featured__name">Bedrooms</div>
                                        <div class="listings-featured__info-block">2</div>
                                    </div>
                                    <div class="listings-featured__block">
                                        <div class="listings-featured__name">Bathrooms</div>
                                        <div class="listings-featured__info-block">1</div>
                                    </div>
                                    <div class="listings-featured__block">
                                        <div class="listings-featured__name">Garages</div>
                                        <div class="listings-featured__info-block">2</div>
                                    </div>
                                </div>
                                <div class="listings-featured__text-item">Front-facing, single level, well-designed floor plan. Secure building in a highly DESIRABLE location!</div>
                                <a class="lines-button" href="#">more</a>
                            </div>
                            <div class="listings-featured__foto col-xl-8">
                                <div class="listings-featured__block-icon">
                                    <img class="listings-featured__icon" src="<?php echo get_template_directory_uri()?>/assets/img/list-feature-1.png" style="width: 100%;" alt="" role="presentation" />
                                </div>
                            </div>
                        </div>
                        <div class="listings-featured__item row" data-aos="fade-left">
                            <div class="listings-featured__info col-xl-4">
                                <div class="listings-featured__title-list"><a href="/my-listings-page/">AVA Nob Hill</a></div>
                                <div class="listings-featured__location">7843 Durham Avenue, MD</div>
                                <div class="listings-featured__price"><span>$1,199.00/mon</span></div>
                                <div class="listings-featured__info-item">
                                    <div class="listings-featured__block">
                                        <div class="listings-featured__name">Area</div>
                                        <div class="listings-featured__info-block">2,100 sq. ft.</div>
                                    </div>
                                    <div class="listings-featured__block">
                                        <div class="listings-featured__name">Bedrooms</div>
                                        <div class="listings-featured__info-block">2</div>
                                    </div>
                                    <div class="listings-featured__block">
                                        <div class="listings-featured__name">Bathrooms</div>
                                        <div class="listings-featured__info-block">1</div>
                                    </div>
                                    <div class="listings-featured__block">
                                        <div class="listings-featured__name">Garages</div>
                                        <div class="listings-featured__info-block">2</div>
                                    </div>
                                </div>
                                <div class="listings-featured__text-item">Front-facing, single level, well-designed floor plan. Secure building in a highly DESIRABLE location!</div>
                                <a class="lines-button" href="#">more</a>
                            </div>
                            <div class="listings-featured__foto col-xl-8">
                                <div class="listings-featured__block-icon">
                                    <img class="listings-featured__icon" src="<?php echo get_template_directory_uri()?>/assets/img/list-feature-2.png" style="width: 100%;" alt="" role="presentation" />
                                </div>
                            </div>
                        </div>
                    </div><a class="button button_primary" href="<?php the_sub_field('button_url'); ?>" data-aos="flip-left" data-aos-duration="1500"><?php the_sub_field('button_text'); ?></a></div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'featured_cities' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="featured-cities">
                <div class="lines">
                    <div class="container">
                        <div class="row">
                            <div class="lines-items lines-items lines-items_gray-light small-line">
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="block-title">
                        <div class="block-title__title"><?php the_sub_field('title'); ?></div>
                        <div class="block-title__title-text" data-aos="fade-down" data-aos-duration="1500"><?php the_sub_field('description'); ?></div>
                    </div>
                    <div class="featured-cities__list">
                        <div class="row">
                            <div class="col-xl-8 col-lg-8 col-md-8" data-aos="zoom-in-down">
                                <a class="featured-cities__item" href="#" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-featured-cities-1.png);">
                                          <span class="featured-cities__info">
                                              <span class="featured-cities__info-block">
                                                  <span class="featured-cities__info-title">Newport Beach</span>
                                                  <span class="featured-cities__info-text">14 Properties</span>
                                              </span>
                                          </span>
                                </a>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4" data-aos="zoom-in-down">
                                <a class="featured-cities__item" href="#" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-featured-cities-2.png);">
                                          <span class="featured-cities__info">
                                              <span class="featured-cities__info-block">
                                                  <span class="featured-cities__info-title">Laguna Niguel</span>
                                                  <span class="featured-cities__info-text">14 Properties</span>
                                              </span>
                                          </span>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-8 col-lg-8 col-md-8" data-aos="zoom-in-down">
                                <a class="featured-cities__item" href="#" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-featured-cities-4.png);">
                                          <span class="featured-cities__info">
                                              <span class="featured-cities__info-block">
                                                  <span class="featured-cities__info-title">Newport Beach</span>
                                                  <span class="featured-cities__info-text">14 Properties</span>
                                              </span>
                                          </span>
                                </a>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4" data-aos="zoom-in-down">
                                <a class="featured-cities__item" href="#" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-featured-cities-3.png);">
                                          <span class="featured-cities__info">
                                              <span class="featured-cities__info-block">
                                                  <span class="featured-cities__info-title">Laguna Niguel</span>
                                                  <span class="featured-cities__info-text">14 Properties</span>
                                              </span>
                                          </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'helping_block' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="get-an-estimate" style="background-image: url(<?php the_sub_field('image')?>);">
                <div class="lines">
                    <div class="container">
                        <div class="row">
                            <div class="lines-items lines-items lines-items_white-light small-line">
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="get-an-estimate__sub-title" data-aos="fade-down" data-aos-duration="1000"><?php the_sub_field('text'); ?></div>
                    <div class="get-an-estimate__title" data-aos="fade-up" data-aos-duration="2000"><?php the_sub_field('title'); ?></div>
                    <div class="get-an-estimate__title-text"><?php the_sub_field('description'); ?></div>
                    <a class="button button_primary" href="<?php the_sub_field('button_url'); ?>" data-aos="flip-left" data-aos-duration="1500"><?php the_sub_field('button_text'); ?></a></div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'our_services' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="our-services">
                <div class="lines">
                    <div class="container">
                        <div class="row">
                            <div class="lines-items small-line">
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="our-services__title-block">
                        <div class="our-services__title"><?php the_sub_field('title'); ?></div>
                        <div class="our-services__title-text" data-aos="fade-down" data-aos-duration="1500"><?php the_sub_field('description'); ?></div>
                    </div>
                    <div class="list row">

                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                            <div class="list__item">
                                <div class="list__block" data-aos="fade-left" data-aos-duration="700">
                                    <div class="list__title"><?php the_sub_field('title_1'); ?></div>
                                    <img class="list__chart" src="<?php echo get_template_directory_uri()?>/assets/img/services-icon-1.svg" alt="" role="presentation" /></div>
                                <div class="list__text"><?php the_sub_field('description_1'); ?></div>
                                <a class="lines-button" href="<?php the_sub_field('url_1'); ?>">more</a>
                            </div>
                        </div>

                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                            <div class="list__item">
                                <div class="list__block" data-aos="fade-left" data-aos-duration="700">
                                    <div class="list__title"><?php the_sub_field('title_2'); ?></div>
                                    <img class="list__chart" src="<?php echo get_template_directory_uri()?>/assets/img/services-icon-2.svg" alt="" role="presentation" /></div>
                                <div class="list__text"><?php the_sub_field('description_2'); ?></div>
                                <a class="lines-button" href="<?php the_sub_field('url_2'); ?>">more</a>
                            </div>
                        </div>

                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                            <div class="list__item">
                                <div class="list__block" data-aos="fade-left" data-aos-duration="700">
                                    <div class="list__title"><?php the_sub_field('title_3'); ?></div>
                                    <img class="list__chart" src="<?php echo get_template_directory_uri()?>/assets/img/services-icon-3.svg" alt="" role="presentation" /></div>
                                <div class="list__text"><?php the_sub_field('description_3'); ?></div>
                                <a class="lines-button" href="<?php the_sub_field('url_3'); ?>">more</a>
                            </div>
                        </div>

                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                            <div class="list__item">
                                <div class="list__block" data-aos="fade-left" data-aos-duration="700">
                                    <div class="list__title"><?php the_sub_field('title_4'); ?></div>
                                    <img class="list__chart" src="<?php echo get_template_directory_uri()?>/assets/img/services-icon-4.svg" alt="" role="presentation" /></div>
                                <div class="list__text"><?php the_sub_field('description_4'); ?></div>
                                <a class="lines-button" href="<?php the_sub_field('url_4'); ?>">more</a>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'form_get_more_details' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="form-main" style="background-image: url(<?php the_sub_field('image'); ?>);">
                <div class="lines">
                    <div class="container">
                        <div class="row">
                            <div class="lines-items lines-items lines-items_light small-line">
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="block-title">
                        <div class="block-title__title"><?php the_sub_field('title'); ?></div>
                        <div class="block-title__title-text" data-aos="fade-down" data-aos-duration="1500"><?php the_sub_field('descripriton'); ?></div>
                    </div>
                    <form class="form-search-main row">
                        <div class="col-xl-4 col-lg-4"><input class="form-search-main__input" placeholder="Name" value="Kristina Mora" autocomplete="" type="text" /></div>
                        <div class="col-xl-4 col-lg-4"><input class="form-search-main__input" placeholder="E-mail" type="text" /></div>
                        <div class="col-xl-4 col-lg-4"><input class="form-search-main__input" placeholder="Phone" type="text" /></div>
                        <div class="form-search-main__wrapper-chexbox col-12">
                            <div class="form-search-main__section-1">
                                <div class="form-search-main__title-section">Would you like to:</div>
                                <label class="checkbox">
                                    <input type="radio" name="order" value="1">
                                    <div class="icon"></div>Buy
                                </label>
                                <label class="checkbox">
                                    <input type="radio" name="order" value="0"><div class="icon"></div>Rent
                                </label>
                            </div>
                            <div class="form-search-main__section-2">
                                <div class="form-search-main__title-section">Property Type:</div>
                                <label class="checkbox">
                                    <input type="radio" name="order2" value="1">
                                    <div class="icon"></div>Single Family
                                </label>
                                <label class="checkbox">
                                    <input type="radio" name="order2" value="0"><div class="icon"></div>Townhome
                                </label>
                                <label class="checkbox">
                                    <input type="radio" name="order2" value="0">
                                    <div class="icon"></div>Condo
                                </label>
                                <label class="checkbox">
                                    <input type="radio" name="order2" value="0">
                                    <div class="icon"></div>Apartment
                                </label>
                            </div>
                        </div>
                        <div class="form-search-main__wrapper-input col-12">
                            <div class="form-search-main__section-3">
                                <div class="form-search-main__title-section">Price Range:</div>
                                <input class="form-search-main__input price" placeholder="Min" type="number" />
                                <input class="form-search-main__input price margin-none" placeholder="Max" type="number" />
                            </div>
                            <div class="form-search-main__section-3">
                                <div class="form-search-main__title-section plus-1">Bedrooms:</div>
                                <input class="form-search-main__input rooms" placeholder="1" type="text" />
                            </div>
                            <div class="form-search-main__section-3">
                                <div class="form-search-main__title-section plus-2">Bathrooms:</div>
                                <input class="form-search-main__input rooms" placeholder="1" type="text" />
                            </div>
                            <div class="form-search-main__section-3">
                                <div class="form-search-main__title-section">Zip Code(s):</div>
                                <input class="form-search-main__input zip" placeholder="12345" type="text" />
                            </div>
                        </div>
                        <div class="col-xl-8 col-lg-8">
                            <textarea class="form-search-main__input textarea" placeholder="I am interested in Houston's Most Expensive Neighborhoods and would like to get more details."></textarea>
                        </div>
                        <div class="col-xl-4 col-lg-4"><a class="button button_primary" href="#">get more details</a></div>
                    </form>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'about_us' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="about-us">
                <div class="lines">
                    <div class="container">
                        <div class="row">
                            <div class="lines-items small-line">
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="block-title">
                        <div class="block-title__title"><?php the_sub_field('title'); ?></div>
                        <div class="block-title__title-text" data-aos="fade-down" data-aos-duration="1500"><?php the_sub_field('description'); ?></div>
                    </div>
                    <div class="row first" data-aos="fade-right">
                        <div class="about-us__bag col-xl-8 offset-xl-4">
                            <div class="row">
                                <div class="col-xl-7 offset-xl-5">
                                    <div class="about-us__title-info"><?php the_sub_field('title_text'); ?></div>
                                    <p><?php the_sub_field('text'); ?></p>
                                    <a class="lines-button" href="<?php the_sub_field('more_url'); ?>">more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="second" data-aos="fade-left">
                        <div class="about-us__icons">
                            <div class="about-us__quote"><?php the_sub_field('quote'); ?></div>
                            <div class="about-us__icons-block">
                                <img src="<?php the_sub_field('image'); ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'testimonials' ):
            $show = get_sub_field('show_hide');
            if ($show == "1"): ?>
                <section class="testimonials" style="background-color: <?php the_sub_field('background_color'); ?>">
                    <div class="lines">
                        <div class="container">
                            <div class="row">
                                <div class="lines-items lines-items lines-items_light small-line">
                                    <div class="lines-items__item"></div>
                                    <div class="lines-items__item"></div>
                                    <div class="lines-items__item"></div>
                                    <div class="lines-items__item"></div>
                                    <div class="lines-items__item"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="block-title">
                            <div class="block-title__title"><?php the_sub_field('title'); ?></div>
                            <div class="block-title__title-text" data-aos="fade-down" data-aos-duration="1500"><?php the_sub_field('description'); ?></div>
                        </div>
                        <div class="list-slide-testimonials">

                          <?php $case = get_sub_field('test_contact_global_or_custom'); ?>

                          <?php if ($case == "global"): ?>

                              <!-- contact Global-->
                            <?php
                            if( have_rows('testimonials_rep_global',option) ):
                              while ( have_rows('testimonials_rep_global',option) ) : the_row(); ?>

                                  <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 hover">
                                      <div class="list-slide-testimonials__item" data-aos="fade-left" data-aos-duration="500">
                                          <div class="list-slide-testimonials__icon" style="background-image: url(<?php the_sub_field('image',option); ?>);"></div>
                                          <div class="list-slide-testimonials__title"><?php the_sub_field('name',option); ?></div>
                                          <div class="list-slide-testimonials__text"><?php the_sub_field('text',option); ?></div>
                                          <svg width="22px" height="20px" viewbox="0 0 612 792"><path fill="#EDC77C" d="M580.563,125.859H389.791c-17.332,0-31.437,14.104-31.437,31.437v190.772                    c0,17.332,14.104,31.437,31.437,31.437h91.322c-1.195,49.964-12.79,89.888-34.783,120.009                    c-17.332,23.668-43.51,43.391-78.652,58.93c-16.137,7.052-23.069,26.177-15.539,42.074l22.592,47.693                    c7.291,15.3,25.341,21.994,40.88,15.3c41.597-17.93,76.619-40.641,105.307-68.252c34.903-33.708,58.81-71.719,71.719-114.152                    S612,380.819,612,307.308V157.296C612,139.964,597.896,125.859,580.563,125.859z"></path><path fill="#EDC77C" d="M66.579,663.392c40.999-17.93,75.902-40.641,104.709-68.133c35.262-33.708,59.288-71.6,72.197-113.675                    c12.909-42.074,19.364-100.167,19.364-174.157V157.296c0-17.332-14.104-31.437-31.437-31.437H40.641                    c-17.212,0-31.317,14.104-31.317,31.437v190.772c0,17.332,14.105,31.437,31.437,31.437h91.322                    c-1.195,49.964-12.79,89.888-34.784,120.009c-17.332,23.668-43.509,43.391-78.651,58.93C2.51,565.495-4.423,584.62,3.108,600.518                    l22.591,47.574C32.871,663.392,51.04,670.205,66.579,663.392z"></path></svg>
                                      </div>
                                  </div>

                              <?php  endwhile;
                            else :
                            endif;
                            ?>


                          <?php endif; ?>

                          <?php if ($case == "custom"): ?>

                              <!-- contact Custom -->
                                <?php
                                if( have_rows('testimonials_rep') ):
                                  while ( have_rows('testimonials_rep') ) : the_row(); ?>

                                      <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 hover">
                                          <div class="list-slide-testimonials__item" data-aos="fade-left" data-aos-duration="500">
                                              <div class="list-slide-testimonials__icon" style="background-image: url(<?php the_sub_field('image'); ?>);"></div>
                                              <div class="list-slide-testimonials__title"><?php the_sub_field('name'); ?></div>
                                              <div class="list-slide-testimonials__text"><?php the_sub_field('text'); ?></div>
                                              <svg width="22px" height="20px" viewbox="0 0 612 792"><path fill="#EDC77C" d="M580.563,125.859H389.791c-17.332,0-31.437,14.104-31.437,31.437v190.772                    c0,17.332,14.104,31.437,31.437,31.437h91.322c-1.195,49.964-12.79,89.888-34.783,120.009                    c-17.332,23.668-43.51,43.391-78.652,58.93c-16.137,7.052-23.069,26.177-15.539,42.074l22.592,47.693                    c7.291,15.3,25.341,21.994,40.88,15.3c41.597-17.93,76.619-40.641,105.307-68.252c34.903-33.708,58.81-71.719,71.719-114.152                    S612,380.819,612,307.308V157.296C612,139.964,597.896,125.859,580.563,125.859z"></path><path fill="#EDC77C" d="M66.579,663.392c40.999-17.93,75.902-40.641,104.709-68.133c35.262-33.708,59.288-71.6,72.197-113.675                    c12.909-42.074,19.364-100.167,19.364-174.157V157.296c0-17.332-14.104-31.437-31.437-31.437H40.641                    c-17.212,0-31.317,14.104-31.317,31.437v190.772c0,17.332,14.105,31.437,31.437,31.437h91.322                    c-1.195,49.964-12.79,89.888-34.784,120.009c-17.332,23.668-43.509,43.391-78.651,58.93C2.51,565.495-4.423,584.62,3.108,600.518                    l22.591,47.574C32.871,663.392,51.04,670.205,66.579,663.392z"></path></svg>
                                          </div>
                                      </div>

                                  <?php  endwhile;
                                else :
                                endif;
                                ?>

                          <?php endif; ?>
                            <!-- SELECT FORM END -->

                        </div>
                        <a class="button button_primary" href="<?php the_sub_field('button_url'); ?>" data-aos="flip-left" data-aos-duration="1500"><?php the_sub_field('button_text'); ?></a></div>
                </section>
        <?php endif;

        elseif( get_row_layout() == 'google_maps' ):
  		  $show = get_sub_field('show_hide');
  		  if ($show == "1"): ?>
          <section class="map-contact">
                      <div class="lines">
                          <div class="container">
                              <div class="row">
                                  <div class="lines-items lines-items lines-items_gray-light">
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="container">
                          <div class="map-contact__title"><?php the_field('google_map_title',option); ?></div>
                          <div class="map-contact__wrapper row">
                              <div class="col-xl-9">
                                  <div class="map" id="map"></div>
                                  <script>
                                      var mapElement = document.getElementById('map');
                                      function mapinit() {
                                          var zoom = 17;
                                          var mapOptions = {
                                              zoom: zoom,
                                              center: new google.maps.LatLng(33.606848, -117.876609),
                                              scrollwheel: false
                                          };
                                          var map = new google.maps.Map(mapElement, mapOptions);
                                          var marker = new google.maps.Marker({
                                              icon: {
                                                  url: 'http://wordpress-158890-481489.cloudwaysapps.com/wp-content/themes/kristinamorales/assets/img/maps-filled-point.png',
                                                  anchor: new google.maps.Point(70, 135)
                                              },
                                              position: {
                                                  lat: 33.606675,
                                                  lng: -117.876250
                                              },
                                              map: map
                                          });
                                          if (window.innerWidth >= 1024) {
                                              map.panBy(150, -60);
                                          }
                                          if (window.innerWidth < 1024) {
                                              map.panBy(-60, -40);
                                          }
                                          if (window.innerWidth < 768) {
                                              map.panBy(30, -20);
                                          }
                                      }
                                  </script>
                                  <script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD41ju8fEBULLIEGvSODoqTUIGcX5nQxA4&amp;callback=mapinit"></script>
                              </div>
                              <div class="wrapper-info">
                                  <div class="wrapper-info__title">Contacts</div>
                                  <div class="wrapper-info__info">
                                      <div class="wrapper-info__item phone">
                                          <div class="wrapper-info__name">Mobile:</div>
                                          <div class="wrapper-info__text"><a href="tel:(310) 625-1567call"><?php the_field('google_map_mobile',option); ?></a></div>
                                      </div>
                                      <div class="wrapper-info__item email">
                                          <div class="wrapper-info__name">Email:</div>
                                          <div class="wrapper-info__text"> <a href="mailto:<?php the_field('google_map_email',option); ?>"><?php the_field('google_map_email',option); ?></a></div>
                                      </div>
                                      <div class="wrapper-info__item address">
                                          <div class="wrapper-info__name">Office Address:</div>
                                          <div class="wrapper-info__text"><?php the_field('google_map_office_address',option); ?></div>
                                      </div>
                                  </div>
                                  <ul class="social">
                                      <li class="social__items"><a class="social__link" href="<?php the_field('google_map_linkedin',option); ?>"> <svg width="17px" height="17px" viewbox="0 0 512 512"><path d="M61.538,0.44C27.531,0.44,0.005,28.015,0,61.945c0,33.962,27.526,61.532,61.543,61.532                  c33.924,0,61.488-27.57,61.488-61.532C123.032,28.01,95.462,0.44,61.538,0.44z"></path><rect x="8.462" y="170.149" , width="106.114" height="341.41"></rect><path d="M384.715,161.66c-51.618,0-86.229,28.301-100.396,55.139h-1.42v-46.65H181.125h-0.005v341.405h106.021                  V342.659c0-44.526,8.479-87.655,63.684-87.655c54.413,0,55.139,50.926,55.139,90.511v166.034H512v-187.26                  C512,232.37,492.166,161.66,384.715,161.66z"></path></svg></a></li>
                                      <li class="social__items"><a class="social__link" href="<?php the_field('google_map_facebook',option); ?>"><svg width="17px" height="27px" viewbox="0 0 612 792"><path d="M459,90.12L379.631,90c-89.17,0-146.784,59.168-146.784,150.609v69.448H153                  c-6.933,0-12.431,5.618-12.431,12.431v100.646c0,6.933,5.618,12.431,12.431,12.431h79.847v253.885                  c0,6.933,5.618,12.432,12.432,12.432H349.39c6.933,0,12.432-5.618,12.432-12.432V435.685h93.354                  c6.933,0,12.432-5.618,12.432-12.432V322.608c0-3.347-1.315-6.455-3.706-8.845s-5.498-3.706-8.845-3.706h-93.354v-58.81                  c0-28.329,6.694-42.672,43.629-42.672h53.431c6.933,0,12.432-5.618,12.432-12.432v-93.593C471.432,95.737,465.813,90.12,459,90.12z"></path></svg></a></li>
                                      <li class="social__items"><a class="social__link" href="<?php the_field('google_map_instagam',option); ?>"><svg width="17px" height="17px" viewbox="0 0 154.333 154.333"><defs><rect id="instagram1" width="154.333" height="154.333"></rect></defs><clippath id="instagram2"><use xlink:href="#instagram1" overflow="visible"></use></clippath><path clip-path="url(#instagram2)" d="M77.166,48.979c-15.542,0-28.187,12.645-28.187,28.187                  c0,15.543,12.645,28.188,28.187,28.188c15.542,0,28.188-12.645,28.188-28.188C105.354,61.624,92.708,48.979,77.166,48.979"></path><path clip-path="url(#instagram2)" d="M108.333,0H46C20.595,0,0,20.595,0,46v62.333c0,25.404,20.595,46,46,46h62.333                  c25.405,0,46-20.596,46-46V46C154.333,20.595,133.738,0,108.333,0 M77.166,119.354c-23.262,0-42.187-18.925-42.187-42.187                  c0-23.263,18.925-42.188,42.187-42.188s42.188,18.925,42.188,42.188C119.354,100.429,100.428,119.354,77.166,119.354                  M123.38,39.604c-4.694,0-8.5-3.805-8.5-8.5c0-4.694,3.806-8.5,8.5-8.5s8.5,3.806,8.5,8.5                  C131.88,35.799,128.074,39.604,123.38,39.604"></path></svg></a></li>
                                  </ul>
                              </div>
                          </div>
                      </div>
           </section>
        <?php endif;

        elseif( get_row_layout() == 'watch_video' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
          <section class="block-watch-video" style="background-image: url(<?php the_sub_field('image'); ?>);">
          <div class="button button_video click-modal-video" data-aos="flip-left" data-aos-delay="2000"><svg width="15px" height="15px" viewbox="26.911 0 458.178 512"><path d="M457.726,208.497L108.994,7.181c-16.619-9.586-38.135-9.56-54.693,0   c-16.889,9.726-27.39,27.896-27.39,47.408v402.658c0,19.521,10.492,37.69,27.286,47.364c8.305,4.836,17.795,7.39,27.442,7.39   c9.621,0,19.103-2.545,27.373-7.347L457.743,303.32c16.871-9.752,27.346-27.913,27.346-47.398   C485.081,236.462,474.614,218.301,457.726,208.497z M431.582,258.057L82.824,459.398c-0.715,0.41-1.569,0.453-2.432-0.043   c-0.732-0.428-1.194-1.238-1.194-2.109V54.58c0-0.872,0.453-1.673,1.22-2.118c0.366-0.218,0.784-0.331,1.211-0.331   c0.444,0,0.862,0.113,1.237,0.331l348.671,201.29c0.775,0.453,1.264,1.281,1.264,2.17   C432.793,256.819,432.34,257.612,431.582,258.057z"></path></svg>
          <?php the_sub_field('button_title'); ?></div>
          </section>
            <div class="modal-video">
            <div class="modal-video__wrapper">
              <div class="modal-video__close">c l o s e</div>
              <div class="modal-video__iframe"><iframe width="100%" height="580" src="https://www.youtube.com/embed/<?php the_sub_field('youtube_id'); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe></div>
            </div>
            <div class="bg-overlay-video"></div>
            </div>
        <?php endif;

        elseif( get_row_layout() == 'i_sell_faster_for_more_money' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
          <section class="information-for-sellers">
            <div class="lines">
              <div class="container">
              <div class="row">
                <div class="lines-items">
                <div class="lines-items__item"></div>
                <div class="lines-items__item"></div>
                <div class="lines-items__item"></div>
                <div class="lines-items__item"></div>
                <div class="lines-items__item"></div>
                </div>
              </div>
              </div>
            </div>
            <div class="container">
              <div class="information-for-sellers__title"><?php the_sub_field('title'); ?></div>
              <div class="information-for-sellers__title-text"><?php the_sub_field('description'); ?></div>
              <div class="information-for-sellers__list row tabs-js tabs-js-accordion">
              <div class="information-for-sellers__left col-xl-6 col-lg-6">
                <div class="row js-tabs-items">
                <div class="col-xl-6 col-lg-6 col-md-6 js-tabs-item active">
                  <div class="information-for-sellers__item js-item"><img class="information-for-sellers__img-list" src="<?php the_sub_field('image_1'); ?>" alt="" role="presentation" />
                  <div class="information-for-sellers__title-list"><?php the_sub_field('title_1'); ?></div>
                  </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 js-tabs-item">
                  <div class="information-for-sellers__item js-item"><img class="information-for-sellers__img-list" src="<?php the_sub_field('image_2'); ?>" alt="" role="presentation" />
                  <div class="information-for-sellers__title-list"><?php the_sub_field('title_2'); ?></div>
                  </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 js-tabs-item">
                  <div class="information-for-sellers__item js-item"><img class="information-for-sellers__img-list" src="<?php the_sub_field('image_3'); ?>" alt="" role="presentation" />
                  <div class="information-for-sellers__title-list"><?php the_sub_field('title_3'); ?></div>
                  </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 js-tabs-item">
                  <div class="information-for-sellers__item js-item"><img class="information-for-sellers__img-list" src="<?php the_sub_field('image_4'); ?>" alt="" role="presentation" />
                  <div class="information-for-sellers__title-list"><?php the_sub_field('title_4'); ?></div>
                  </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 js-tabs-item">
                  <div class="information-for-sellers__item js-item"><img class="information-for-sellers__img-list" src="<?php the_sub_field('image_5'); ?>" alt="" role="presentation" />
                  <div class="information-for-sellers__title-list"><?php the_sub_field('title_5'); ?></div>
                  </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 js-tabs-item">
                  <div class="information-for-sellers__item js-item"><img class="information-for-sellers__img-list" src="<?php the_sub_field('image_6'); ?>" alt="" role="presentation" />
                  <div class="information-for-sellers__title-list"><?php the_sub_field('title_6'); ?></div>
                  </div>
                </div>
                </div>
              </div>
              <div class="information-for-sellers__right col-xl-6 col-lg-6">
                <div class="information-for-sellers__accordion js-accordion">
                <span>
                  <img class="information-for-sellers__img-list-accordion" src="<?php the_sub_field('image_1'); ?>" alt="" role="presentation"/>
                  <div class="information-for-sellers__accordion-title"><?php the_sub_field('title_1'); ?></div>
                </span>
                </div>
                <div
                class="information-for-sellers__block js-tabs-content active">
                <div class="information-for-sellers__title-block"><?php the_sub_field('title_1'); ?></div>
                <div class="information-for-sellers__text">
                  <?php the_sub_field('text_1'); ?>
                </div>
                </div>
                <div class="information-for-sellers__accordion js-accordion"><span>
                  <img class="information-for-sellers__img-list-accordion" src="<?php the_sub_field('image_2'); ?>" alt="" role="presentation"/>
                  <div class="information-for-sellers__accordion-title"><?php the_sub_field('title_2'); ?></div>
                </span>
                </div>
                <div
                class="information-for-sellers__block js-tabs-content">
                <div class="information-for-sellers__title-block"><?php the_sub_field('title_2'); ?></div>
                <div class="information-for-sellers__text">
                  <?php the_sub_field('text_2'); ?>
                </div>
                </div>
                <div class="information-for-sellers__accordion js-accordion">
                <span>
                  <img class="information-for-sellers__img-list-accordion" src="<?php the_sub_field('image_3'); ?>" alt="" role="presentation"/>
                  <div class="information-for-sellers__accordion-title"><?php the_sub_field('title_3'); ?></div>
                </span>
                </div>
                <div
                class="information-for-sellers__block js-tabs-content">
                <div class="information-for-sellers__title-block"><?php the_sub_field('title_3'); ?></div>
                <div class="information-for-sellers__text">
                  <?php the_sub_field('text_3'); ?>
                </div>
                </div>
                <div class="information-for-sellers__accordion js-accordion"><span>
                  <img class="information-for-sellers__img-list-accordion" src="<?php the_sub_field('image_4'); ?>" alt="" role="presentation"/>
                  <div class="information-for-sellers__accordion-title"><?php the_sub_field('title_4'); ?></div>
                </span>
                </div>
                <div class="information-for-sellers__block js-tabs-content">
                <div class="information-for-sellers__title-block"><?php the_sub_field('title_4'); ?></div>
                <div class="information-for-sellers__text">
                  <?php the_sub_field('text_4'); ?>
                </div>
                </div>
                <div class="information-for-sellers__accordion js-accordion"><span>
                  <img class="information-for-sellers__img-list-accordion" src="<?php the_sub_field('image_5'); ?>" alt="" role="presentation"/>
                  <div class="information-for-sellers__accordion-title"><?php the_sub_field('title_5'); ?></div>
                </span>
                </div>
                <div
                class="information-for-sellers__block js-tabs-content">
                <div class="information-for-sellers__title-block"><?php the_sub_field('title_5'); ?></div>
                <div class="information-for-sellers__text">
                  <?php the_sub_field('text_5'); ?>
                </div>
                </div>
                <div class="information-for-sellers__accordion js-accordion"><span>
                  <img class="information-for-sellers__img-list-accordion" src="<?php the_sub_field('image_6'); ?>" alt="" role="presentation"/>
                  <div class="information-for-sellers__accordion-title"><?php the_sub_field('title_6'); ?></div>
                </span>
                </div>
                <div
                class="information-for-sellers__block js-tabs-content">
                <div class="information-for-sellers__title-block"><?php the_sub_field('title_6'); ?></div>
                <div class="information-for-sellers__text">
                  <?php the_sub_field('text_6'); ?>
                </div>
                </div>
              </div>
              </div>
            </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'volum_list' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
          <section class="information-for-sellers-2">
          <div class="lines">
            <div class="container">
            <div class="row">
              <div class="lines-items lines-items lines-items_gray-light">
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              </div>
            </div>
            </div>
          </div>
          <div class="container">
            <div class="block-title">
            <div class="block-title__title"><?php the_sub_field('title'); ?></div>
            <div class="block-title__title-text"><?php the_sub_field('description'); ?></div>
            </div>
            <div class="information-for-sellers-2__statistics-block tabs-js">
            <div class="information-for-sellers-2__tabs js-tabs-items">
              <div class="information-for-sellers-2__tabs-items js-tabs-item active"><span><?php the_sub_field('left_title'); ?></span></div>
              <div class="information-for-sellers-2__tabs-items js-tabs-item"> <span><?php the_sub_field('right_text'); ?></span></div>
            </div>
            <div class="information-for-sellers-2__content">
              <div class="information-for-sellers-2__tabs-content js-tabs-content active">
              <div class="information-for-sellers-2__title-tabs"><?php the_sub_field('left_description'); ?></div>
              <div class="information-for-sellers-2__text">
                <div class="progress-bar">
                <div class="progress-bar__progress-header">
                  <div class="progress-bar__progress-title"><?php the_sub_field('text_1'); ?></div>
                  <div class="progress-bar__progress-value"> <input id="progress-value" readonly>%</div>
                </div><progress id="progress" max="100" value="<?php the_sub_field('value_1'); ?>"></progress></div>
                <div class="progress-bar">
                <div class="progress-bar__progress-header">
                  <div class="progress-bar__progress-title"><?php the_sub_field('text_2'); ?></div>
                  <div class="progress-bar__progress-value"><input id="progress-value2" readonly>%</div>
                </div><progress id="progress2" max="100" value="<?php the_sub_field('value_2'); ?>"></progress></div>
                <div class="progress-bar">
                <div class="progress-bar__progress-header">
                  <div class="progress-bar__progress-title"><?php the_sub_field('text_3'); ?></div>
                  <div class="progress-bar__progress-value"><input id="progress-value3" readonly>%</div>
                </div><progress id="progress3" max="100" value="<?php the_sub_field('value_3'); ?>"></progress></div>
              </div>
              </div>
              <div class="information-for-sellers-2__tabs-content js-tabs-content">
              <div class="information-for-sellers-2__title-tabs"><?php the_sub_field('right_description'); ?></div>
              <div class="information-for-sellers-2__text">
                <div class="progress-bar">
                <div class="progress-bar__progress-header">
                  <div class="progress-bar__progress-title"><?php the_sub_field('text_4'); ?></div>
                  <div class="progress-bar__progress-value"> <input id="progress-value" readonly>%</div>
                </div><progress id="progress" max="100" value="<?php the_sub_field('value_4'); ?>"></progress></div>
                <div class="progress-bar">
                <div class="progress-bar__progress-header">
                  <div class="progress-bar__progress-title"><?php the_sub_field('text_5'); ?></div>
                  <div class="progress-bar__progress-value"><input id="progress-value2" readonly>%</div>
                </div><progress id="progress2" max="100" value="<?php the_sub_field('value_5'); ?>"></progress></div>
                <div class="progress-bar">
                <div class="progress-bar__progress-header">
                  <div class="progress-bar__progress-title"><?php the_sub_field('text_6'); ?></div>
                  <div class="progress-bar__progress-value"><input id="progress-value3" readonly>%</div>
                </div><progress id="progress3" max="100" value="<?php the_sub_field('value_6'); ?>"></progress></div>
              </div>
              </div>
            </div>
            </div>
          </div>
          </section>
        <?php endif;

        elseif( get_row_layout() == 'hire_block' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
          <section class="why-hire-me">
          <div class="lines">
            <div class="container">
            <div class="row">
              <div class="lines-items">
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              </div>
            </div>
            </div>
          </div>
          <div class="container">
            <div class="block-title">
            <div class="block-title__title"><?php the_sub_field('title'); ?></div>
            <div class="block-title__title-text"><?php the_sub_field('description'); ?></div>
            </div>
            <div class="list-why-hire-me tabs-js">
            <div class="list-why-hire-me__tabs js-tabs-items">
              <div class="list-why-hire-me__tabs-items js-tabs-item active"><span><?php the_sub_field('title_1'); ?></span></div>
              <div class="list-why-hire-me__tabs-items js-tabs-item"><span><?php the_sub_field('title_2'); ?></span></div>
              <div class="list-why-hire-me__tabs-items js-tabs-item"><span><?php the_sub_field('title_3'); ?></span></div>
              <div class="list-why-hire-me__tabs-items js-tabs-item"><span><?php the_sub_field('title_4'); ?></span></div>
            </div>
            <div class="list-why-hire-me__content">
              <div class="list-why-hire-me__tabs-content js-tabs-content active">
              <div class="list-why-hire-me__title-tabs"><?php the_sub_field('title_1'); ?></div>
              <div class="list-why-hire-me__text">
                <?php the_sub_field('text_1'); ?>
              </div>
              </div>
              <div class="list-why-hire-me__tabs-content js-tabs-content">
              <div class="list-why-hire-me__title-tabs"><?php the_sub_field('title_2'); ?></div>
              <div class="list-why-hire-me__text">
                <?php the_sub_field('text_2'); ?>
              </div>
              </div>
              <div class="list-why-hire-me__tabs-content js-tabs-content">
              <div class="list-why-hire-me__title-tabs"><?php the_sub_field('title_3'); ?></div>
              <div class="list-why-hire-me__text">
                <?php the_sub_field('text_3'); ?>
              </div>
              </div>
              <div class="list-why-hire-me__tabs-content js-tabs-content">
              <div class="list-why-hire-me__title-tabs"><?php the_sub_field('title_4'); ?></div>
              <div class="list-why-hire-me__text">
                <?php the_sub_field('text_4'); ?>
              </div>
              </div>
            </div>
            </div>
          </div>
          </section>
        <?php endif;

        elseif( get_row_layout() == 'partners_block' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
          <section class="partnership">
          <div class="lines">
            <div class="container">
            <div class="row">
              <div class="lines-items">
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              </div>
            </div>
            </div>
          </div>
          <div class="container">
            <div class="block-title">
            <div class="block-title__title"><?php the_sub_field('title'); ?></div>
            <div class="block-title__title-text"><?php the_sub_field('description'); ?></div>
            </div>
            <div class="list-slide-partnership row">
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12"><a class="list-slide-partnership__icon" href="#"><img class="list-slide-partnership__img" src="<?php the_sub_field('image_1'); ?>" alt="" role="presentation"/></a></div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12"><a class="list-slide-partnership__icon" href="#"><img class="list-slide-partnership__img" src="<?php the_sub_field('image_2'); ?>" alt="" role="presentation"/></a></div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12"><a class="list-slide-partnership__icon" href="#"><img class="list-slide-partnership__img" src="<?php the_sub_field('image_3'); ?>" alt="" role="presentation"/></a></div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12"><a class="list-slide-partnership__icon" href="#"><img class="list-slide-partnership__img" src="<?php the_sub_field('image_4'); ?>" alt="" role="presentation"/></a></div>
            </div>
          </div>
          </section>
        <?php endif;

        elseif( get_row_layout() == 'record_sales' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
            <section class="record-sales">
            <div class="lines">
              <div class="container">
              <div class="row">
                <div class="lines-items lines-items lines-items_gray-light">
                <div class="lines-items__item"></div>
                <div class="lines-items__item"></div>
                <div class="lines-items__item"></div>
                <div class="lines-items__item"></div>
                <div class="lines-items__item"></div>
                </div>
              </div>
              </div>
            </div>
            <div class="container">
              <div class="block-title">
              <div class="block-title__title"><?php the_sub_field('title'); ?></div>
              <div class="block-title__title-text"><?php the_sub_field('description'); ?></div>
              </div>
              <div class="list-slide-record-sales">

              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                <div class="list-slide-record-sales__item">
                <div class="list-slide-record-sales__top"><img class="list-slide-record-sales__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png" alt="" role="presentation" />
                  <div class="list-slide-record-sales__hover"><a class="list-slide-record-sales__title" href="#">AVA Nob Hill</a>
                  <div class="list-slide-record-sales__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                    Main street. Newport Beach CA 92660</div>
                  </div>
                </div>
                <div class="list-slide-record-sales__bottom">
                  <div class="list-slide-record-sales__price"><span>$1,199.00/mon</span></div>
                  <div class="list-slide-record-sales__info-item">
                  <div class="list-slide-record-sales__block">
                    <div class="list-slide-record-sales__name">Area</div>
                    <div class="list-slide-record-sales__info-block">2,100 sq. ft.</div>
                  </div>
                  <div class="list-slide-record-sales__block">
                    <div class="list-slide-record-sales__name">Bedrooms</div>
                    <div class="list-slide-record-sales__info-block">2</div>
                  </div>
                  <div class="list-slide-record-sales__block">
                    <div class="list-slide-record-sales__name">Bathrooms</div>
                    <div class="list-slide-record-sales__info-block">1</div>
                  </div>
                  <div class="list-slide-record-sales__block">
                    <div class="list-slide-record-sales__name">Garages</div>
                    <div class="list-slide-record-sales__info-block">2</div>
                  </div>
                  </div>
                </div>
                </div>
              </div>

              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                <div class="list-slide-record-sales__item">
                <div class="list-slide-record-sales__top"><img class="list-slide-record-sales__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png" alt="" role="presentation" />
                  <div class="list-slide-record-sales__hover"><a class="list-slide-record-sales__title" href="#">AVA Nob Hill</a>
                  <div class="list-slide-record-sales__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                    Main street. Newport Beach CA 92660</div>
                  </div>
                </div>
                <div class="list-slide-record-sales__bottom">
                  <div class="list-slide-record-sales__price"><span>$1,199.00/mon</span></div>
                  <div class="list-slide-record-sales__info-item">
                  <div class="list-slide-record-sales__block">
                    <div class="list-slide-record-sales__name">Area</div>
                    <div class="list-slide-record-sales__info-block">2,100 sq. ft.</div>
                  </div>
                  <div class="list-slide-record-sales__block">
                    <div class="list-slide-record-sales__name">Bedrooms</div>
                    <div class="list-slide-record-sales__info-block">2</div>
                  </div>
                  <div class="list-slide-record-sales__block">
                    <div class="list-slide-record-sales__name">Bathrooms</div>
                    <div class="list-slide-record-sales__info-block">1</div>
                  </div>
                  <div class="list-slide-record-sales__block">
                    <div class="list-slide-record-sales__name">Garages</div>
                    <div class="list-slide-record-sales__info-block">2</div>
                  </div>
                  </div>
                </div>
                </div>
              </div>

              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                <div class="list-slide-record-sales__item">
                <div class="list-slide-record-sales__top"><img class="list-slide-record-sales__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png" alt="" role="presentation" />
                  <div class="list-slide-record-sales__hover"><a class="list-slide-record-sales__title" href="#">AVA Nob Hill</a>
                  <div class="list-slide-record-sales__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                    Main street. Newport Beach CA 92660</div>
                  </div>
                </div>
                <div class="list-slide-record-sales__bottom">
                  <div class="list-slide-record-sales__price"><span>$1,199.00/mon</span></div>
                  <div class="list-slide-record-sales__info-item">
                  <div class="list-slide-record-sales__block">
                    <div class="list-slide-record-sales__name">Area</div>
                    <div class="list-slide-record-sales__info-block">2,100 sq. ft.</div>
                  </div>
                  <div class="list-slide-record-sales__block">
                    <div class="list-slide-record-sales__name">Bedrooms</div>
                    <div class="list-slide-record-sales__info-block">2</div>
                  </div>
                  <div class="list-slide-record-sales__block">
                    <div class="list-slide-record-sales__name">Bathrooms</div>
                    <div class="list-slide-record-sales__info-block">1</div>
                  </div>
                  <div class="list-slide-record-sales__block">
                    <div class="list-slide-record-sales__name">Garages</div>
                    <div class="list-slide-record-sales__info-block">2</div>
                  </div>
                  </div>
                </div>
                </div>
              </div>

              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                <div class="list-slide-record-sales__item">
                <div class="list-slide-record-sales__top"><img class="list-slide-record-sales__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png" alt="" role="presentation" />
                  <div class="list-slide-record-sales__hover"><a class="list-slide-record-sales__title" href="#">AVA Nob Hill</a>
                  <div class="list-slide-record-sales__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                    Main street. Newport Beach CA 92660</div>
                  </div>
                </div>
                <div class="list-slide-record-sales__bottom">
                  <div class="list-slide-record-sales__price"><span>$1,199.00/mon</span></div>
                  <div class="list-slide-record-sales__info-item">
                  <div class="list-slide-record-sales__block">
                    <div class="list-slide-record-sales__name">Area</div>
                    <div class="list-slide-record-sales__info-block">2,100 sq. ft.</div>
                  </div>
                  <div class="list-slide-record-sales__block">
                    <div class="list-slide-record-sales__name">Bedrooms</div>
                    <div class="list-slide-record-sales__info-block">2</div>
                  </div>
                  <div class="list-slide-record-sales__block">
                    <div class="list-slide-record-sales__name">Bathrooms</div>
                    <div class="list-slide-record-sales__info-block">1</div>
                  </div>
                  <div class="list-slide-record-sales__block">
                    <div class="list-slide-record-sales__name">Garages</div>
                    <div class="list-slide-record-sales__info-block">2</div>
                  </div>
                  </div>
                </div>
                </div>
              </div>

              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                <div class="list-slide-record-sales__item">
                <div class="list-slide-record-sales__top"><img class="list-slide-record-sales__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png" alt="" role="presentation" />
                  <div class="list-slide-record-sales__hover"><a class="list-slide-record-sales__title" href="#">AVA Nob Hill</a>
                  <div class="list-slide-record-sales__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                    Main street. Newport Beach CA 92660</div>
                  </div>
                </div>
                <div class="list-slide-record-sales__bottom">
                  <div class="list-slide-record-sales__price"><span>$1,199.00/mon</span></div>
                  <div class="list-slide-record-sales__info-item">
                  <div class="list-slide-record-sales__block">
                    <div class="list-slide-record-sales__name">Area</div>
                    <div class="list-slide-record-sales__info-block">2,100 sq. ft.</div>
                  </div>
                  <div class="list-slide-record-sales__block">
                    <div class="list-slide-record-sales__name">Bedrooms</div>
                    <div class="list-slide-record-sales__info-block">2</div>
                  </div>
                  <div class="list-slide-record-sales__block">
                    <div class="list-slide-record-sales__name">Bathrooms</div>
                    <div class="list-slide-record-sales__info-block">1</div>
                  </div>
                  <div class="list-slide-record-sales__block">
                    <div class="list-slide-record-sales__name">Garages</div>
                    <div class="list-slide-record-sales__info-block">2</div>
                  </div>
                  </div>
                </div>
                </div>
              </div>

              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                <div class="list-slide-record-sales__item">
                <div class="list-slide-record-sales__top"><img class="list-slide-record-sales__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png" alt="" role="presentation" />
                  <div class="list-slide-record-sales__hover"><a class="list-slide-record-sales__title" href="#">AVA Nob Hill</a>
                  <div class="list-slide-record-sales__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                    Main street. Newport Beach CA 92660</div>
                  </div>
                </div>
                <div class="list-slide-record-sales__bottom">
                  <div class="list-slide-record-sales__price"><span>$1,199.00/mon</span></div>
                  <div class="list-slide-record-sales__info-item">
                  <div class="list-slide-record-sales__block">
                    <div class="list-slide-record-sales__name">Area</div>
                    <div class="list-slide-record-sales__info-block">2,100 sq. ft.</div>
                  </div>
                  <div class="list-slide-record-sales__block">
                    <div class="list-slide-record-sales__name">Bedrooms</div>
                    <div class="list-slide-record-sales__info-block">2</div>
                  </div>
                  <div class="list-slide-record-sales__block">
                    <div class="list-slide-record-sales__name">Bathrooms</div>
                    <div class="list-slide-record-sales__info-block">1</div>
                  </div>
                  <div class="list-slide-record-sales__block">
                    <div class="list-slide-record-sales__name">Garages</div>
                    <div class="list-slide-record-sales__info-block">2</div>
                  </div>
                  </div>
                </div>
                </div>
              </div>

              </div>
            </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'strategy' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
          <section class="selling-strategy">
          <div class="lines">
            <div class="container">
            <div class="row">
              <div class="lines-items">
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              </div>
            </div>
            </div>
          </div>
          <div class="container">
            <div class="block-title">
            <div class="block-title__title"><?php the_sub_field('title'); ?></div>
            <div class="block-title__title-text"><?php the_sub_field('descriotion'); ?></div>
            </div>
            <div class="selling-strategy__list-top row">
            <div class="selling-strategy__item col-xl-3 col-lg-3 col-md-3 col-sm-6">
              <img class="selling-strategy__img" src="<?php the_sub_field('image_1'); ?>" alt="" role="presentation" />
              <div class="selling-strategy__sub-title">step 1</div>
              <div class="selling-strategy__title"><?php the_sub_field('text_1'); ?></div>
            </div>
            <div class="selling-strategy__item col-xl-3 col-lg-3 col-md-3 col-sm-6">
              <img class="selling-strategy__img" src="<?php the_sub_field('image_2'); ?>" alt="" role="presentation" />
              <div class="selling-strategy__sub-title">step 2</div>
              <div class="selling-strategy__title"><?php the_sub_field('text_2'); ?></div>
            </div>
            <div class="selling-strategy__item col-xl-3 col-lg-3 col-md-3 col-sm-6">
              <img class="selling-strategy__img" src="<?php the_sub_field('image_3'); ?>" alt="" role="presentation" />
              <div class="selling-strategy__sub-title">step 3</div>
              <div class="selling-strategy__title"><?php the_sub_field('text_3'); ?></div>
            </div>
            <div class="selling-strategy__item col-xl-3 col-lg-3 col-md-3 col-sm-6">
              <img class="selling-strategy__img" src="<?php the_sub_field('image_4'); ?>" alt="" role="presentation" />
              <div class="selling-strategy__sub-title">step 4</div>
              <div class="selling-strategy__title"><?php the_sub_field('text_4'); ?></div>
            </div>
            </div>
            <div class="selling-strategy__list-bottom row">
            <div class="selling-strategy__item col-xl-3 col-lg-3 col-md-3 col-sm-6">
              <img class="selling-strategy__img" src="<?php the_sub_field('image_8'); ?>" alt="" role="presentation" />
              <div class="selling-strategy__sub-title">step 8</div>
              <div class="selling-strategy__title"><?php the_sub_field('text_8'); ?></div>
            </div>
            <div class="selling-strategy__item col-xl-3 col-lg-3 col-md-3 col-sm-6">
              <img class="selling-strategy__img" src="<?php the_sub_field('image_7'); ?>" alt="" role="presentation" />
              <div class="selling-strategy__sub-title">step 7</div>
              <div class="selling-strategy__title"><?php the_sub_field('text_7'); ?></div>
            </div>
            <div class="selling-strategy__item col-xl-3 col-lg-3 col-md-3 col-sm-6">
              <img class="selling-strategy__img" src="<?php the_sub_field('image_6'); ?>" alt="" role="presentation" />
              <div class="selling-strategy__sub-title">step 6</div>
              <div class="selling-strategy__title"><?php the_sub_field('text_6'); ?></div>
            </div>
            <div class="selling-strategy__item col-xl-3 col-lg-3 col-md-3 col-sm-6">
              <img class="selling-strategy__img" src="<?php the_sub_field('image_5'); ?>" alt="" role="presentation" />
              <div class="selling-strategy__sub-title">step 5</div>
              <div class="selling-strategy__title"><?php the_sub_field('text_5'); ?></div>
            </div>
            </div>
          </div>
          </section>
        <?php endif;

        elseif( get_row_layout() == 'advantage' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
          <section class="buyers-advantage">
          <div class="lines">
            <div class="container">
            <div class="row">
              <div class="lines-items lines-items lines-items_gray-light">
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              </div>
            </div>
            </div>
          </div>
          <div class="container">
            <div class="block-title">
            <div class="block-title__title"><?php the_sub_field('title'); ?></div>
            <div class="block-title__title-text"><?php the_sub_field('description'); ?></div>
            </div>
            <div class="list row">

            <?php
            if( have_rows('luxury') ):
              while ( have_rows('luxury') ) : the_row(); ?>

              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                <div class="list__item" data-aos="fade-left" data-aos-duration="500">
                <div class="list__block">
                  <div class="list__title"><?php the_sub_field('title'); ?></div>
                  <div class="list__chart"><?php the_sub_field('number'); ?>.</div>
                </div>
                <div class="list__text"><?php the_sub_field('text'); ?></div>
                </div>
              </div>

              <?php  endwhile;
            else :
            endif;
            ?>

            </div>
          </div>
          </section>
        <?php endif;

        elseif( get_row_layout() == 'buying_strategy' ):
        $show = get_sub_field('show_hide');
        if ($show == "1"): ?>
          <section class="buying-strategy">
          <div class="lines">
            <div class="container">
            <div class="row">
              <div class="lines-items">
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              <div class="lines-items__item"></div>
              </div>
            </div>
            </div>
          </div>
          <div class="container">
            <div class="block-title">
            <div class="block-title__title"><?php the_sub_field('title'); ?></div>
            <div class="block-title__title-text"><?php the_sub_field('description'); ?></div>
            </div>
            <div class="buying-strategy__list row">
            <div class="buying-strategy__item col-xl-2 col-lg-2 col-md-6 col-sm-6">
                    <img class="buying-strategy__img" src="<?php the_sub_field('image_1'); ?>" alt="" role="presentation" />
              <div class="buying-strategy__sub-title">step 1</div>
              <div class="buying-strategy__title"><?php the_sub_field('text_1'); ?></div>
            </div>
            <div class="buying-strategy__item col-xl-2 col-lg-2 col-md-6 col-sm-6">
                    <img class="buying-strategy__img" src="<?php the_sub_field('image_2'); ?>" alt="" role="presentation" />
              <div class="buying-strategy__sub-title">step 2</div>
              <div class="buying-strategy__title"><?php the_sub_field('text_2'); ?></div>
            </div>
            <div class="buying-strategy__item col-xl-2 col-lg-2 col-md-6 col-sm-6">
                    <img class="buying-strategy__img" src="<?php the_sub_field('image_3'); ?>" alt="" role="presentation" />
              <div class="buying-strategy__sub-title">step 3</div>
              <div class="buying-strategy__title"><?php the_sub_field('text_3'); ?></div>
            </div>
            <div class="buying-strategy__item col-xl-2 col-lg-2 col-md-6 col-sm-6">
                    <img class="buying-strategy__img" src="<?php the_sub_field('image_4'); ?>" alt="" role="presentation" />
              <div class="buying-strategy__sub-title">step 4</div>
              <div class="buying-strategy__title"><?php the_sub_field('text_4'); ?></div>
            </div>
            <div class="buying-strategy__item col-xl-2 col-lg-2 col-md-6 col-sm-6">
                    <img class="buying-strategy__img" src="<?php the_sub_field('image_5'); ?>" alt="" role="presentation" />
              <div class="buying-strategy__sub-title">step 5</div>
              <div class="buying-strategy__title"><?php the_sub_field('text_5'); ?></div>
            </div>
            <div class="buying-strategy__item col-xl-2 col-lg-2 col-md-6 col-sm-6">
                    <img class="buying-strategy__img" src="<?php the_sub_field('image_6'); ?>" alt="" role="presentation" />
              <div class="buying-strategy__sub-title">step 6</div>
              <div class="buying-strategy__title"><?php the_sub_field('text_6'); ?></div>
            </div>
            </div>
          </div>
          </section>
      <?php endif;

        elseif( get_row_layout() == 'breadcrumb2' ):
		$show = get_sub_field('show_hide');
		  if ($show == "1"): ?>
        <section class="breadcrumbs">
            <div class="lines">
              <div class="container">
                <div class="row">
                  <div class="lines-items lines-items lines-items_gray-light">
                    <div class="lines-items__item"></div>
                    <div class="lines-items__item"></div>
                    <div class="lines-items__item"></div>
                    <div class="lines-items__item"></div>
                    <div class="lines-items__item"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="container">
              <div class="breadcrumbs__wrapper">
                <div class="breadcrumbs__title-block">
                  <div class="breadcrumbs__title" data-aos="fade-down" data-aos-delay="800"><?php the_sub_field('title'); ?></div>
                  <div class="breadcrumbs__sub-title" data-aos="fade-up" data-aos-delay="300"><?php the_sub_field('description'); ?></div>
                </div>
                <div class="breadcrumbs__nav">
                  <div class="breadcrumbs__item"><a href="<?php echo site_url(); ?>">home</a></div>
                  <div class="breadcrumbs__item"> <span><?php the_sub_field('title'); ?></span></div>
                </div>
              </div>
            </div>
          </section>
        <?php endif;

        elseif( get_row_layout() == 'my_team' ):
		$show = get_sub_field('show_hide');
		if ($show == "1"): ?>
           <section class="my-team">
            <div class="lines">
              <div class="container">
                <div class="row">
                  <div class="lines-items">
                    <div class="lines-items__item"></div>
                    <div class="lines-items__item"></div>
                    <div class="lines-items__item"></div>
                    <div class="lines-items__item"></div>
                    <div class="lines-items__item"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="container">
              <div class="my-team__title"><?php the_sub_field('title'); ?></div>
              <div class="my-team__title-text"><?php the_sub_field('description'); ?></div>
              <div class="list-slide-my-team row">

                <?php
                if( have_rows('meet_repeater') ):
                  while ( have_rows('meet_repeater') ) : the_row(); ?>

                      <div class="col-xl-3 col-lg-4">
                          <div class="block-user-info"><img class="block-user-info__img" src="<?php the_sub_field('image'); ?>" alt="" role="presentation" />
                              <div class="block-user-info__name"><?php the_sub_field('name'); ?></div><a class="block-user-info__phone" href="tel:<?php the_sub_field('phone'); ?>call"> <svg width="10px" height="16px" viewBox="111.031 0 289.938 512">
                                      <path d="M363.71,0H148.29c-20.617,0-37.259,16.717-37.259,37.296v437.445c0,20.561,16.643,37.259,37.259,37.259h215.42c20.579,0,37.259-16.698,37.259-37.259V37.296C400.969,16.717,384.289,0,363.71,0z M202.845,22.65h106.348c2.687,0,4.87,4.011,4.87,8.974s-2.184,8.993-4.87,8.993H202.845c-2.706,0-4.851-4.03-4.851-8.993S200.139,22.65,202.845,22.65zM256.019,475.188c-13.116,0-23.788-10.672-23.788-23.807s10.672-23.77,23.788-23.77c13.079,0,23.751,10.635,23.751,23.77   S269.098,475.188,256.019,475.188z M373.058,393.674H138.961V62.932h234.096V393.674L373.058,393.674z"/>
                                  </svg><?php the_sub_field('phone'); ?></a><a class="button button_primary button_full button_high" href="#">contact us</a></div>
                      </div>

                  <?php  endwhile;
                else :
                endif;
                ?>

              </div>
            </div>
          </section>
        <?php endif;

        elseif( get_row_layout() == 'block_3_tabs' ):
		$show = get_sub_field('show_hide');
		if ($show == "1"): ?>
            <section class="tabs-information">
            <div class="lines">
              <div class="container">
                <div class="row">
                  <div class="lines-items lines-items lines-items_gray-light">
                    <div class="lines-items__item"></div>
                    <div class="lines-items__item"></div>
                    <div class="lines-items__item"></div>
                    <div class="lines-items__item"></div>
                    <div class="lines-items__item"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="container">
              <div class="tabs-information__list tabs-js tabs-js-accordion">
                <div class="tabs-information__tabs js-tabs-items">
                  <div class="tabs-information__tabs-items js-tabs-item active"><span><?php the_sub_field('title_1'); ?></span></div>
                  <div class="tabs-information__tabs-items js-tabs-item"> <span><?php the_sub_field('title_2'); ?></span></div>
                  <div class="tabs-information__tabs-items js-tabs-item"> <span><?php the_sub_field('title_3'); ?></span></div>
                </div>
                <div class="tabs-information__content">
                  <div class="tabs-information__accordion js-accordion"><span><?php the_sub_field('title_1'); ?></span></div>
                  <div class="tabs-information__tabs-content js-tabs-content active">
                    <div class="tabs-information__text">
                      <?php the_sub_field('text_1'); ?>
                    </div>
                  </div>
                  <div class="tabs-information__accordion js-accordion"><span><?php the_sub_field('title_2'); ?></span></div>
                  <div class="tabs-information__tabs-content js-tabs-content">
                    <div class="tabs-information__text">
                      <?php the_sub_field('text_2'); ?>
                    </div>
                  </div>
                  <div class="tabs-information__accordion js-accordion"><span><?php the_sub_field('title_3'); ?></span></div>
                  <div class="tabs-information__tabs-content js-tabs-content">
                    <div class="tabs-information__text">
                      <?php the_sub_field('text_3'); ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        <?php endif;

        elseif( get_row_layout() == 'neighborhood_tab' ):
		$show = get_sub_field('show_hide');
		if ($show == "1"): ?>

		<?php $active = get_sub_field('select_active_tab'); ?>
		<style> .tab-neighborhood {height: 63px;!important}</style>
              <section class="tab-neighborhood">
                <div class="container">
                    <div class="tab-neighborhood__items">
                        <a class="tab-neighborhood__item <?php if ($active == "overview"): ?>  active  <?php endif; ?> " href="<?php the_sub_field('url_overview'); ?>">Overview</a>
                        <a class="tab-neighborhood__item <?php if ($active == "schools"): ?> active   <?php endif; ?>" href="<?php the_sub_field('url_schools'); ?>">Schools</a>
                        <a class="tab-neighborhood__item <?php if ($active == "realestate"): ?>  active  <?php endif; ?>"  href="<?php the_sub_field('url_real_estate'); ?>">Real Estate</a>
                        <a class="tab-neighborhood__item <?php if ($active == "things"): ?>  active  <?php endif; ?>" href="<?php the_sub_field('url_things_to_do'); ?>">Things to Do</a>
                    </div>
                </div>
              </section>
          <?php endif;

        elseif( get_row_layout() == 'schools_block' ):
		$show = get_sub_field('show_hide');
		if ($show == "1"): ?>
                <section class="public-schools" style=" background-color: <?php the_sub_field('background_color'); ?>">
                        <div class="lines">
                            <div class="container">
                                <div class="row">
                                    <div class="lines-items lines-items lines-items_gray-light">
                                        <div class="lines-items__item"></div>
                                        <div class="lines-items__item"></div>
                                        <div class="lines-items__item"></div>
                                        <div class="lines-items__item"></div>
                                        <div class="lines-items__item"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="public-schools__title"><?php the_sub_field('title'); ?></div>
                            <div class="block-title-text text-show"><?php the_sub_field('description'); ?></div>
                            <div class="list-public-schools">
                                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                    <div class="list-public-schools__item">
                                        <div class="list-public-schools__top"><img class="list-public-schools__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png" alt="" role="presentation" />
                                            <div class="list-public-schools__block-info">
                                                <div class="list-public-schools__block-info-title">10</div>
                                                <div class="list-public-schools__block-info-text">GreatSchools Rating</div>
                                            </div>
                                        </div>
                                        <div class="list-public-schools__bottom"><a class="list-public-schools__title" href="#">Adam Elementary School</a>
                                            <div class="list-public-schools__title-info">Address:</div>
                                            <div class="list-public-schools__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                            <div class="list-public-schools__title-info">Phone:</div><a class="list-public-schools__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                            <div class="list-public-schools__title-info">Website:</div><a class="list-public-schools__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                    <div class="list-public-schools__item">
                                        <div class="list-public-schools__top"><img class="list-public-schools__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png" alt="" role="presentation" />
                                            <div class="list-public-schools__block-info">
                                                <div class="list-public-schools__block-info-title">10</div>
                                                <div class="list-public-schools__block-info-text">GreatSchools Rating</div>
                                            </div>
                                        </div>
                                        <div class="list-public-schools__bottom"><a class="list-public-schools__title" href="#">Adam Elementary School</a>
                                            <div class="list-public-schools__title-info">Address:</div>
                                            <div class="list-public-schools__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                            <div class="list-public-schools__title-info">Phone:</div><a class="list-public-schools__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                            <div class="list-public-schools__title-info">Website:</div><a class="list-public-schools__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                    <div class="list-public-schools__item">
                                        <div class="list-public-schools__top"><img class="list-public-schools__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png" alt="" role="presentation" />
                                            <div class="list-public-schools__block-info">
                                                <div class="list-public-schools__block-info-title">10</div>
                                                <div class="list-public-schools__block-info-text">GreatSchools Rating</div>
                                            </div>
                                        </div>
                                        <div class="list-public-schools__bottom"><a class="list-public-schools__title" href="#">Adam Elementary School</a>
                                            <div class="list-public-schools__title-info">Address:</div>
                                            <div class="list-public-schools__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                            <div class="list-public-schools__title-info">Phone:</div><a class="list-public-schools__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                            <div class="list-public-schools__title-info">Website:</div><a class="list-public-schools__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                    <div class="list-public-schools__item">
                                        <div class="list-public-schools__top"><img class="list-public-schools__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png" alt="" role="presentation" />
                                            <div class="list-public-schools__block-info">
                                                <div class="list-public-schools__block-info-title">10</div>
                                                <div class="list-public-schools__block-info-text">GreatSchools Rating</div>
                                            </div>
                                        </div>
                                        <div class="list-public-schools__bottom"><a class="list-public-schools__title" href="#">Adam Elementary School</a>
                                            <div class="list-public-schools__title-info">Address:</div>
                                            <div class="list-public-schools__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                            <div class="list-public-schools__title-info">Phone:</div><a class="list-public-schools__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                            <div class="list-public-schools__title-info">Website:</div><a class="list-public-schools__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                    <div class="list-public-schools__item">
                                        <div class="list-public-schools__top"><img class="list-public-schools__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png" alt="" role="presentation" />
                                            <div class="list-public-schools__block-info">
                                                <div class="list-public-schools__block-info-title">10</div>
                                                <div class="list-public-schools__block-info-text">GreatSchools Rating</div>
                                            </div>
                                        </div>
                                        <div class="list-public-schools__bottom"><a class="list-public-schools__title" href="#">Adam Elementary School</a>
                                            <div class="list-public-schools__title-info">Address:</div>
                                            <div class="list-public-schools__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                            <div class="list-public-schools__title-info">Phone:</div><a class="list-public-schools__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                            <div class="list-public-schools__title-info">Website:</div><a class="list-public-schools__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
        <?php endif;

        elseif( get_row_layout() == 'board_of_education' ):
		$show = get_sub_field('show_hide');
		if ($show == "1"): ?>
            <section class="board-of-education">
                <div class="lines">
                    <div class="container">
                        <div class="row">
                            <div class="lines-items lines-items lines-items_gray-light">
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="board-of-education__title"><?php the_sub_field('title'); ?></div>
                    <div class="block-title-text text-show"><?php the_sub_field('description'); ?></div>
                    <div class="row">
                        <div class="col-xl-5"><img class="board-of-education__img" src="<?php the_sub_field('image'); ?>" alt="" role="presentation" /></div>
                        <div class="col-xl-7">
                            <div class="list-board-of-education tabs-js">
                                <div class="list-board-of-education__tabs js-tabs-items">
                                    <div class="list-board-of-education__tabs-items js-tabs-item"><span><?php the_sub_field('title_1'); ?></span></div>
                                    <div class="list-board-of-education__tabs-items js-tabs-item"> <span><?php the_sub_field('title_2'); ?></span></div>
                                    <div class="list-board-of-education__tabs-items js-tabs-item"> <span><?php the_sub_field('title_3'); ?></span></div>
                                    <div class="list-board-of-education__tabs-items js-tabs-item active"><span><?php the_sub_field('title_4'); ?></span></div>
                                </div>
                                <div class="list-board-of-education__content">
                                    <div class="list-board-of-education__tabs-content js-tabs-content">
                                        <div class="list-board-of-education__text">
                                            <p><?php the_sub_field('text_1'); ?></p>
                                        </div>
                                        <div class="list-board-of-education__info">
                                            <div class="list-board-of-education__info-block">
                                                <div class="list-board-of-education__title-info"> <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 512 512"><path d="M500.521,404.63l-79.07-79.079c-15.749-15.686-41.83-15.209-58.128,1.095l-39.836,39.827     c-2.518-1.387-5.122-2.836-7.861-4.373c-25.156-13.938-59.586-33.043-95.817-69.301c-36.338-36.333-55.46-70.816-69.441-95.989     c-1.475-2.667-2.889-5.238-4.285-7.68l26.736-26.696l13.144-13.161c16.323-16.328,16.773-42.401,1.061-58.133l-79.07-79.088     C92.24-3.657,66.146-3.18,49.824,13.147L27.539,35.56l0.609,0.604c-7.473,9.535-13.717,20.531-18.363,32.39     c-4.283,11.288-6.95,22.06-8.169,32.854c-10.441,86.556,29.113,165.661,136.456,273.006     c148.381,148.371,267.956,137.162,273.115,136.615c11.234-1.343,22.002-4.027,32.944-8.277     c11.756-4.592,22.745-10.827,32.273-18.283l0.487,0.432l22.576-22.106C515.757,446.47,516.226,420.387,500.521,404.63z"></path></svg>Phone:</div>
                                                <a class="list-board-of-education__text-info" href="tel:<?php the_sub_field('phone_1'); ?>call"><?php the_sub_field('phone_1'); ?></a>
                                            </div>
                                            <div class="list-board-of-education__info-block">
                                                <div class="list-board-of-education__title-info"> <svg viewbox="0 0 512 512"><path d="M328.719,366.51c-2.021-2.438-5.031-3.844-8.197-3.844H191.479c-3.167,0-6.177,1.406-8.198,3.844     c-2.031,2.438-2.865,5.646-2.292,8.76C196.323,458.333,225.771,512,256,512c30.229,0,59.677-53.667,75.01-136.729     C331.583,372.156,330.75,368.948,328.719,366.51z"></path><path d="M499.833,178.083c-1.406-4.417-5.51-7.417-10.156-7.417H369.563c-3.01,0-5.885,1.271-7.905,3.51     c-2.031,2.24-3.011,5.229-2.708,8.229c2.469,24.5,3.719,49.271,3.719,73.594s-1.25,49.094-3.719,73.594     c-0.303,3,0.677,5.99,2.708,8.229c2.021,2.24,4.896,3.51,7.905,3.51h120.115c4.646,0,8.75-3,10.156-7.417     C507.906,308.635,512,282.427,512,256S507.906,203.365,499.833,178.083z"></path><path d="M353.292,140.438c0.865,5.135,5.313,8.896,10.521,8.896H471.48c3.76,0,7.25-1.99,9.177-5.219     c1.917-3.24,1.989-7.25,0.188-10.552C448.823,74.885,393.813,30.188,329.896,10.938c-4.115-1.271-8.646,0.135-11.386,3.521     c-2.719,3.396-3.114,8.094-1,11.885C333.052,54.198,345.427,93.646,353.292,140.438z"></path><path d="M326.719,170.667H185.281c-5.448,0-10.021,4.104-10.604,9.521c-2.656,24.625-4.01,50.135-4.01,75.813     c0,25.679,1.354,51.188,4.01,75.813c0.583,5.416,5.156,9.521,10.604,9.521h141.438c5.448,0,10.021-4.104,10.604-9.521     c2.655-24.625,4.01-50.135,4.01-75.813c0-25.678-1.354-51.188-4.01-75.813C336.74,174.771,332.167,170.667,326.719,170.667z"></path><path d="M40.521,149.333h107.667c5.208,0,9.656-3.76,10.521-8.896c7.865-46.792,20.24-86.24,35.781-114.094     c2.115-3.792,1.719-8.49-1-11.885c-2.729-3.375-7.24-4.802-11.385-3.521C118.188,30.188,63.177,74.885,31.156,133.563     c-1.802,3.302-1.729,7.313,0.188,10.552C33.271,147.344,36.76,149.333,40.521,149.333z"></path><path d="M150.344,337.823c2.031-2.24,3.01-5.229,2.708-8.229c-2.469-24.5-3.719-49.271-3.719-73.594     s1.25-49.094,3.719-73.594c0.302-3-0.677-5.99-2.708-8.229c-2.021-2.24-4.896-3.51-7.906-3.51H22.323     c-4.646,0-8.75,3-10.156,7.417C4.094,203.365,0,229.573,0,256s4.094,52.635,12.167,77.917c1.406,4.417,5.51,7.417,10.156,7.417     h120.115C145.448,341.333,148.323,340.063,150.344,337.823z"></path><path d="M158.708,371.563c-0.865-5.135-5.313-8.896-10.521-8.896H40.521c-3.76,0-7.25,1.99-9.177,5.219     c-1.917,3.24-1.99,7.25-0.188,10.552c32.021,58.678,87.031,103.375,150.948,122.625c1.01,0.313,2.042,0.459,3.073,0.459     c3.177,0,6.25-1.428,8.313-3.979c2.719-3.396,3.115-8.094,1-11.885C178.948,457.802,166.573,418.354,158.708,371.563z"></path><path d="M471.479,362.667H363.813c-5.207,0-9.655,3.76-10.521,8.896c-7.865,46.793-20.24,86.24-35.781,114.095     c-2.114,3.792-1.719,8.489,1,11.885c2.063,2.552,5.136,3.979,8.313,3.979c1.021,0,2.063-0.146,3.072-0.459     c63.918-19.25,118.928-63.947,150.948-122.625c1.802-3.302,1.729-7.313-0.188-10.552     C478.729,364.656,475.24,362.667,471.479,362.667z"></path><path d="M183.281,145.49c2.021,2.438,5.031,3.844,8.198,3.844h129.042c3.166,0,6.177-1.406,8.197-3.844     c2.031-2.438,2.865-5.646,2.292-8.76C315.677,53.667,286.229,0,256,0c-30.229,0-59.677,53.667-75.01,136.729     C180.417,139.844,181.25,143.052,183.281,145.49z"></path></svg>Website:</div>
                                                <a class="list-board-of-education__text-info" href="https://<?php the_sub_field('website_1'); ?>"><?php the_sub_field('website_1'); ?></a>
                                            </div>
                                            <div class="list-board-of-education__info-block">
                                                <div class="list-board-of-education__title-info"><svg viewbox="79.5 90 448.111 617.5"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696    c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134    c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522    S367.439,423.134,306,423.134z"></path></svg>Address:</div>
                                                <div class="list-board-of-education__text-info"><?php the_sub_field('addres_1'); ?></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="list-board-of-education__tabs-content js-tabs-content">

                                    <div class="list-board-of-education__text">
                                        <p><?php the_sub_field('text_2'); ?></p>
                                    </div>
                                    <div class="list-board-of-education__info">
                                        <div class="list-board-of-education__info-block">
                                            <div class="list-board-of-education__title-info"> <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 512 512"><path d="M500.521,404.63l-79.07-79.079c-15.749-15.686-41.83-15.209-58.128,1.095l-39.836,39.827     c-2.518-1.387-5.122-2.836-7.861-4.373c-25.156-13.938-59.586-33.043-95.817-69.301c-36.338-36.333-55.46-70.816-69.441-95.989     c-1.475-2.667-2.889-5.238-4.285-7.68l26.736-26.696l13.144-13.161c16.323-16.328,16.773-42.401,1.061-58.133l-79.07-79.088     C92.24-3.657,66.146-3.18,49.824,13.147L27.539,35.56l0.609,0.604c-7.473,9.535-13.717,20.531-18.363,32.39     c-4.283,11.288-6.95,22.06-8.169,32.854c-10.441,86.556,29.113,165.661,136.456,273.006     c148.381,148.371,267.956,137.162,273.115,136.615c11.234-1.343,22.002-4.027,32.944-8.277     c11.756-4.592,22.745-10.827,32.273-18.283l0.487,0.432l22.576-22.106C515.757,446.47,516.226,420.387,500.521,404.63z"></path></svg>Phone:</div>
                                            <a
                                                class="list-board-of-education__text-info" href="tel:<?php the_sub_field('phone_2'); ?>call"><?php the_sub_field('phone_2'); ?></a>
                                        </div>
                                        <div class="list-board-of-education__info-block">
                                            <div class="list-board-of-education__title-info"> <svg viewbox="0 0 512 512"><path d="M328.719,366.51c-2.021-2.438-5.031-3.844-8.197-3.844H191.479c-3.167,0-6.177,1.406-8.198,3.844     c-2.031,2.438-2.865,5.646-2.292,8.76C196.323,458.333,225.771,512,256,512c30.229,0,59.677-53.667,75.01-136.729     C331.583,372.156,330.75,368.948,328.719,366.51z"></path><path d="M499.833,178.083c-1.406-4.417-5.51-7.417-10.156-7.417H369.563c-3.01,0-5.885,1.271-7.905,3.51     c-2.031,2.24-3.011,5.229-2.708,8.229c2.469,24.5,3.719,49.271,3.719,73.594s-1.25,49.094-3.719,73.594     c-0.303,3,0.677,5.99,2.708,8.229c2.021,2.24,4.896,3.51,7.905,3.51h120.115c4.646,0,8.75-3,10.156-7.417     C507.906,308.635,512,282.427,512,256S507.906,203.365,499.833,178.083z"></path><path d="M353.292,140.438c0.865,5.135,5.313,8.896,10.521,8.896H471.48c3.76,0,7.25-1.99,9.177-5.219     c1.917-3.24,1.989-7.25,0.188-10.552C448.823,74.885,393.813,30.188,329.896,10.938c-4.115-1.271-8.646,0.135-11.386,3.521     c-2.719,3.396-3.114,8.094-1,11.885C333.052,54.198,345.427,93.646,353.292,140.438z"></path><path d="M326.719,170.667H185.281c-5.448,0-10.021,4.104-10.604,9.521c-2.656,24.625-4.01,50.135-4.01,75.813     c0,25.679,1.354,51.188,4.01,75.813c0.583,5.416,5.156,9.521,10.604,9.521h141.438c5.448,0,10.021-4.104,10.604-9.521     c2.655-24.625,4.01-50.135,4.01-75.813c0-25.678-1.354-51.188-4.01-75.813C336.74,174.771,332.167,170.667,326.719,170.667z"></path><path d="M40.521,149.333h107.667c5.208,0,9.656-3.76,10.521-8.896c7.865-46.792,20.24-86.24,35.781-114.094     c2.115-3.792,1.719-8.49-1-11.885c-2.729-3.375-7.24-4.802-11.385-3.521C118.188,30.188,63.177,74.885,31.156,133.563     c-1.802,3.302-1.729,7.313,0.188,10.552C33.271,147.344,36.76,149.333,40.521,149.333z"></path><path d="M150.344,337.823c2.031-2.24,3.01-5.229,2.708-8.229c-2.469-24.5-3.719-49.271-3.719-73.594     s1.25-49.094,3.719-73.594c0.302-3-0.677-5.99-2.708-8.229c-2.021-2.24-4.896-3.51-7.906-3.51H22.323     c-4.646,0-8.75,3-10.156,7.417C4.094,203.365,0,229.573,0,256s4.094,52.635,12.167,77.917c1.406,4.417,5.51,7.417,10.156,7.417     h120.115C145.448,341.333,148.323,340.063,150.344,337.823z"></path><path d="M158.708,371.563c-0.865-5.135-5.313-8.896-10.521-8.896H40.521c-3.76,0-7.25,1.99-9.177,5.219     c-1.917,3.24-1.99,7.25-0.188,10.552c32.021,58.678,87.031,103.375,150.948,122.625c1.01,0.313,2.042,0.459,3.073,0.459     c3.177,0,6.25-1.428,8.313-3.979c2.719-3.396,3.115-8.094,1-11.885C178.948,457.802,166.573,418.354,158.708,371.563z"></path><path d="M471.479,362.667H363.813c-5.207,0-9.655,3.76-10.521,8.896c-7.865,46.793-20.24,86.24-35.781,114.095     c-2.114,3.792-1.719,8.489,1,11.885c2.063,2.552,5.136,3.979,8.313,3.979c1.021,0,2.063-0.146,3.072-0.459     c63.918-19.25,118.928-63.947,150.948-122.625c1.802-3.302,1.729-7.313-0.188-10.552     C478.729,364.656,475.24,362.667,471.479,362.667z"></path><path d="M183.281,145.49c2.021,2.438,5.031,3.844,8.198,3.844h129.042c3.166,0,6.177-1.406,8.197-3.844     c2.031-2.438,2.865-5.646,2.292-8.76C315.677,53.667,286.229,0,256,0c-30.229,0-59.677,53.667-75.01,136.729     C180.417,139.844,181.25,143.052,183.281,145.49z"></path></svg>Website:</div>
                                            <a
                                                class="list-board-of-education__text-info" href="https://<?php the_sub_field('website_2'); ?>"><?php the_sub_field('website_2'); ?></a>
                                        </div>
                                        <div class="list-board-of-education__info-block">
                                            <div class="list-board-of-education__title-info"><svg viewbox="79.5 90 448.111 617.5"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696    c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134    c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522    S367.439,423.134,306,423.134z"></path></svg>Address:</div>
                                            <div
                                                class="list-board-of-education__text-info"><?php the_sub_field('addres_2'); ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="list-board-of-education__tabs-content js-tabs-content">

                                <div class="list-board-of-education__text">
                                    <p><?php the_sub_field('text_3'); ?></p>
                                </div>
                                <div class="list-board-of-education__info">
                                    <div class="list-board-of-education__info-block">
                                        <div class="list-board-of-education__title-info"> <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 512 512"><path d="M500.521,404.63l-79.07-79.079c-15.749-15.686-41.83-15.209-58.128,1.095l-39.836,39.827     c-2.518-1.387-5.122-2.836-7.861-4.373c-25.156-13.938-59.586-33.043-95.817-69.301c-36.338-36.333-55.46-70.816-69.441-95.989     c-1.475-2.667-2.889-5.238-4.285-7.68l26.736-26.696l13.144-13.161c16.323-16.328,16.773-42.401,1.061-58.133l-79.07-79.088     C92.24-3.657,66.146-3.18,49.824,13.147L27.539,35.56l0.609,0.604c-7.473,9.535-13.717,20.531-18.363,32.39     c-4.283,11.288-6.95,22.06-8.169,32.854c-10.441,86.556,29.113,165.661,136.456,273.006     c148.381,148.371,267.956,137.162,273.115,136.615c11.234-1.343,22.002-4.027,32.944-8.277     c11.756-4.592,22.745-10.827,32.273-18.283l0.487,0.432l22.576-22.106C515.757,446.47,516.226,420.387,500.521,404.63z"></path></svg>Phone:</div>
                                        <a
                                            class="list-board-of-education__text-info" href="tel:<?php the_sub_field('phone_3'); ?>call"><?php the_sub_field('phone_3'); ?></a>
                                    </div>
                                    <div class="list-board-of-education__info-block">
                                        <div class="list-board-of-education__title-info"> <svg viewbox="0 0 512 512"><path d="M328.719,366.51c-2.021-2.438-5.031-3.844-8.197-3.844H191.479c-3.167,0-6.177,1.406-8.198,3.844     c-2.031,2.438-2.865,5.646-2.292,8.76C196.323,458.333,225.771,512,256,512c30.229,0,59.677-53.667,75.01-136.729     C331.583,372.156,330.75,368.948,328.719,366.51z"></path><path d="M499.833,178.083c-1.406-4.417-5.51-7.417-10.156-7.417H369.563c-3.01,0-5.885,1.271-7.905,3.51     c-2.031,2.24-3.011,5.229-2.708,8.229c2.469,24.5,3.719,49.271,3.719,73.594s-1.25,49.094-3.719,73.594     c-0.303,3,0.677,5.99,2.708,8.229c2.021,2.24,4.896,3.51,7.905,3.51h120.115c4.646,0,8.75-3,10.156-7.417     C507.906,308.635,512,282.427,512,256S507.906,203.365,499.833,178.083z"></path><path d="M353.292,140.438c0.865,5.135,5.313,8.896,10.521,8.896H471.48c3.76,0,7.25-1.99,9.177-5.219     c1.917-3.24,1.989-7.25,0.188-10.552C448.823,74.885,393.813,30.188,329.896,10.938c-4.115-1.271-8.646,0.135-11.386,3.521     c-2.719,3.396-3.114,8.094-1,11.885C333.052,54.198,345.427,93.646,353.292,140.438z"></path><path d="M326.719,170.667H185.281c-5.448,0-10.021,4.104-10.604,9.521c-2.656,24.625-4.01,50.135-4.01,75.813     c0,25.679,1.354,51.188,4.01,75.813c0.583,5.416,5.156,9.521,10.604,9.521h141.438c5.448,0,10.021-4.104,10.604-9.521     c2.655-24.625,4.01-50.135,4.01-75.813c0-25.678-1.354-51.188-4.01-75.813C336.74,174.771,332.167,170.667,326.719,170.667z"></path><path d="M40.521,149.333h107.667c5.208,0,9.656-3.76,10.521-8.896c7.865-46.792,20.24-86.24,35.781-114.094     c2.115-3.792,1.719-8.49-1-11.885c-2.729-3.375-7.24-4.802-11.385-3.521C118.188,30.188,63.177,74.885,31.156,133.563     c-1.802,3.302-1.729,7.313,0.188,10.552C33.271,147.344,36.76,149.333,40.521,149.333z"></path><path d="M150.344,337.823c2.031-2.24,3.01-5.229,2.708-8.229c-2.469-24.5-3.719-49.271-3.719-73.594     s1.25-49.094,3.719-73.594c0.302-3-0.677-5.99-2.708-8.229c-2.021-2.24-4.896-3.51-7.906-3.51H22.323     c-4.646,0-8.75,3-10.156,7.417C4.094,203.365,0,229.573,0,256s4.094,52.635,12.167,77.917c1.406,4.417,5.51,7.417,10.156,7.417     h120.115C145.448,341.333,148.323,340.063,150.344,337.823z"></path><path d="M158.708,371.563c-0.865-5.135-5.313-8.896-10.521-8.896H40.521c-3.76,0-7.25,1.99-9.177,5.219     c-1.917,3.24-1.99,7.25-0.188,10.552c32.021,58.678,87.031,103.375,150.948,122.625c1.01,0.313,2.042,0.459,3.073,0.459     c3.177,0,6.25-1.428,8.313-3.979c2.719-3.396,3.115-8.094,1-11.885C178.948,457.802,166.573,418.354,158.708,371.563z"></path><path d="M471.479,362.667H363.813c-5.207,0-9.655,3.76-10.521,8.896c-7.865,46.793-20.24,86.24-35.781,114.095     c-2.114,3.792-1.719,8.489,1,11.885c2.063,2.552,5.136,3.979,8.313,3.979c1.021,0,2.063-0.146,3.072-0.459     c63.918-19.25,118.928-63.947,150.948-122.625c1.802-3.302,1.729-7.313-0.188-10.552     C478.729,364.656,475.24,362.667,471.479,362.667z"></path><path d="M183.281,145.49c2.021,2.438,5.031,3.844,8.198,3.844h129.042c3.166,0,6.177-1.406,8.197-3.844     c2.031-2.438,2.865-5.646,2.292-8.76C315.677,53.667,286.229,0,256,0c-30.229,0-59.677,53.667-75.01,136.729     C180.417,139.844,181.25,143.052,183.281,145.49z"></path></svg>Website:</div>
                                        <a
                                            class="list-board-of-education__text-info" href="htps://<?php the_sub_field('website_3'); ?>"><?php the_sub_field('website_3'); ?></a>
                                    </div>
                                    <div class="list-board-of-education__info-block">
                                        <div class="list-board-of-education__title-info"><svg viewbox="79.5 90 448.111 617.5"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696    c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134    c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522    S367.439,423.134,306,423.134z"></path></svg>Address:</div>
                                        <div
                                            class="list-board-of-education__text-info"><?php the_sub_field('addres_3'); ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="list-board-of-education__tabs-content js-tabs-content active">

                            <div class="list-board-of-education__text">
                                <p><?php the_sub_field('text_4'); ?></p>
                            </div>
                            <div class="list-board-of-education__info">
                                <div class="list-board-of-education__info-block">
                                    <div class="list-board-of-education__title-info"> <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 512 512"><path d="M500.521,404.63l-79.07-79.079c-15.749-15.686-41.83-15.209-58.128,1.095l-39.836,39.827     c-2.518-1.387-5.122-2.836-7.861-4.373c-25.156-13.938-59.586-33.043-95.817-69.301c-36.338-36.333-55.46-70.816-69.441-95.989     c-1.475-2.667-2.889-5.238-4.285-7.68l26.736-26.696l13.144-13.161c16.323-16.328,16.773-42.401,1.061-58.133l-79.07-79.088     C92.24-3.657,66.146-3.18,49.824,13.147L27.539,35.56l0.609,0.604c-7.473,9.535-13.717,20.531-18.363,32.39     c-4.283,11.288-6.95,22.06-8.169,32.854c-10.441,86.556,29.113,165.661,136.456,273.006     c148.381,148.371,267.956,137.162,273.115,136.615c11.234-1.343,22.002-4.027,32.944-8.277     c11.756-4.592,22.745-10.827,32.273-18.283l0.487,0.432l22.576-22.106C515.757,446.47,516.226,420.387,500.521,404.63z"></path></svg>Phone:</div>
                                    <a
                                        class="list-board-of-education__text-info" href="tel:<?php the_sub_field('phone_4'); ?>call"><?php the_sub_field('phone_4'); ?></a>
                                </div>
                                <div class="list-board-of-education__info-block">
                                    <div class="list-board-of-education__title-info"> <svg viewbox="0 0 512 512"><path d="M328.719,366.51c-2.021-2.438-5.031-3.844-8.197-3.844H191.479c-3.167,0-6.177,1.406-8.198,3.844     c-2.031,2.438-2.865,5.646-2.292,8.76C196.323,458.333,225.771,512,256,512c30.229,0,59.677-53.667,75.01-136.729     C331.583,372.156,330.75,368.948,328.719,366.51z"></path><path d="M499.833,178.083c-1.406-4.417-5.51-7.417-10.156-7.417H369.563c-3.01,0-5.885,1.271-7.905,3.51     c-2.031,2.24-3.011,5.229-2.708,8.229c2.469,24.5,3.719,49.271,3.719,73.594s-1.25,49.094-3.719,73.594     c-0.303,3,0.677,5.99,2.708,8.229c2.021,2.24,4.896,3.51,7.905,3.51h120.115c4.646,0,8.75-3,10.156-7.417     C507.906,308.635,512,282.427,512,256S507.906,203.365,499.833,178.083z"></path><path d="M353.292,140.438c0.865,5.135,5.313,8.896,10.521,8.896H471.48c3.76,0,7.25-1.99,9.177-5.219     c1.917-3.24,1.989-7.25,0.188-10.552C448.823,74.885,393.813,30.188,329.896,10.938c-4.115-1.271-8.646,0.135-11.386,3.521     c-2.719,3.396-3.114,8.094-1,11.885C333.052,54.198,345.427,93.646,353.292,140.438z"></path><path d="M326.719,170.667H185.281c-5.448,0-10.021,4.104-10.604,9.521c-2.656,24.625-4.01,50.135-4.01,75.813     c0,25.679,1.354,51.188,4.01,75.813c0.583,5.416,5.156,9.521,10.604,9.521h141.438c5.448,0,10.021-4.104,10.604-9.521     c2.655-24.625,4.01-50.135,4.01-75.813c0-25.678-1.354-51.188-4.01-75.813C336.74,174.771,332.167,170.667,326.719,170.667z"></path><path d="M40.521,149.333h107.667c5.208,0,9.656-3.76,10.521-8.896c7.865-46.792,20.24-86.24,35.781-114.094     c2.115-3.792,1.719-8.49-1-11.885c-2.729-3.375-7.24-4.802-11.385-3.521C118.188,30.188,63.177,74.885,31.156,133.563     c-1.802,3.302-1.729,7.313,0.188,10.552C33.271,147.344,36.76,149.333,40.521,149.333z"></path><path d="M150.344,337.823c2.031-2.24,3.01-5.229,2.708-8.229c-2.469-24.5-3.719-49.271-3.719-73.594     s1.25-49.094,3.719-73.594c0.302-3-0.677-5.99-2.708-8.229c-2.021-2.24-4.896-3.51-7.906-3.51H22.323     c-4.646,0-8.75,3-10.156,7.417C4.094,203.365,0,229.573,0,256s4.094,52.635,12.167,77.917c1.406,4.417,5.51,7.417,10.156,7.417     h120.115C145.448,341.333,148.323,340.063,150.344,337.823z"></path><path d="M158.708,371.563c-0.865-5.135-5.313-8.896-10.521-8.896H40.521c-3.76,0-7.25,1.99-9.177,5.219     c-1.917,3.24-1.99,7.25-0.188,10.552c32.021,58.678,87.031,103.375,150.948,122.625c1.01,0.313,2.042,0.459,3.073,0.459     c3.177,0,6.25-1.428,8.313-3.979c2.719-3.396,3.115-8.094,1-11.885C178.948,457.802,166.573,418.354,158.708,371.563z"></path><path d="M471.479,362.667H363.813c-5.207,0-9.655,3.76-10.521,8.896c-7.865,46.793-20.24,86.24-35.781,114.095     c-2.114,3.792-1.719,8.489,1,11.885c2.063,2.552,5.136,3.979,8.313,3.979c1.021,0,2.063-0.146,3.072-0.459     c63.918-19.25,118.928-63.947,150.948-122.625c1.802-3.302,1.729-7.313-0.188-10.552     C478.729,364.656,475.24,362.667,471.479,362.667z"></path><path d="M183.281,145.49c2.021,2.438,5.031,3.844,8.198,3.844h129.042c3.166,0,6.177-1.406,8.197-3.844     c2.031-2.438,2.865-5.646,2.292-8.76C315.677,53.667,286.229,0,256,0c-30.229,0-59.677,53.667-75.01,136.729     C180.417,139.844,181.25,143.052,183.281,145.49z"></path></svg>Website:</div>
                                    <a
                                        class="list-board-of-education__text-info" href="htps://<?php the_sub_field('website_4'); ?>"><?php the_sub_field('website_4'); ?></a>
                                </div>
                                <div class="list-board-of-education__info-block">
                                    <div class="list-board-of-education__title-info"><svg viewbox="79.5 90 448.111 617.5"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696    c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134    c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522    S367.439,423.134,306,423.134z"></path></svg>Address:</div>
                                    <div
                                        class="list-board-of-education__text-info"><?php the_sub_field('addres_4'); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                </div>
                </div>
                </div>
            </section>

        <?php endif;

        elseif( get_row_layout() == 'local_real_estate' ):
		  $show = get_sub_field('show_hide');
		  if ($show == "1"): ?>
            <section class="local-real-estate">
                <div class="lines">
                    <div class="container">
                        <div class="row">
                            <div class="lines-items">
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="local-real-estate__title"><?php the_sub_field('title'); ?></div>
                    <div class="local-real-estate__list">

                        <?php
                        if( have_rows('local_real') ):
                          while ( have_rows('local_real') ) : the_row(); ?>

                            <div class="col-xl-4 col-lg-4 col-md-6">
                                 <div class="local-real-estate__item">
                                     <img class="local-real-estate__img-list" src="<?php the_sub_field('image'); ?>" alt="" role="presentation" />
                                     <div class="local-real-estate__number"><?php the_sub_field('value'); ?></div>
                                     <div class="local-real-estate__title-list"><?php the_sub_field('text'); ?></div>
                                 </div>
                            </div>

                          <?php  endwhile;
                        else :
                        endif;
                        ?>

                    </div>
                </div>
            </section>
          <?php endif;

        elseif( get_row_layout() == 'real_estate_block' ):
		$show = get_sub_field('show_hide');
		if ($show == "1"): ?>
            <section class="new-construction" style="background-color: <?php the_sub_field('background_color'); ?>">
                <div class="lines">
                    <div class="container">
                        <div class="row">
                            <div class="lines-items lines-items lines-items_gray-light">
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="new-construction__title"><?php the_sub_field('title') ?></div>
                    <div class="list-new-construction">
                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                            <div class="list-new-construction__item">
                                <div class="list-new-construction__top"><img class="list-new-construction__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png" alt="" role="presentation" />
                                    <div class="list-new-construction__hover"><a class="list-new-construction__title" href="#">AVA Nob Hill</a>
                                        <div class="list-new-construction__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                            Main street. Newport Beach CA 92660</div>
                                    </div>
                                </div>
                                <div class="list-new-construction__bottom">
                                    <div class="list-new-construction__price"><span>$1,199.00/mon</span></div>
                                    <div class="list-new-construction__info-item">
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Area</div>
                                            <div class="list-new-construction__info-block">2,100 sq. ft.</div>
                                        </div>
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Bedrooms</div>
                                            <div class="list-new-construction__info-block">2</div>
                                        </div>
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Bathrooms</div>
                                            <div class="list-new-construction__info-block">1</div>
                                        </div>
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Garages</div>
                                            <div class="list-new-construction__info-block">2</div>
                                        </div>
                                    </div>
                                    <div class="list-new-construction__text">Front-facing, single level, well-designed floor plan. Secure building in a highly DESIRABLE location!</div><a class="lines-button" href="#">more</a></div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                            <div class="list-new-construction__item">
                                <div class="list-new-construction__top"><img class="list-new-construction__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png" alt="" role="presentation" />
                                    <div class="list-new-construction__hover"><a class="list-new-construction__title" href="#">AVA Nob Hill</a>
                                        <div class="list-new-construction__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                            Main street. Newport Beach CA 92660</div>
                                    </div>
                                </div>
                                <div class="list-new-construction__bottom">
                                    <div class="list-new-construction__price"><span>$1,199.00/mon</span></div>
                                    <div class="list-new-construction__info-item">
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Area</div>
                                            <div class="list-new-construction__info-block">2,100 sq. ft.</div>
                                        </div>
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Bedrooms</div>
                                            <div class="list-new-construction__info-block">2</div>
                                        </div>
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Bathrooms</div>
                                            <div class="list-new-construction__info-block">1</div>
                                        </div>
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Garages</div>
                                            <div class="list-new-construction__info-block">2</div>
                                        </div>
                                    </div>
                                    <div class="list-new-construction__text">Front-facing, single level, well-designed floor plan. Secure building in a highly DESIRABLE location!</div><a class="lines-button" href="#">more</a></div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                            <div class="list-new-construction__item">
                                <div class="list-new-construction__top"><img class="list-new-construction__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png" alt="" role="presentation" />
                                    <div class="list-new-construction__hover"><a class="list-new-construction__title" href="#">AVA Nob Hill</a>
                                        <div class="list-new-construction__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                            Main street. Newport Beach CA 92660</div>
                                    </div>
                                </div>
                                <div class="list-new-construction__bottom">
                                    <div class="list-new-construction__price"><span>$1,199.00/mon</span></div>
                                    <div class="list-new-construction__info-item">
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Area</div>
                                            <div class="list-new-construction__info-block">2,100 sq. ft.</div>
                                        </div>
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Bedrooms</div>
                                            <div class="list-new-construction__info-block">2</div>
                                        </div>
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Bathrooms</div>
                                            <div class="list-new-construction__info-block">1</div>
                                        </div>
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Garages</div>
                                            <div class="list-new-construction__info-block">2</div>
                                        </div>
                                    </div>
                                    <div class="list-new-construction__text">Front-facing, single level, well-designed floor plan. Secure building in a highly DESIRABLE location!</div><a class="lines-button" href="#">more</a></div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                            <div class="list-new-construction__item">
                                <div class="list-new-construction__top"><img class="list-new-construction__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png" alt="" role="presentation" />
                                    <div class="list-new-construction__hover"><a class="list-new-construction__title" href="#">AVA Nob Hill</a>
                                        <div class="list-new-construction__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                            Main street. Newport Beach CA 92660</div>
                                    </div>
                                </div>
                                <div class="list-new-construction__bottom">
                                    <div class="list-new-construction__price"><span>$1,199.00/mon</span></div>
                                    <div class="list-new-construction__info-item">
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Area</div>
                                            <div class="list-new-construction__info-block">2,100 sq. ft.</div>
                                        </div>
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Bedrooms</div>
                                            <div class="list-new-construction__info-block">2</div>
                                        </div>
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Bathrooms</div>
                                            <div class="list-new-construction__info-block">1</div>
                                        </div>
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Garages</div>
                                            <div class="list-new-construction__info-block">2</div>
                                        </div>
                                    </div>
                                    <div class="list-new-construction__text">Front-facing, single level, well-designed floor plan. Secure building in a highly DESIRABLE location!</div><a class="lines-button" href="#">more</a></div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                            <div class="list-new-construction__item">
                                <div class="list-new-construction__top"><img class="list-new-construction__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png" alt="" role="presentation" />
                                    <div class="list-new-construction__hover"><a class="list-new-construction__title" href="#">AVA Nob Hill</a>
                                        <div class="list-new-construction__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                            Main street. Newport Beach CA 92660</div>
                                    </div>
                                </div>
                                <div class="list-new-construction__bottom">
                                    <div class="list-new-construction__price"><span>$1,199.00/mon</span></div>
                                    <div class="list-new-construction__info-item">
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Area</div>
                                            <div class="list-new-construction__info-block">2,100 sq. ft.</div>
                                        </div>
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Bedrooms</div>
                                            <div class="list-new-construction__info-block">2</div>
                                        </div>
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Bathrooms</div>
                                            <div class="list-new-construction__info-block">1</div>
                                        </div>
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Garages</div>
                                            <div class="list-new-construction__info-block">2</div>
                                        </div>
                                    </div>
                                    <div class="list-new-construction__text">Front-facing, single level, well-designed floor plan. Secure building in a highly DESIRABLE location!</div><a class="lines-button" href="#">more</a></div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                            <div class="list-new-construction__item">
                                <div class="list-new-construction__top"><img class="list-new-construction__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png" alt="" role="presentation" />
                                    <div class="list-new-construction__hover"><a class="list-new-construction__title" href="#">AVA Nob Hill</a>
                                        <div class="list-new-construction__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                            Main street. Newport Beach CA 92660</div>
                                    </div>
                                </div>
                                <div class="list-new-construction__bottom">
                                    <div class="list-new-construction__price"><span>$1,199.00/mon</span></div>
                                    <div class="list-new-construction__info-item">
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Area</div>
                                            <div class="list-new-construction__info-block">2,100 sq. ft.</div>
                                        </div>
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Bedrooms</div>
                                            <div class="list-new-construction__info-block">2</div>
                                        </div>
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Bathrooms</div>
                                            <div class="list-new-construction__info-block">1</div>
                                        </div>
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Garages</div>
                                            <div class="list-new-construction__info-block">2</div>
                                        </div>
                                    </div>
                                    <div class="list-new-construction__text">Front-facing, single level, well-designed floor plan. Secure building in a highly DESIRABLE location!</div><a class="lines-button" href="#">more</a></div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                            <div class="list-new-construction__item">
                                <div class="list-new-construction__top"><img class="list-new-construction__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png" alt="" role="presentation" />
                                    <div class="list-new-construction__hover"><a class="list-new-construction__title" href="#">AVA Nob Hill</a>
                                        <div class="list-new-construction__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                            Main street. Newport Beach CA 92660</div>
                                    </div>
                                </div>
                                <div class="list-new-construction__bottom">
                                    <div class="list-new-construction__price"><span>$1,199.00/mon</span></div>
                                    <div class="list-new-construction__info-item">
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Area</div>
                                            <div class="list-new-construction__info-block">2,100 sq. ft.</div>
                                        </div>
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Bedrooms</div>
                                            <div class="list-new-construction__info-block">2</div>
                                        </div>
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Bathrooms</div>
                                            <div class="list-new-construction__info-block">1</div>
                                        </div>
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Garages</div>
                                            <div class="list-new-construction__info-block">2</div>
                                        </div>
                                    </div>
                                    <div class="list-new-construction__text">Front-facing, single level, well-designed floor plan. Secure building in a highly DESIRABLE location!</div><a class="lines-button" href="#">more</a></div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                            <div class="list-new-construction__item">
                                <div class="list-new-construction__top"><img class="list-new-construction__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png" alt="" role="presentation" />
                                    <div class="list-new-construction__hover"><a class="list-new-construction__title" href="#">AVA Nob Hill</a>
                                        <div class="list-new-construction__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                            Main street. Newport Beach CA 92660</div>
                                    </div>
                                </div>
                                <div class="list-new-construction__bottom">
                                    <div class="list-new-construction__price"><span>$1,199.00/mon</span></div>
                                    <div class="list-new-construction__info-item">
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Area</div>
                                            <div class="list-new-construction__info-block">2,100 sq. ft.</div>
                                        </div>
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Bedrooms</div>
                                            <div class="list-new-construction__info-block">2</div>
                                        </div>
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Bathrooms</div>
                                            <div class="list-new-construction__info-block">1</div>
                                        </div>
                                        <div class="list-new-construction__block">
                                            <div class="list-new-construction__name">Garages</div>
                                            <div class="list-new-construction__info-block">2</div>
                                        </div>
                                    </div>
                                    <div class="list-new-construction__text">Front-facing, single level, well-designed floor plan. Secure building in a highly DESIRABLE location!</div><a class="lines-button" href="#">more</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'things_to_do' ):
		  $show = get_sub_field('show_hide');
		  if ($show == "1"): ?>
            <?php $type = get_sub_field('select_type');?>

            <?php if($type == "parks_recreation"): ?>
              <section class="parks-and-recreation" style="background-color: <?php the_sub_field('background_color');?>">
                  <div class="lines">
                      <div class="container">
                          <div class="row">
                              <div class="lines-items lines-items lines-items_gray-light">
                                  <div class="lines-items__item"></div>
                                  <div class="lines-items__item"></div>
                                  <div class="lines-items__item"></div>
                                  <div class="lines-items__item"></div>
                                  <div class="lines-items__item"></div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="container">
                      <div class="parks-and-recreation__title"><?php the_sub_field('title'); ?></div>
                      <div class="block-title-text text-show"><?php the_sub_field('description'); ?></div>
                      <div class="list-parks-and-recreation">
                          <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                              <div class="list-parks-and-recreation__item">
                                  <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                      <div class="list-parks-and-recreation__block-info">
                                          <div class="list-parks-and-recreation__block-info-title">10</div>
                                          <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                      </div>
                                  </div>
                                  <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                      <div class="list-parks-and-recreation__title-info">Address:</div>
                                      <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                      <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                      <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                              </div>
                          </div>
                          <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                              <div class="list-parks-and-recreation__item">
                                  <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                      <div class="list-parks-and-recreation__block-info">
                                          <div class="list-parks-and-recreation__block-info-title">10</div>
                                          <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                      </div>
                                  </div>
                                  <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                      <div class="list-parks-and-recreation__title-info">Address:</div>
                                      <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                      <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                      <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                              </div>
                          </div>
                          <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                              <div class="list-parks-and-recreation__item">
                                  <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                      <div class="list-parks-and-recreation__block-info">
                                          <div class="list-parks-and-recreation__block-info-title">10</div>
                                          <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                      </div>
                                  </div>
                                  <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                      <div class="list-parks-and-recreation__title-info">Address:</div>
                                      <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                      <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                      <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                              </div>
                          </div>
                          <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                              <div class="list-parks-and-recreation__item">
                                  <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                      <div class="list-parks-and-recreation__block-info">
                                          <div class="list-parks-and-recreation__block-info-title">10</div>
                                          <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                      </div>
                                  </div>
                                  <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                      <div class="list-parks-and-recreation__title-info">Address:</div>
                                      <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                      <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                      <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                              </div>
                          </div>
                          <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                              <div class="list-parks-and-recreation__item">
                                  <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                      <div class="list-parks-and-recreation__block-info">
                                          <div class="list-parks-and-recreation__block-info-title">10</div>
                                          <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                      </div>
                                  </div>
                                  <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                      <div class="list-parks-and-recreation__title-info">Address:</div>
                                      <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                      <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                      <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                              </div>
                          </div>
                      </div>
                  </div>
              </section>
			<?php endif;?>

			<?php if($type == "fitness_health"): ?>
                  <section class="parks-and-recreation" style="background-color: <?php the_sub_field('background_color');?>">
                      <div class="lines">
                          <div class="container">
                              <div class="row">
                                  <div class="lines-items lines-items lines-items_gray-light">
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="container">
                          <div class="parks-and-recreation__title"><?php the_sub_field('title'); ?></div>
                          <div class="block-title-text text-show"><?php the_sub_field('description'); ?></div>
                          <div class="list-parks-and-recreation">
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </section>
			<?php endif;?>

			<?php if($type == "shopping"): ?>
                  <section class="parks-and-recreation" style="background-color: <?php the_sub_field('background_color');?>">
                      <div class="lines">
                          <div class="container">
                              <div class="row">
                                  <div class="lines-items lines-items lines-items_gray-light">
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="container">
                          <div class="parks-and-recreation__title"><?php the_sub_field('title'); ?></div>
                          <div class="block-title-text text-show"><?php the_sub_field('description'); ?></div>
                          <div class="list-parks-and-recreation">
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </section>
			<?php endif;?>

			<?php if($type == "beach_activities"): ?>
                  <section class="parks-and-recreation" style="background-color: <?php the_sub_field('background_color');?>">
                      <div class="lines">
                          <div class="container">
                              <div class="row">
                                  <div class="lines-items lines-items lines-items_gray-light">
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="container">
                          <div class="parks-and-recreation__title"><?php the_sub_field('title'); ?></div>
                          <div class="block-title-text text-show"><?php the_sub_field('description'); ?></div>
                          <div class="list-parks-and-recreation">
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </section>
			<?php endif;?>

			<?php if($type == "art_culture"): ?>
                  <section class="parks-and-recreation" style="background-color: <?php the_sub_field('background_color');?>">
                      <div class="lines">
                          <div class="container">
                              <div class="row">
                                  <div class="lines-items lines-items lines-items_gray-light">
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="container">
                          <div class="parks-and-recreation__title"><?php the_sub_field('title'); ?></div>
                          <div class="block-title-text text-show"><?php the_sub_field('description'); ?></div>
                          <div class="list-parks-and-recreation">
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </section>
			<?php endif;?>

			<?php if($type == "libraries"): ?>
                  <section class="parks-and-recreation" style="background-color: <?php the_sub_field('background_color');?>">
                      <div class="lines">
                          <div class="container">
                              <div class="row">
                                  <div class="lines-items lines-items lines-items_gray-light">
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="container">
                          <div class="parks-and-recreation__title"><?php the_sub_field('title'); ?></div>
                          <div class="block-title-text text-show"><?php the_sub_field('description'); ?></div>
                          <div class="list-parks-and-recreation">
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </section>
			<?php endif;?>

			<?php if($type == "events"): ?>
                  <section class="parks-and-recreation" style="background-color: <?php the_sub_field('background_color');?>">
                      <div class="lines">
                          <div class="container">
                              <div class="row">
                                  <div class="lines-items lines-items lines-items_gray-light">
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                      <div class="lines-items__item"></div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="container">
                          <div class="parks-and-recreation__title"><?php the_sub_field('title'); ?></div>
                          <div class="block-title-text text-show"><?php the_sub_field('description'); ?></div>
                          <div class="list-parks-and-recreation">
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                  <div class="list-parks-and-recreation__item">
                                      <div class="list-parks-and-recreation__top"><img class="list-parks-and-recreation__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-parks-and-recreation-1.png" alt="" role="presentation" />
                                          <div class="list-parks-and-recreation__block-info">
                                              <div class="list-parks-and-recreation__block-info-title">10</div>
                                              <div class="list-parks-and-recreation__block-info-text">GreatSchools Rating</div>
                                          </div>
                                      </div>
                                      <div class="list-parks-and-recreation__bottom"><a class="list-parks-and-recreation__title" href="#">Yosemite Park California</a>
                                          <div class="list-parks-and-recreation__title-info">Address:</div>
                                          <div class="list-parks-and-recreation__text-info">45 Park Avenue, Apt. 303 New York, NY 10016</div>
                                          <div class="list-parks-and-recreation__title-info">Phone:</div><a class="list-parks-and-recreation__text-info" href="tel:(123) 123-456call">(123) 123-456</a>
                                          <div class="list-parks-and-recreation__title-info">Website:</div><a class="list-parks-and-recreation__text-info" href="www.example.com" target="_blank">www.example.com</a><a class="button button_primary button_full" href="#">View nearby homes</a></div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </section>
			<?php endif;?>

        <?php endif;

        elseif( get_row_layout() == 'pocket_listings' ):
		  $show = get_sub_field('show_hide');
		  if ($show == "1"): ?>
              <section class="section-pocket-listings" style="background-image: url(<?php the_sub_field('image'); ?>);">
                  <div class="lines">
                      <div class="container">
                          <div class="row">
                              <div class="lines-items lines-items lines-items_white-light small-line">
                                  <div class="lines-items__item"></div>
                                  <div class="lines-items__item"></div>
                                  <div class="lines-items__item"></div>
                                  <div class="lines-items__item"></div>
                                  <div class="lines-items__item"></div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="container">
                      <div class="breadcrumbs-center-light">
                          <div class="breadcrumbs-center-light__nav">
                              <div class="breadcrumbs-center-light__item"><a href="/">home</a></div>
                              <div class="breadcrumbs-center-light__item"> <span>buyers</span></div>
                          </div>
                      </div>
                      <div class="section-pocket-listings__title"><?php the_sub_field('title'); ?></div>
                      <div class="section-pocket-listings__title-text"><?php the_sub_field('description'); ?></div>
                      <form class="form-pocket-listings row">
                          <div class="col-xl-3 col-lg-3"><input class="form-pocket-listings__input" placeholder="First Name" type="text" /></div>
                          <div class="col-xl-3 col-lg-3"><input class="form-pocket-listings__input" placeholder="Last Name" type="text" /></div>
                          <div class="col-xl-3 col-lg-3"><input class="form-pocket-listings__input" placeholder="E-mail" type="text" /></div>
                          <div class="col-xl-3 col-lg-3"><input class="form-pocket-listings__input" placeholder="Phone" type="text" /></div>
                          <div class="col-12"><button class="button button_primary" type="submit">contact us</button></div>
                      </form>
                      <div class="lock-pocket-listings">
                          <div class="lock-pocket-listings__svg"><svg width="29px" height="35px" viewbox="55.824 0 400.352 512"><path d="M426.477,199.899h-7.668v-37.09C418.81,73.036,345.774,0,256,0C166.226,0,93.19,73.036,93.19,162.81     v37.09h-7.666c-16.377,0-29.7,13.324-29.7,29.7v252.7c0,16.377,13.323,29.7,29.7,29.7h340.953     c16.377,0,29.699-13.323,29.699-29.7V229.6C456.176,213.223,442.854,199.899,426.477,199.899z M109.204,162.81     c0-80.943,65.853-146.796,146.796-146.796c80.943,0,146.796,65.853,146.796,146.796v37.09h-58.718v-37.09     c0-48.565-39.512-88.077-88.077-88.077c-48.565,0-88.077,39.512-88.077,88.077v37.09h-58.718v-37.09H109.204z M328.063,162.81     v37.09H183.937v-37.09c0-39.736,32.327-72.063,72.063-72.063C295.736,90.747,328.063,123.074,328.063,162.81z M71.838,229.6     c0-7.546,6.14-13.686,13.686-13.686h340.953c7.547,0,13.686,6.14,13.686,13.686v224.44H71.838V229.6z M440.162,482.3     c0,7.546-6.139,13.687-13.686,13.687H85.524c-7.546,0-13.686-6.141-13.686-13.687v-12.245h368.324V482.3z"></path><path d="M310.755,305.214c0-14.918-5.892-28.86-16.589-39.261c-10.695-10.398-24.846-15.892-39.742-15.472     c-28.176,0.793-51.495,23.441-53.09,51.558c-0.97,17.113,5.894,33.357,18.838,44.566c0.309,0.269,0.467,0.658,0.421,1.045     l-7.311,61.313c-0.674,5.644,1.104,11.319,4.878,15.569c3.773,4.25,9.197,6.688,14.88,6.688h45.918     c5.685,0,11.106-2.437,14.881-6.688c3.774-4.25,5.552-9.926,4.878-15.568l-7.311-61.315c-0.047-0.385,0.115-0.778,0.432-1.054     C303.861,336.178,310.755,321.096,310.755,305.214z M281.354,334.491c-4.34,3.759-6.525,9.39-5.849,15.055l7.312,61.314     c0.18,1.508-0.52,2.553-0.952,3.039c-0.433,0.487-1.388,1.307-2.906,1.307H233.04c-1.519,0-2.471-0.819-2.906-1.307     c-0.432-0.487-1.133-1.531-0.952-3.041l7.311-61.313c0.678-5.671-1.506-11.296-5.84-15.048     c-9.16-7.934-14.019-19.436-13.33-31.555c1.127-19.881,17.621-35.895,37.551-36.457c0.378-0.012,0.754-0.017,1.13-0.017     c10.153,0,19.703,3.868,26.999,10.962c7.569,7.36,11.738,17.226,11.738,27.78C294.741,316.448,289.861,327.119,281.354,334.491z"></path></svg></div>
                      </div>
                  </div>
              </section>
          <?php endif;

        elseif( get_row_layout() == 'my_listings' ):
		$show = get_sub_field('show_hide');
		if ($show == "1"): ?>
            <section class="my-listings-catalog">
                <div class="lines">
                    <div class="container">
                        <div class="row">
                            <div class="lines-items lines-items lines-items_gray-light">
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row wrap-mls">
                        <div class="col-xl-9">
                            <div class="my-listings-catalog__list">
                                <div class="my-listings-catalog__item row" data-aos="fade-right">
                                    <div class="my-listings-catalog__info col-xl-5 col-lg-5">
                                        <div class="my-listings-catalog__title-list"><a href="/my-listings-page/">AVA Nob Hill</a></div>
                                        <div class="my-listings-catalog__location">7843 Durham Avenue, MD</div>
                                        <div class="my-listings-catalog__price"><span>$1,199.00/mon</span></div>
                                        <div class="my-listings-catalog__info-item">
                                            <div class="my-listings-catalog__block">
                                                <div class="my-listings-catalog__name">Area</div>
                                                <div class="my-listings-catalog__info-block">2,100 sq. ft.</div>
                                            </div>
                                            <div class="my-listings-catalog__block">
                                                <div class="my-listings-catalog__name">Bedrooms</div>
                                                <div class="my-listings-catalog__info-block">2</div>
                                            </div>
                                            <div class="my-listings-catalog__block">
                                                <div class="my-listings-catalog__name">Bathrooms</div>
                                                <div class="my-listings-catalog__info-block">1</div>
                                            </div>
                                            <div class="my-listings-catalog__block">
                                                <div class="my-listings-catalog__name">Garages</div>
                                                <div class="my-listings-catalog__info-block">2</div>
                                            </div>
                                        </div>
                                        <div class="my-listings-catalog__text-item">Front-facing, single level, well-designed floor plan. Secure building in a highly DESIRABLE location!</div><a class="lines-button" href="#">more</a></div>
                                    <div class="my-listings-catalog__foto col-xl-7 col-lg-7">
                                        <div class="my-listings-catalog__block-icon"><img class="my-listings-catalog__icon" src="<?php echo get_template_directory_uri()?>/assets/img/my-listings-catalog-1.png" style="width: 100%;" alt="" role="presentation" /></div>
                                    </div>
                                </div>
                                <div class="my-listings-catalog__item row" data-aos="fade-left">
                                    <div class="my-listings-catalog__info col-xl-5 col-lg-5">
                                        <div class="my-listings-catalog__title-list"><a href="/my-listings-page/">AVA Nob Hill</a></div>
                                        <div class="my-listings-catalog__location">7843 Durham Avenue, MD</div>
                                        <div class="my-listings-catalog__price"><span>$1,199.00/mon</span></div>
                                        <div class="my-listings-catalog__info-item">
                                            <div class="my-listings-catalog__block">
                                                <div class="my-listings-catalog__name">Area</div>
                                                <div class="my-listings-catalog__info-block">2,100 sq. ft.</div>
                                            </div>
                                            <div class="my-listings-catalog__block">
                                                <div class="my-listings-catalog__name">Bedrooms</div>
                                                <div class="my-listings-catalog__info-block">2</div>
                                            </div>
                                            <div class="my-listings-catalog__block">
                                                <div class="my-listings-catalog__name">Bathrooms</div>
                                                <div class="my-listings-catalog__info-block">1</div>
                                            </div>
                                            <div class="my-listings-catalog__block">
                                                <div class="my-listings-catalog__name">Garages</div>
                                                <div class="my-listings-catalog__info-block">2</div>
                                            </div>
                                        </div>
                                        <div class="my-listings-catalog__text-item">Front-facing, single level, well-designed floor plan. Secure building in a highly DESIRABLE location!</div><a class="lines-button" href="#">more</a></div>
                                    <div class="my-listings-catalog__foto col-xl-7 col-lg-7">
                                        <div class="my-listings-catalog__block-icon"><img class="my-listings-catalog__icon" src="<?php echo get_template_directory_uri()?>/assets/img/my-listings-catalog-2.png" style="width: 100%;" alt="" role="presentation" /></div>
                                    </div>
                                </div>
                                <div class="my-listings-catalog__item row" data-aos="fade-right">
                                    <div class="my-listings-catalog__info col-xl-5 col-lg-5">
                                        <div class="my-listings-catalog__title-list"><a href="/my-listings-page/">AVA Nob Hill</a></div>
                                        <div class="my-listings-catalog__location">7843 Durham Avenue, MD</div>
                                        <div class="my-listings-catalog__price"><span>$1,199.00/mon</span></div>
                                        <div class="my-listings-catalog__info-item">
                                            <div class="my-listings-catalog__block">
                                                <div class="my-listings-catalog__name">Area</div>
                                                <div class="my-listings-catalog__info-block">2,100 sq. ft.</div>
                                            </div>
                                            <div class="my-listings-catalog__block">
                                                <div class="my-listings-catalog__name">Bedrooms</div>
                                                <div class="my-listings-catalog__info-block">2</div>
                                            </div>
                                            <div class="my-listings-catalog__block">
                                                <div class="my-listings-catalog__name">Bathrooms</div>
                                                <div class="my-listings-catalog__info-block">1</div>
                                            </div>
                                            <div class="my-listings-catalog__block">
                                                <div class="my-listings-catalog__name">Garages</div>
                                                <div class="my-listings-catalog__info-block">2</div>
                                            </div>
                                        </div>
                                        <div class="my-listings-catalog__text-item">Front-facing, single level, well-designed floor plan. Secure building in a highly DESIRABLE location!</div><a class="lines-button" href="#">more</a></div>
                                    <div class="my-listings-catalog__foto col-xl-7 col-lg-7">
                                        <div class="my-listings-catalog__block-icon"><img class="my-listings-catalog__icon" src="<?php echo get_template_directory_uri()?>/assets/img/my-listings-catalog-1.png" style="width: 100%;" alt="" role="presentation" /></div>
                                    </div>
                                </div>
                                <div class="my-listings-catalog__item row" data-aos="fade-left">
                                    <div class="my-listings-catalog__info col-xl-5 col-lg-5">
                                        <div class="my-listings-catalog__title-list"><a href="/my-listings-page/">AVA Nob Hill</a></div>
                                        <div class="my-listings-catalog__location">7843 Durham Avenue, MD</div>
                                        <div class="my-listings-catalog__price"><span>$1,199.00/mon</span></div>
                                        <div class="my-listings-catalog__info-item">
                                            <div class="my-listings-catalog__block">
                                                <div class="my-listings-catalog__name">Area</div>
                                                <div class="my-listings-catalog__info-block">2,100 sq. ft.</div>
                                            </div>
                                            <div class="my-listings-catalog__block">
                                                <div class="my-listings-catalog__name">Bedrooms</div>
                                                <div class="my-listings-catalog__info-block">2</div>
                                            </div>
                                            <div class="my-listings-catalog__block">
                                                <div class="my-listings-catalog__name">Bathrooms</div>
                                                <div class="my-listings-catalog__info-block">1</div>
                                            </div>
                                            <div class="my-listings-catalog__block">
                                                <div class="my-listings-catalog__name">Garages</div>
                                                <div class="my-listings-catalog__info-block">2</div>
                                            </div>
                                        </div>
                                        <div class="my-listings-catalog__text-item">Front-facing, single level, well-designed floor plan. Secure building in a highly DESIRABLE location!</div><a class="lines-button" href="#">more</a></div>
                                    <div class="my-listings-catalog__foto col-xl-7 col-lg-7">
                                        <div class="my-listings-catalog__block-icon"><img class="my-listings-catalog__icon" src="<?php echo get_template_directory_uri()?>/assets/img/my-listings-catalog-2.png" style="width: 100%;" alt="" role="presentation" /></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3">
                            <div class="wrapper-sidebar">
                                <div class="block-user-info-request">
                                    <img class="block-user-info-request__img" src="<?php the_sub_field('image'); ?>" alt="" role="presentation" />
                                    <div class="block-user-info-request__name"><?php the_sub_field('name'); ?></div>
                                    <a class="block-user-info-request__phone" href="tel:(310) 625-1567call">
                                        <svg width="10px" height="16px" viewBox="111.031 0 289.938 512">
                                            <path d="M363.71,0H148.29c-20.617,0-37.259,16.717-37.259,37.296v437.445c0,20.561,16.643,37.259,37.259,37.259h215.42c20.579,0,37.259-16.698,37.259-37.259V37.296C400.969,16.717,384.289,0,363.71,0z M202.845,22.65h106.348c2.687,0,4.87,4.011,4.87,8.974s-2.184,8.993-4.87,8.993H202.845c-2.706,0-4.851-4.03-4.851-8.993S200.139,22.65,202.845,22.65zM256.019,475.188c-13.116,0-23.788-10.672-23.788-23.807s10.672-23.77,23.788-23.77c13.079,0,23.751,10.635,23.751,23.77   S269.098,475.188,256.019,475.188z M373.058,393.674H138.961V62.932h234.096V393.674L373.058,393.674z"/>
                                        </svg>
									  <?php the_sub_field('phone'); ?></a></div>
                                <form class="form-request-block-user-info">
                                    <div class="form-request-block-user-info__title">Request a Showing</div><input class="form-request-block-user-info__input" placeholder="Name" type="text" /><input class="form-request-block-user-info__input" placeholder="Phone" type="text" /><input class="form-request-block-user-info__input"
                                                                                                                                                                                                                                                                                            placeholder="E-mail" type="text" /><a class="button button_primary button_full button_high" href="#">send request</a></form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'contact-side' ):
		  $show = get_sub_field('show_hide');
		  if ($show == "1"): ?>
              <div class="contact-side">
                  <div class="contact-side__btn button button_primary open-contact-side"><?php the_sub_field('title'); ?></div>
                  <div class="form"><input class="form__input" placeholder="First name" type="text" /><input class="form__input" placeholder="Last name" type="text" /><input class="form__input" placeholder="E-mail" type="text" /><input class="form__input" placeholder="Phone" type="text"
                      /><textarea class="form__input textarea" type="textarea" placeholder="I would liketo get more details about"></textarea><a class="button button_primary" href="#"><?php the_sub_field('button_text'); ?></a></div>
              </div>
        <?php endif;

        elseif( get_row_layout() == 'advanced-search' ):
		$show = get_sub_field('show_hide');
		if ($show == "1"): ?>
            <section class="advanced-search">
                <form class="form-search-mls">
                    <div class="advanced-search advanced-search_top">
                        <div class="container">
                            <div class="advanced-search__wrapper"><input class="advanced-search__input" placeholder="City" type="text" /><input class="advanced-search__input" placeholder="State" type="text" />
                                <div class="advanced-search__price">
                                <label for="amount">Price </label>
                                <input id="amount" readonly>
                                    <div id="slider-range"></div>
                                    <input id="min" type="hidden">
                                    <input id="max" type="hidden"></div>
                                <div class="advanced-search__rooms">
                                <select class="select-box">
                                    <option value="hide">Beds</option>
                                    <option>Beds</option>
                                    <option>Beds</option>
                                    <option>Beds</option>
                                </select>
                                <select class="select-box">
                                    <option value="hide">Baths</option>
                                    <option>Baths</option>
                                    <option>Baths</option>
                                    <option>Baths</option>
                                </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-search-mls__dropdown-search">
                        <div class="advanced-search advanced-search_bottom">
                            <div class="container">
                                <div class="advanced-search__wrapper"><select class="select-box"><option value="hide">Area</option><option>Area 1</option><option>Area 2</option><option>Area 3</option></select><select class="select-box"><option value="hide">Property Type</option><option>Property Type 1</option><option>Property Type 2</option><option>Property Type 3</option></select><input
                                        class="advanced-search__input" placeholder="County" type="text" /><select class="select-box"><option value="hide">Sub-division</option><option>Sub-division 1</option><option>Sub-division 2</option><option>Sub-division 3</option></select><input
                                        class="advanced-search__input" placeholder="Street Name" type="text" /><input class="advanced-search__input" placeholder="Street Number" type="text" /><select class="select-box"><option value="hide">School District</option><option>School District 1</option><option>School District 2</option><option>School District 3</option></select>
                                    <select
                                        class="select-box">
                                        <option value="hide">Garage</option>
                                        <option>Garage 1</option>
                                        <option>Garage 2</option>
                                        <option>Garage 3</option>
                                        </select><input class="advanced-search__input" placeholder="Square Feet" type="text" /><input class="advanced-search__input" placeholder="Lot Size" type="text" /><select class="select-box"><option value="hide">Year Built</option><option>Year Built 1</option><option>Year Built 2</option><option>Year Built 3</option></select>
                                        <select
                                            class="select-box">
                                            <option value="hide">Levels</option>
                                            <option>Levels 1</option>
                                            <option>Levels 2</option>
                                            <option>Levels 3</option>
                                            </select><select class="select-box"><option value="hide">Pool</option><option>Pool 1</option><option>Pool 2</option><option>Pool 3</option></select><select class="select-box"><option value="hide">View</option><option>View 1</option><option>View 2</option><option>View 3</option></select>
                                            <select
                                                class="select-box">
                                                <option value="hide">Architecture</option>
                                                <option>Architecture 1</option>
                                                <option>Architecture 2</option>
                                                <option>Architecture 3</option>
                                                </select>
                                </div><button class="button button_primary submit" type="submit"><?php the_sub_field('button_text'); ?>  </button></div>
                        </div>
                        <div class="form-search-mls__advanced">
                            <div class="form-search-mls__advanced-button">
                                <div class="form-search-mls__button">advanced search </div>
                            </div>
                        </div>
                    </div>
                </form>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'list-and-map' ):
		  $show = get_sub_field('show_hide');
		  if ($show == "1"): ?>
            <section class="list-and-map">
                <div class="left-block">
                    <div class="left-block__title"><?php the_sub_field('title'); ?></div>
                    <div class="left-block__sub-title"><?php the_sub_field('description'); ?></div>
                    <div class="left-block__cards row">
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                            <div class="left-block__item">
                                <div class="left-block__top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png;);">
                                    <!--+e.IMG.img(src='assets/img/' + "list-slide-record-sales-1" + '.png')-->
                                    <div class="left-block__hover"><a class="left-block__title-list" href="/mls-listings-page/">AVA Nob Hill</a>
                                        <div class="left-block__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                            Main street. Newport Beach CA 92660</div>
                                    </div>
                                </div>
                                <div class="left-block__bottom">
                                    <div class="left-block__price"><span>$1,199.00/mon</span></div>
                                    <div class="left-block__info-item">
                                        <div class="left-block__block">
                                            <div class="left-block__name">Area</div>
                                            <div class="left-block__info-block">2,100 sq. ft.</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bedrooms</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bathrooms</div>
                                            <div class="left-block__info-block">1</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Garages</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                            <div class="left-block__item">
                                <div class="left-block__top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png;);">
                                    <!--+e.IMG.img(src='assets/img/' + "list-slide-record-sales-1" + '.png')-->
                                    <div class="left-block__hover"><a class="left-block__title-list" href="/mls-listings-page/">AVA Nob Hill</a>
                                        <div class="left-block__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                            Main street. Newport Beach CA 92660</div>
                                    </div>
                                </div>
                                <div class="left-block__bottom">
                                    <div class="left-block__price"><span>$1,199.00/mon</span></div>
                                    <div class="left-block__info-item">
                                        <div class="left-block__block">
                                            <div class="left-block__name">Area</div>
                                            <div class="left-block__info-block">2,100 sq. ft.</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bedrooms</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bathrooms</div>
                                            <div class="left-block__info-block">1</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Garages</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                            <div class="left-block__item">
                                <div class="left-block__top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png;);">
                                    <!--+e.IMG.img(src='assets/img/' + "list-slide-record-sales-1" + '.png')-->
                                    <div class="left-block__hover"><a class="left-block__title-list" href="/mls-listings-page/">AVA Nob Hill</a>
                                        <div class="left-block__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                            Main street. Newport Beach CA 92660</div>
                                    </div>
                                </div>
                                <div class="left-block__bottom">
                                    <div class="left-block__price"><span>$1,199.00/mon</span></div>
                                    <div class="left-block__info-item">
                                        <div class="left-block__block">
                                            <div class="left-block__name">Area</div>
                                            <div class="left-block__info-block">2,100 sq. ft.</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bedrooms</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bathrooms</div>
                                            <div class="left-block__info-block">1</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Garages</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                            <div class="left-block__item">
                                <div class="left-block__top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png;);">
                                    <!--+e.IMG.img(src='assets/img/' + "list-slide-record-sales-1" + '.png')-->
                                    <div class="left-block__hover"><a class="left-block__title-list" href="/mls-listings-page/">AVA Nob Hill</a>
                                        <div class="left-block__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                            Main street. Newport Beach CA 92660</div>
                                    </div>
                                </div>
                                <div class="left-block__bottom">
                                    <div class="left-block__price"><span>$1,199.00/mon</span></div>
                                    <div class="left-block__info-item">
                                        <div class="left-block__block">
                                            <div class="left-block__name">Area</div>
                                            <div class="left-block__info-block">2,100 sq. ft.</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bedrooms</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bathrooms</div>
                                            <div class="left-block__info-block">1</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Garages</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                            <div class="left-block__item">
                                <div class="left-block__top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png;);">
                                    <!--+e.IMG.img(src='assets/img/' + "list-slide-record-sales-1" + '.png')-->
                                    <div class="left-block__hover"><a class="left-block__title-list" href="/mls-listings-page/">AVA Nob Hill</a>
                                        <div class="left-block__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                            Main street. Newport Beach CA 92660</div>
                                    </div>
                                </div>
                                <div class="left-block__bottom">
                                    <div class="left-block__price"><span>$1,199.00/mon</span></div>
                                    <div class="left-block__info-item">
                                        <div class="left-block__block">
                                            <div class="left-block__name">Area</div>
                                            <div class="left-block__info-block">2,100 sq. ft.</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bedrooms</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bathrooms</div>
                                            <div class="left-block__info-block">1</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Garages</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                            <div class="left-block__item">
                                <div class="left-block__top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png;);">
                                    <!--+e.IMG.img(src='assets/img/' + "list-slide-record-sales-1" + '.png')-->
                                    <div class="left-block__hover"><a class="left-block__title-list" href="/mls-listings-page/">AVA Nob Hill</a>
                                        <div class="left-block__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                            Main street. Newport Beach CA 92660</div>
                                    </div>
                                </div>
                                <div class="left-block__bottom">
                                    <div class="left-block__price"><span>$1,199.00/mon</span></div>
                                    <div class="left-block__info-item">
                                        <div class="left-block__block">
                                            <div class="left-block__name">Area</div>
                                            <div class="left-block__info-block">2,100 sq. ft.</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bedrooms</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bathrooms</div>
                                            <div class="left-block__info-block">1</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Garages</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                            <div class="left-block__item">
                                <div class="left-block__top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png;);">
                                    <!--+e.IMG.img(src='assets/img/' + "list-slide-record-sales-1" + '.png')-->
                                    <div class="left-block__hover"><a class="left-block__title-list" href="/mls-listings-page/">AVA Nob Hill</a>
                                        <div class="left-block__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                            Main street. Newport Beach CA 92660</div>
                                    </div>
                                </div>
                                <div class="left-block__bottom">
                                    <div class="left-block__price"><span>$1,199.00/mon</span></div>
                                    <div class="left-block__info-item">
                                        <div class="left-block__block">
                                            <div class="left-block__name">Area</div>
                                            <div class="left-block__info-block">2,100 sq. ft.</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bedrooms</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bathrooms</div>
                                            <div class="left-block__info-block">1</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Garages</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                            <div class="left-block__item">
                                <div class="left-block__top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png;);">
                                    <!--+e.IMG.img(src='assets/img/' + "list-slide-record-sales-1" + '.png')-->
                                    <div class="left-block__hover"><a class="left-block__title-list" href="/mls-listings-page/">AVA Nob Hill</a>
                                        <div class="left-block__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                            Main street. Newport Beach CA 92660</div>
                                    </div>
                                </div>
                                <div class="left-block__bottom">
                                    <div class="left-block__price"><span>$1,199.00/mon</span></div>
                                    <div class="left-block__info-item">
                                        <div class="left-block__block">
                                            <div class="left-block__name">Area</div>
                                            <div class="left-block__info-block">2,100 sq. ft.</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bedrooms</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bathrooms</div>
                                            <div class="left-block__info-block">1</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Garages</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="right-block">
                    <div id="listing-and-map"></div>
                    <script>
                        var mapElement = document.getElementById('listing-and-map');

                        function mapinit() {
                            var zoom = 17;
                            var mapOptions = {
                                zoom: zoom,
                                center: new google.maps.LatLng(33.606848, -117.876609),
                                scrollwheel: false
                            };
                            var map = new google.maps.Map(mapElement, mapOptions);
                            var marker = new google.maps.Marker({
                                icon: {
                                    url: 'assets/img/maps-filled-point.png',
                                    anchor: new google.maps.Point(70, 135)
                                },
                                position: {
                                    lat: 33.606675,
                                    lng: -117.876250
                                },
                                map: map
                            });
                            if (window.innerWidth >= 1024) {
                                map.panBy(150, -60);
                            }
                            if (window.innerWidth < 1024) {
                                map.panBy(-60, -40);
                            }
                            if (window.innerWidth < 768) {
                                map.panBy(30, -20);
                            }
                        }
                    </script>
                    <script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD41ju8fEBULLIEGvSODoqTUIGcX5nQxA4&amp;callback=mapinit"></script>
                </div>
            </section>
          <?php endif;

        elseif( get_row_layout() == 'rent-list-and-map' ):
		  $show = get_sub_field('show_hide');
		  if ($show == "1"): ?>
            <section class="list-and-map">
                <div class="left-block">
                    <div class="left-block__title"><?php the_sub_field('title'); ?></div>
                    <div class="left-block__sub-title"><?php the_sub_field('description'); ?></div>
                    <div class="left-block__cards row">
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                            <div class="left-block__item">
                                <div class="left-block__top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png;);">
                                    <!--+e.IMG.img(src='assets/img/' + "list-slide-record-sales-1" + '.png')-->
                                    <div class="left-block__hover"><a class="left-block__title-list" href="/mls-listings-page/">AVA Nob Hill</a>
                                        <div class="left-block__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                            Main street. Newport Beach CA 92660</div>
                                    </div>
                                </div>
                                <div class="left-block__bottom">
                                    <div class="left-block__price"><span>$1,199.00/mon</span></div>
                                    <div class="left-block__info-item">
                                        <div class="left-block__block">
                                            <div class="left-block__name">Area</div>
                                            <div class="left-block__info-block">2,100 sq. ft.</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bedrooms</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bathrooms</div>
                                            <div class="left-block__info-block">1</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Garages</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                            <div class="left-block__item">
                                <div class="left-block__top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png;);">
                                    <!--+e.IMG.img(src='assets/img/' + "list-slide-record-sales-1" + '.png')-->
                                    <div class="left-block__hover"><a class="left-block__title-list" href="/mls-listings-page/">AVA Nob Hill</a>
                                        <div class="left-block__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                            Main street. Newport Beach CA 92660</div>
                                    </div>
                                </div>
                                <div class="left-block__bottom">
                                    <div class="left-block__price"><span>$1,199.00/mon</span></div>
                                    <div class="left-block__info-item">
                                        <div class="left-block__block">
                                            <div class="left-block__name">Area</div>
                                            <div class="left-block__info-block">2,100 sq. ft.</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bedrooms</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bathrooms</div>
                                            <div class="left-block__info-block">1</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Garages</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                            <div class="left-block__item">
                                <div class="left-block__top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png;);">
                                    <!--+e.IMG.img(src='assets/img/' + "list-slide-record-sales-1" + '.png')-->
                                    <div class="left-block__hover"><a class="left-block__title-list" href="/mls-listings-page/">AVA Nob Hill</a>
                                        <div class="left-block__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                            Main street. Newport Beach CA 92660</div>
                                    </div>
                                </div>
                                <div class="left-block__bottom">
                                    <div class="left-block__price"><span>$1,199.00/mon</span></div>
                                    <div class="left-block__info-item">
                                        <div class="left-block__block">
                                            <div class="left-block__name">Area</div>
                                            <div class="left-block__info-block">2,100 sq. ft.</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bedrooms</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bathrooms</div>
                                            <div class="left-block__info-block">1</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Garages</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                            <div class="left-block__item">
                                <div class="left-block__top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png;);">
                                    <!--+e.IMG.img(src='assets/img/' + "list-slide-record-sales-1" + '.png')-->
                                    <div class="left-block__hover"><a class="left-block__title-list" href="/mls-listings-page/">AVA Nob Hill</a>
                                        <div class="left-block__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                            Main street. Newport Beach CA 92660</div>
                                    </div>
                                </div>
                                <div class="left-block__bottom">
                                    <div class="left-block__price"><span>$1,199.00/mon</span></div>
                                    <div class="left-block__info-item">
                                        <div class="left-block__block">
                                            <div class="left-block__name">Area</div>
                                            <div class="left-block__info-block">2,100 sq. ft.</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bedrooms</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bathrooms</div>
                                            <div class="left-block__info-block">1</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Garages</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                            <div class="left-block__item">
                                <div class="left-block__top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png;);">
                                    <!--+e.IMG.img(src='assets/img/' + "list-slide-record-sales-1" + '.png')-->
                                    <div class="left-block__hover"><a class="left-block__title-list" href="/mls-listings-page/">AVA Nob Hill</a>
                                        <div class="left-block__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                            Main street. Newport Beach CA 92660</div>
                                    </div>
                                </div>
                                <div class="left-block__bottom">
                                    <div class="left-block__price"><span>$1,199.00/mon</span></div>
                                    <div class="left-block__info-item">
                                        <div class="left-block__block">
                                            <div class="left-block__name">Area</div>
                                            <div class="left-block__info-block">2,100 sq. ft.</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bedrooms</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bathrooms</div>
                                            <div class="left-block__info-block">1</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Garages</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                            <div class="left-block__item">
                                <div class="left-block__top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png;);">
                                    <!--+e.IMG.img(src='assets/img/' + "list-slide-record-sales-1" + '.png')-->
                                    <div class="left-block__hover"><a class="left-block__title-list" href="/mls-listings-page/">AVA Nob Hill</a>
                                        <div class="left-block__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                            Main street. Newport Beach CA 92660</div>
                                    </div>
                                </div>
                                <div class="left-block__bottom">
                                    <div class="left-block__price"><span>$1,199.00/mon</span></div>
                                    <div class="left-block__info-item">
                                        <div class="left-block__block">
                                            <div class="left-block__name">Area</div>
                                            <div class="left-block__info-block">2,100 sq. ft.</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bedrooms</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bathrooms</div>
                                            <div class="left-block__info-block">1</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Garages</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                            <div class="left-block__item">
                                <div class="left-block__top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png;);">
                                    <!--+e.IMG.img(src='assets/img/' + "list-slide-record-sales-1" + '.png')-->
                                    <div class="left-block__hover"><a class="left-block__title-list" href="/mls-listings-page/">AVA Nob Hill</a>
                                        <div class="left-block__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                            Main street. Newport Beach CA 92660</div>
                                    </div>
                                </div>
                                <div class="left-block__bottom">
                                    <div class="left-block__price"><span>$1,199.00/mon</span></div>
                                    <div class="left-block__info-item">
                                        <div class="left-block__block">
                                            <div class="left-block__name">Area</div>
                                            <div class="left-block__info-block">2,100 sq. ft.</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bedrooms</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bathrooms</div>
                                            <div class="left-block__info-block">1</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Garages</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                            <div class="left-block__item">
                                <div class="left-block__top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png;);">
                                    <!--+e.IMG.img(src='assets/img/' + "list-slide-record-sales-1" + '.png')-->
                                    <div class="left-block__hover"><a class="left-block__title-list" href="/mls-listings-page/">AVA Nob Hill</a>
                                        <div class="left-block__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                            Main street. Newport Beach CA 92660</div>
                                    </div>
                                </div>
                                <div class="left-block__bottom">
                                    <div class="left-block__price"><span>$1,199.00/mon</span></div>
                                    <div class="left-block__info-item">
                                        <div class="left-block__block">
                                            <div class="left-block__name">Area</div>
                                            <div class="left-block__info-block">2,100 sq. ft.</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bedrooms</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Bathrooms</div>
                                            <div class="left-block__info-block">1</div>
                                        </div>
                                        <div class="left-block__block">
                                            <div class="left-block__name">Garages</div>
                                            <div class="left-block__info-block">2</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="right-block">
                    <div id="listing-and-map"></div>
                    <script>
                        var mapElement = document.getElementById('listing-and-map');

                        function mapinit() {
                            var zoom = 17;
                            var mapOptions = {
                                zoom: zoom,
                                center: new google.maps.LatLng(33.606848, -117.876609),
                                scrollwheel: false
                            };
                            var map = new google.maps.Map(mapElement, mapOptions);
                            var marker = new google.maps.Marker({
                                icon: {
                                    url: 'assets/img/maps-filled-point.png',
                                    anchor: new google.maps.Point(70, 135)
                                },
                                position: {
                                    lat: 33.606675,
                                    lng: -117.876250
                                },
                                map: map
                            });
                            if (window.innerWidth >= 1024) {
                                map.panBy(150, -60);
                            }
                            if (window.innerWidth < 1024) {
                                map.panBy(-60, -40);
                            }
                            if (window.innerWidth < 768) {
                                map.panBy(30, -20);
                            }
                        }
                    </script>
                    <script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD41ju8fEBULLIEGvSODoqTUIGcX5nQxA4&amp;callback=mapinit"></script>
                </div>
            </section>
          <?php endif;

        elseif( get_row_layout() == 'open_houses_list-and_map' ):
			$show = get_sub_field('show_hide');
			if ($show == "1"): ?>
                <section class="list-and-map">
                    <div class="left-block">
                        <div class="left-block__title"><?php the_sub_field('title'); ?></div>
                        <div class="left-block__sub-title"><?php the_sub_field('description'); ?></div>
                        <div class="left-block__cards row">
                            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                <div class="left-block__item">
                                    <div class="left-block__top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png;);">
                                        <!--+e.IMG.img(src='assets/img/' + "list-slide-record-sales-1" + '.png')-->
                                        <div class="left-block__hover"><a class="left-block__title-list" href="/mls-listings-page/">AVA Nob Hill</a>
                                            <div class="left-block__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                                Main street. Newport Beach CA 92660</div>
                                        </div>
                                    </div>
                                    <div class="left-block__bottom">
                                        <div class="left-block__price"><span>$1,199.00/mon</span></div>
                                        <div class="left-block__info-item">
                                            <div class="left-block__block">
                                                <div class="left-block__name">Area</div>
                                                <div class="left-block__info-block">2,100 sq. ft.</div>
                                            </div>
                                            <div class="left-block__block">
                                                <div class="left-block__name">Bedrooms</div>
                                                <div class="left-block__info-block">2</div>
                                            </div>
                                            <div class="left-block__block">
                                                <div class="left-block__name">Bathrooms</div>
                                                <div class="left-block__info-block">1</div>
                                            </div>
                                            <div class="left-block__block">
                                                <div class="left-block__name">Garages</div>
                                                <div class="left-block__info-block">2</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                <div class="left-block__item">
                                    <div class="left-block__top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png;);">
                                        <!--+e.IMG.img(src='assets/img/' + "list-slide-record-sales-1" + '.png')-->
                                        <div class="left-block__hover"><a class="left-block__title-list" href="/mls-listings-page/">AVA Nob Hill</a>
                                            <div class="left-block__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                                Main street. Newport Beach CA 92660</div>
                                        </div>
                                    </div>
                                    <div class="left-block__bottom">
                                        <div class="left-block__price"><span>$1,199.00/mon</span></div>
                                        <div class="left-block__info-item">
                                            <div class="left-block__block">
                                                <div class="left-block__name">Area</div>
                                                <div class="left-block__info-block">2,100 sq. ft.</div>
                                            </div>
                                            <div class="left-block__block">
                                                <div class="left-block__name">Bedrooms</div>
                                                <div class="left-block__info-block">2</div>
                                            </div>
                                            <div class="left-block__block">
                                                <div class="left-block__name">Bathrooms</div>
                                                <div class="left-block__info-block">1</div>
                                            </div>
                                            <div class="left-block__block">
                                                <div class="left-block__name">Garages</div>
                                                <div class="left-block__info-block">2</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                <div class="left-block__item">
                                    <div class="left-block__top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png;);">
                                        <!--+e.IMG.img(src='assets/img/' + "list-slide-record-sales-1" + '.png')-->
                                        <div class="left-block__hover"><a class="left-block__title-list" href="/mls-listings-page/">AVA Nob Hill</a>
                                            <div class="left-block__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                                Main street. Newport Beach CA 92660</div>
                                        </div>
                                    </div>
                                    <div class="left-block__bottom">
                                        <div class="left-block__price"><span>$1,199.00/mon</span></div>
                                        <div class="left-block__info-item">
                                            <div class="left-block__block">
                                                <div class="left-block__name">Area</div>
                                                <div class="left-block__info-block">2,100 sq. ft.</div>
                                            </div>
                                            <div class="left-block__block">
                                                <div class="left-block__name">Bedrooms</div>
                                                <div class="left-block__info-block">2</div>
                                            </div>
                                            <div class="left-block__block">
                                                <div class="left-block__name">Bathrooms</div>
                                                <div class="left-block__info-block">1</div>
                                            </div>
                                            <div class="left-block__block">
                                                <div class="left-block__name">Garages</div>
                                                <div class="left-block__info-block">2</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                <div class="left-block__item">
                                    <div class="left-block__top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png;);">
                                        <!--+e.IMG.img(src='assets/img/' + "list-slide-record-sales-1" + '.png')-->
                                        <div class="left-block__hover"><a class="left-block__title-list" href="/mls-listings-page/">AVA Nob Hill</a>
                                            <div class="left-block__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                                Main street. Newport Beach CA 92660</div>
                                        </div>
                                    </div>
                                    <div class="left-block__bottom">
                                        <div class="left-block__price"><span>$1,199.00/mon</span></div>
                                        <div class="left-block__info-item">
                                            <div class="left-block__block">
                                                <div class="left-block__name">Area</div>
                                                <div class="left-block__info-block">2,100 sq. ft.</div>
                                            </div>
                                            <div class="left-block__block">
                                                <div class="left-block__name">Bedrooms</div>
                                                <div class="left-block__info-block">2</div>
                                            </div>
                                            <div class="left-block__block">
                                                <div class="left-block__name">Bathrooms</div>
                                                <div class="left-block__info-block">1</div>
                                            </div>
                                            <div class="left-block__block">
                                                <div class="left-block__name">Garages</div>
                                                <div class="left-block__info-block">2</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                <div class="left-block__item">
                                    <div class="left-block__top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png;);">
                                        <!--+e.IMG.img(src='assets/img/' + "list-slide-record-sales-1" + '.png')-->
                                        <div class="left-block__hover"><a class="left-block__title-list" href="/mls-listings-page/">AVA Nob Hill</a>
                                            <div class="left-block__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                                Main street. Newport Beach CA 92660</div>
                                        </div>
                                    </div>
                                    <div class="left-block__bottom">
                                        <div class="left-block__price"><span>$1,199.00/mon</span></div>
                                        <div class="left-block__info-item">
                                            <div class="left-block__block">
                                                <div class="left-block__name">Area</div>
                                                <div class="left-block__info-block">2,100 sq. ft.</div>
                                            </div>
                                            <div class="left-block__block">
                                                <div class="left-block__name">Bedrooms</div>
                                                <div class="left-block__info-block">2</div>
                                            </div>
                                            <div class="left-block__block">
                                                <div class="left-block__name">Bathrooms</div>
                                                <div class="left-block__info-block">1</div>
                                            </div>
                                            <div class="left-block__block">
                                                <div class="left-block__name">Garages</div>
                                                <div class="left-block__info-block">2</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                <div class="left-block__item">
                                    <div class="left-block__top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png;);">
                                        <!--+e.IMG.img(src='assets/img/' + "list-slide-record-sales-1" + '.png')-->
                                        <div class="left-block__hover"><a class="left-block__title-list" href="/mls-listings-page/">AVA Nob Hill</a>
                                            <div class="left-block__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                                Main street. Newport Beach CA 92660</div>
                                        </div>
                                    </div>
                                    <div class="left-block__bottom">
                                        <div class="left-block__price"><span>$1,199.00/mon</span></div>
                                        <div class="left-block__info-item">
                                            <div class="left-block__block">
                                                <div class="left-block__name">Area</div>
                                                <div class="left-block__info-block">2,100 sq. ft.</div>
                                            </div>
                                            <div class="left-block__block">
                                                <div class="left-block__name">Bedrooms</div>
                                                <div class="left-block__info-block">2</div>
                                            </div>
                                            <div class="left-block__block">
                                                <div class="left-block__name">Bathrooms</div>
                                                <div class="left-block__info-block">1</div>
                                            </div>
                                            <div class="left-block__block">
                                                <div class="left-block__name">Garages</div>
                                                <div class="left-block__info-block">2</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                <div class="left-block__item">
                                    <div class="left-block__top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png;);">
                                        <!--+e.IMG.img(src='assets/img/' + "list-slide-record-sales-1" + '.png')-->
                                        <div class="left-block__hover"><a class="left-block__title-list" href="/mls-listings-page/">AVA Nob Hill</a>
                                            <div class="left-block__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                                Main street. Newport Beach CA 92660</div>
                                        </div>
                                    </div>
                                    <div class="left-block__bottom">
                                        <div class="left-block__price"><span>$1,199.00/mon</span></div>
                                        <div class="left-block__info-item">
                                            <div class="left-block__block">
                                                <div class="left-block__name">Area</div>
                                                <div class="left-block__info-block">2,100 sq. ft.</div>
                                            </div>
                                            <div class="left-block__block">
                                                <div class="left-block__name">Bedrooms</div>
                                                <div class="left-block__info-block">2</div>
                                            </div>
                                            <div class="left-block__block">
                                                <div class="left-block__name">Bathrooms</div>
                                                <div class="left-block__info-block">1</div>
                                            </div>
                                            <div class="left-block__block">
                                                <div class="left-block__name">Garages</div>
                                                <div class="left-block__info-block">2</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                <div class="left-block__item">
                                    <div class="left-block__top" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/list-slide-record-sales-1.png;);">
                                        <!--+e.IMG.img(src='assets/img/' + "list-slide-record-sales-1" + '.png')-->
                                        <div class="left-block__hover"><a class="left-block__title-list" href="/mls-listings-page/">AVA Nob Hill</a>
                                            <div class="left-block__location"> <svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>123
                                                Main street. Newport Beach CA 92660</div>
                                        </div>
                                    </div>
                                    <div class="left-block__bottom">
                                        <div class="left-block__price"><span>$1,199.00/mon</span></div>
                                        <div class="left-block__info-item">
                                            <div class="left-block__block">
                                                <div class="left-block__name">Area</div>
                                                <div class="left-block__info-block">2,100 sq. ft.</div>
                                            </div>
                                            <div class="left-block__block">
                                                <div class="left-block__name">Bedrooms</div>
                                                <div class="left-block__info-block">2</div>
                                            </div>
                                            <div class="left-block__block">
                                                <div class="left-block__name">Bathrooms</div>
                                                <div class="left-block__info-block">1</div>
                                            </div>
                                            <div class="left-block__block">
                                                <div class="left-block__name">Garages</div>
                                                <div class="left-block__info-block">2</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="right-block">
                        <div id="listing-and-map"></div>
                        <script>
                            var mapElement = document.getElementById('listing-and-map');

                            function mapinit() {
                                var zoom = 17;
                                var mapOptions = {
                                    zoom: zoom,
                                    center: new google.maps.LatLng(33.606848, -117.876609),
                                    scrollwheel: false
                                };
                                var map = new google.maps.Map(mapElement, mapOptions);
                                var marker = new google.maps.Marker({
                                    icon: {
                                        url: 'assets/img/maps-filled-point.png',
                                        anchor: new google.maps.Point(70, 135)
                                    },
                                    position: {
                                        lat: 33.606675,
                                        lng: -117.876250
                                    },
                                    map: map
                                });
                                if (window.innerWidth >= 1024) {
                                    map.panBy(150, -60);
                                }
                                if (window.innerWidth < 1024) {
                                    map.panBy(-60, -40);
                                }
                                if (window.innerWidth < 768) {
                                    map.panBy(30, -20);
                                }
                            }
                        </script>
                        <script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD41ju8fEBULLIEGvSODoqTUIGcX5nQxA4&amp;callback=mapinit"></script>
                    </div>
                </section>
			<?php endif;

        elseif( get_row_layout() == 'mls_list_page' ):
		$show = get_sub_field('show_hide');
		if ($show == "1"): ?>
            <section class="section-header-information-mls" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/section-block-1.jpg);">
                <div class="container">
                    <div class="row">
                        <div class="section-header-information-mls__wrapper col-xl-3 offset-xl-9 col-lg-4 offset-lg-8 col-md-12 col-sm-12">
                            <div class="block-info-mls">
                                <div class="block-info-mls__location"><svg width="9px" height="12px" viewbox="84.389 90 443.222 611.939"><path d="M306,90C183.839,90,84.389,189.45,84.389,311.611c0,151.685,198.302,374.372,206.789,383.696c7.889,8.845,21.754,8.845,29.644,0c8.486-9.443,206.789-232.011,206.789-383.696C527.611,189.45,428.161,90,306,90z M306,423.134c-61.439,0-111.522-50.084-111.522-111.523S244.561,200.088,306,200.088c61.439,0,111.522,50.083,111.522,111.522  S367.439,423.134,306,423.134z"></path></svg>Address</div>
                                <div
                                        class="block-info-mls__address">7843 Durham Avenue, MD</div>
                                <div class="block-info-mls__price"><span>$ 1, 199, 255</span></div>
                                <div class="block-info-mls__info-item">
                                    <div class="block-info-mls__block">
                                        <div class="block-info-mls__name">Area</div>
                                        <div class="block-info-mls__info-block">2,100 sq. ft.</div>
                                    </div>
                                    <div class="block-info-mls__block">
                                        <div class="block-info-mls__name">Bedrooms</div>
                                        <div class="block-info-mls__info-block">2</div>
                                    </div>
                                    <div class="block-info-mls__block">
                                        <div class="block-info-mls__name">Bathrooms</div>
                                        <div class="block-info-mls__info-block">1</div>
                                    </div>
                                    <div class="block-info-mls__block">
                                        <div class="block-info-mls__name">Garages</div>
                                        <div class="block-info-mls__info-block">2</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="nav-arrows"><a class="nav-arrows__left" href="#"><svg enable-background="new 109.61 90 392.779 612" viewBox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="415.61,90 109.61,396 415.61,702 502.39,615.221 283.169,396 502.39,176.78"></polygon></svg></a>
                            <a
                                    class="nav-arrows__right" href="#"><svg enable-background="new 109.61 90 392.779 612" viewBox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="196.39,702 502.39,396 196.39,90 109.61,176.779 328.83,396 109.61,615.22"></polygon></svg></a>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section-content-information-mls">
                <div class="lines">
                    <div class="container">
                        <div class="row">
                            <div class="lines-items lines-items lines-items_gray-light">
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="section-content-information-mls__title">AVA Nob Hill</div>
                    <div class="section-content-information-mls__title-text">7843 Durham Avenue, MD</div>
                    <div class="row wrap-mls">
                        <div class="col-xl-9">
                            <div class="adccordion-block">
                                <div class="adccordion-block__header">
                                    <div class="adccordion-block__title"><svg viewbox="0 0 512 512"><path d="M501.874,482.794c-6.313-3.676-44.743-27.475-65.305-70.505c48.022-44.904,75.337-107.371,75.337-171.301    c0-132.311-115.369-240.956-255.953-240.956S0,108.678,0,240.988c0,132.31,115.369,240.956,255.953,240.956    c18.872,0,39.143-1.77,67.111-7.646c73.508,39.061,139.485,37.962,164.589,37.566c11.175-0.14,19.288,1.879,23.463-9.666    C513.942,494.455,509.762,485.51,501.874,482.794z M285.948,361.966c0,16.563-13.429,29.995-29.995,29.995    c-16.566,0-29.995-13.432-29.995-29.995V240.988c0-16.566,13.429-29.995,29.995-29.995c16.566,0,29.995,13.429,29.995,29.995    V361.966z M255.953,165.002c-16.566,0-29.995-13.431-29.995-29.995c0-16.566,13.429-29.995,29.995-29.995    c16.566,0,29.995,13.429,29.995,29.995C285.948,151.572,272.52,165.002,255.953,165.002z"></path></svg>Description</div>
                                </div>
                                <div class="adccordion-block__content">
                                    <p>Fantastic home and great location. 3 story contemporary home ready for your personal touches. Bedroom on first floor is ready for home office, man cave or mother-in-law suite with full bathroom. Large patio/outdoor space off
                                        first floor. Walk in closets in all bedrooms. New carpet 3rd floor and fresh paint throughout. New bathroom counters/cabinets. Fridge, W/D, appliances staying. Second floor has open concept living and is great for entertaining.
                                        Large windows allow for lots of sunlight. Two car garage with guest parking in driveway. Walking distance to Buffalo Bayou Park, Rent a Bike, MAX’s Wine Dive, Washington Corridor and more! Minutes to downtown make for easy
                                        work commute! Come see it today!</p>
                                </div>
                            </div>
                            <div class="adccordion-block">
                                <div class="adccordion-block__header">
                                    <div class="adccordion-block__title"> <svg viewbox="0 0 512 512"><circle cx="256" cy="190.649" r="86.154"></circle><path d="M256,512c0,0,190.657-216.045,190.657-321.342S361.298,0,256,0S65.342,85.361,65.342,190.657    S256,512,256,512z M256,59.014c72.694,0,131.635,58.93,131.635,131.635c0,72.705-58.941,131.634-131.635,131.634    c-72.695,0-131.634-58.931-131.634-131.634C124.366,117.945,183.305,59.014,256,59.014z"></path></svg>Address</div>
                                </div>
                                <div class="adccordion-block__content">
                                    <div class="table-responsive column-2">
                                        <table class="table table-bordered-none table-borderless">
                                            <tbody>
                                            <tr>
                                                <td>Address</td>
                                                <td>4410 Feagan</td>
                                            </tr>
                                            <tr>
                                                <td>State</td>
                                                <td>Texas</td>
                                            </tr>
                                            <tr>
                                                <td>County</td>
                                                <td>Harris</td>
                                            </tr>
                                            <tr>
                                                <td>City</td>
                                                <td>Houston</td>
                                            </tr>
                                            <tr>
                                                <td>Zip</td>
                                                <td>77007</td>
                                            </tr>
                                            <tr>
                                                <td>Subdivision</td>
                                                <td>Feagan Court</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="adccordion-block">
                                <div class="adccordion-block__header">
                                    <div class="adccordion-block__title"> <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 512 512"><rect xmlns="" x="110.822" y="364.605" width="401.178" height="73.144"></rect><rect xmlns="" x="110.822" y="220.537" width="401.178" height="73.143"></rect><rect xmlns="" x="110.822" y="72.035" width="401.178" height="73.143"></rect><rect xmlns="" y="366.822" width="73.143" height="73.144"></rect><rect xmlns="" y="220.537" width="73.143" height="73.143"></rect><rect xmlns="" y="72.035" width="73.143" height="73.143"></rect></svg>Additional
                                        Details</div>
                                </div>
                                <div class="adccordion-block__content">
                                    <div class="table-responsive column-2">
                                        <table class="table table-bordered-none table-borderless">
                                            <tbody>
                                            <tr>
                                                <td>Status</td>
                                                <td>4410 Feagan</td>
                                            </tr>
                                            <tr>
                                                <td>Price</td>
                                                <td>$ 387,000</td>
                                            </tr>
                                            <tr>
                                                <td>Bedrooms</td>
                                                <td>3</td>
                                            </tr>
                                            <tr>
                                                <td>Garages</td>
                                                <td>2</td>
                                            </tr>
                                            <tr>
                                                <td>Home (Sq Ft)</td>
                                                <td>2,156</td>
                                            </tr>
                                            <tr>
                                                <td>Legal Descr.</td>
                                                <td>LT 6 BLK 1 FEAGAN COURT</td>
                                            </tr>
                                            <tr>
                                                <td>Type</td>
                                                <td>Single-Family</td>
                                            </tr>
                                            <tr>
                                                <td>Lot (Sq Ft)</td>
                                                <td>1,972</td>
                                            </tr>
                                            <tr>
                                                <td>Bathrooms</td>
                                                <td>3 Full & 1 Half</td>
                                            </tr>
                                            <tr>
                                                <td>Year Built</td>
                                                <td>2000</td>
                                            </tr>
                                            <tr>
                                                <td>Listing Status</td>
                                                <td>Active</td>
                                            </tr>
                                            <tr>
                                                <td>Style</td>
                                                <td>Contemporary/Modern</td>
                                            </tr>
                                            <tr>
                                                <td>List Type</td>
                                                <td>Exclusive Right to<br>Sell/Lease</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="adccordion-block">
                                <div class="adccordion-block__header">
                                    <div class="adccordion-block__title"> <svg viewbox="0 0 512 512"><path d="M505.586,250.004c9.281-10.207,8.385-25.853-2.001-34.935L274.766,14.851   c-10.387-9.083-27.038-8.893-37.206,0.429L7.963,225.744c-10.167,9.321-10.667,24.946-1.105,34.885l5.756,5.995   c9.55,9.938,24.986,11.124,34.456,2.639l17.159-15.366v224.966c0,13.803,11.184,24.977,24.976,24.977h89.498   c13.792,0,24.976-11.174,24.976-24.977V321.477h114.155v157.386c-0.198,13.792,9.67,24.967,23.463,24.967h94.845   c13.793,0,24.977-11.175,24.977-24.977V257.064c0,0,4.74,4.152,10.587,9.291c5.836,5.129,18.095,1.016,27.376-9.201   L505.586,250.004z"></path></svg>Interior
                                        And Exterior Features</div>
                                </div>
                                <div class="adccordion-block__content">
                                    <div class="table-responsive column-2">
                                        <table class="table table-bordered-none table-borderless">
                                            <tbody>
                                            <tr>
                                                <td>Dishwasher</td>
                                                <td>Yes</td>
                                            </tr>
                                            <tr>
                                                <td>Fireplaces</td>
                                                <td>1/Gaslog Fireplace</td>
                                            </tr>
                                            <tr>
                                                <td>Heating</td>
                                                <td>Central Gas</td>
                                            </tr>
                                            <tr>
                                                <td>Microwave</td>
                                                <td>Yes</td>
                                            </tr>
                                            <tr>
                                                <td>Roof</td>
                                                <td>Composition</td>
                                            </tr>
                                            <tr>
                                                <td>Exterior</td>
                                                <td>Patio/Deck,Porch</td>
                                            </tr>
                                            <tr>
                                                <td>Disposal</td>
                                                <td>Yes</td>
                                            </tr>
                                            <tr>
                                                <td>Cooling</td>
                                                <td>Central Electric</td>
                                            </tr>
                                            <tr>
                                                <td>Icemaker</td>
                                                <td>Yes</td>
                                            </tr>
                                            <tr>
                                                <td>Interior</td>
                                                <td>2 Staircases,Breakfast Bar,Dryer Included,Fire/Smoke Alarm,High Ceiling,Prewired for Alarm System,Refrigerator Included,Washer Included</td>
                                            </tr>
                                            <tr>
                                                <td>Foundation</td>
                                                <td>Slab</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="adccordion-block">
                                <div class="adccordion-block__header">
                                    <div class="adccordion-block__title"> <svg viewbox="0 0 512 512"><path d="M403.342,131.958c7.439,0,9.535-10.059,2.829-13.098L144.322,2.762c-15.927-7.125-34.683,0-41.808,16.031    L10.83,224.375c-7.125,15.927,0,34.683,16.032,41.808l52.81,22.843c6.601,2.934,14.146-1.887,14.146-9.116v-87.283    c0-33.425,27.138-60.563,60.563-60.563h248.961V131.958L403.342,131.958z"></path><path d="M471.869,160.458H154.276c-17.708,0-32.063,14.355-32.063,32.063v224.337    c0,17.708,14.355,32.063,32.063,32.063h42.646c-1.047-6.287-1.571-12.679-1.571-19.176c0-7.334,0.733-14.459,1.991-21.479h-34.369    V300.865h300.304v107.296h-34.473c1.257,7.021,1.99,14.146,1.99,21.48c0,6.497-0.523,12.889-1.571,19.175h42.646    c17.708,0,32.063-14.354,32.063-32.063V192.521C503.932,174.813,489.577,160.458,471.869,160.458z M463.276,234.748H162.973    v-33.635h300.304V234.748z"></path><path d="M313.125,347.283c-45.476,0-82.358,36.883-82.358,82.358S267.649,512,313.125,512    c45.475,0,82.358-36.883,82.358-82.358S358.6,347.283,313.125,347.283z M336.806,462.124c-3.563,4.4-8.278,7.335-13.727,8.802    c-2.41,0.629-3.458,1.886-3.354,4.4c0.105,2.41,0,4.82,0,7.335c0,2.2-1.152,3.354-3.248,3.354c-2.619,0.104-5.239,0.104-7.858,0    c-2.305,0-3.354-1.362-3.354-3.563c0-1.781,0-3.563,0-5.344c0-3.877-0.209-4.086-3.981-4.715    c-4.819-0.733-9.534-1.886-13.936-3.981c-3.458-1.677-3.877-2.515-2.829-6.183c0.733-2.724,1.467-5.448,2.305-8.067    c0.943-3.144,1.781-3.458,4.716-1.991c4.925,2.515,10.059,3.981,15.508,4.61c3.457,0.419,6.915,0.104,10.163-1.362    c6.078-2.619,7.021-9.64,1.887-13.831c-1.781-1.467-3.668-2.515-5.764-3.354c-5.344-2.305-10.896-4.086-15.822-7.125    c-8.172-4.819-13.307-11.525-12.678-21.479c0.629-11.212,7.02-18.127,17.289-21.9c4.191-1.57,4.296-1.467,4.296-5.867    c0-1.467,0-3.039,0-4.506c0.104-3.353,0.629-3.877,3.981-3.981c1.048,0,2.096,0,3.039,0c7.125,0,7.125,0,7.125,7.126    c0,5.029,0,5.029,5.029,5.762c3.772,0.629,7.439,1.678,11.002,3.249c1.991,0.838,2.725,2.2,2.096,4.296    c-0.838,3.039-1.677,6.182-2.725,9.116c-0.942,2.829-1.886,3.248-4.61,1.99c-5.553-2.724-11.316-3.771-17.394-3.457    c-1.571,0.104-3.144,0.313-4.61,0.942c-5.239,2.306-6.183,8.173-1.677,11.735c2.306,1.781,4.82,3.144,7.545,4.297    c4.715,1.886,9.325,3.771,13.831,6.286C342.883,432.366,346.76,450.074,336.806,462.124z"></path></svg>Financial
                                        Info</div>
                                </div>
                                <div class="adccordion-block__content">
                                    <p>Financial Info</p>
                                </div>
                            </div>
                            <div class="adccordion-block">
                                <div class="adccordion-block__header">
                                    <div class="adccordion-block__title"> <svg viewbox="0 0 512 512"><path d="M288.861,354.943c-9.112,5.59-20.783,8.668-32.861,8.668c-12.079,0-23.749-3.077-32.861-8.668     L74.362,263.698c0,0-13.428-8.231-13.428,10.415c0,21.175,0,84.699,0,84.699c0,0.846,0,5.098,0,6.416     c0,47.998,87.333,99.742,195.062,99.742s195.063-51.743,195.063-99.742c0-1.318,0-5.57,0-6.416c0,0,0-67.377,0-89.836     c0-14.967-9.547-7.653-9.547-7.653L288.861,354.943z"></path><path d="M503.426,191.119c11.432-7.011,11.432-18.483,0-25.496L276.784,52.288     c-11.432-7.011-30.138-7.011-41.57,0L8.574,165.623c-11.432,7.011-11.432,18.483,0,25.496l226.641,139     c11.432,7.012,30.138,7.012,41.57,0"></path><path d="M494.053,397.15c0-38.379,0-153.515,0-153.515s0.106-7.271-4.208-4.87     c-3.461,1.926-11.944,6.655-14.934,9.255c-3.451,2.999-2.673,9.731-2.673,9.731s0,104.549,0,139.398     c0,1.979-1.7,2.924-2.512,3.401c-7.834,4.605-13.103,13.107-13.103,22.854c0,14.647,11.873,26.521,26.522,26.521     c14.647,0,26.521-11.874,26.521-26.521c0-9.784-5.307-18.313-13.191-22.907C495.691,400.042,494.053,399.128,494.053,397.15z"></path></svg>Schools</div>
                                </div>
                                <div class="adccordion-block__content">
                                    <div class="table-responsive">
                                        <table class="table table-bordered-none table-borderless">
                                            <tbody>
                                            <tr>
                                                <td>Elementary</td>
                                                <td>MEMORIAL ELEMENTARY SCHOOL (HOUSTON)</td>
                                            </tr>
                                            <tr>
                                                <td>Middle</td>
                                                <td>HOGG MIDDLE SCHOOL (HOUSTON)</td>
                                            </tr>
                                            <tr>
                                                <td>High School</td>
                                                <td>HEIGHTS HIGH SCHOOL</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="adccordion-block">
                                <div class="adccordion-block__header">
                                    <div class="adccordion-block__title"> <svg viewbox="0 0 512 512"><path d="M472.928,34.752c-4.416-3.008-10.016-3.552-14.943-1.6c-1.024,0.416-106.881,42.048-195.168,0.384    C186.72-2.432,102.912,14.4,64,25.76V16c0-8.832-7.168-16-16-16S32,7.168,32,16v32v256v192c0,8.832,7.168,16,16,16s16-7.168,16-16    V315.328c28.384-9.184,112.608-31.137,185.184,3.136c34.591,16.353,70.784,21.792,103.648,21.792    c63.2,0,114.016-20.128,117.184-21.408c6.017-2.464,9.984-8.32,9.984-14.848V48C480,42.688,477.344,37.728,472.928,34.752z"></path></svg>Location
                                        On Map</div>
                                </div>
                                <div class="adccordion-block__content">
                                    <div class="map" id="map"></div>
                                    <script>
                                        var mapElement = document.getElementById('map');

                                        function mapinit() {
                                            var zoom = 17;
                                            var mapOptions = {
                                                zoom: zoom,
                                                center: new google.maps.LatLng(33.606848, -117.876609),
                                                scrollwheel: false
                                            };
                                            var map = new google.maps.Map(mapElement, mapOptions);
                                            var marker = new google.maps.Marker({
                                                icon: {
                                                    url: 'assets/img/maps-filled-point.png',
                                                    anchor: new google.maps.Point(70, 135)
                                                },
                                                position: {
                                                    lat: 33.606675,
                                                    lng: -117.876250
                                                },
                                                map: map
                                            });
                                            if (window.innerWidth >= 1024) {
                                                map.panBy(150, -60);
                                            }
                                            if (window.innerWidth < 1024) {
                                                map.panBy(-60, -40);
                                            }
                                            if (window.innerWidth < 768) {
                                                map.panBy(30, -20);
                                            }
                                        }
                                    </script>
                                    <script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD41ju8fEBULLIEGvSODoqTUIGcX5nQxA4&amp;callback=mapinit"></script>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3">
                            <div class="block-user-info"><img class="block-user-info__img" src="<?php echo get_template_directory_uri()?>/assets/img/blog-list-user.png" alt="" role="presentation" />
                                <div class="block-user-info__name">Kristina Morales</div><a class="block-user-info__phone" href="tel:(310) 625-1567call"> <svg width="10px" height="16px" viewBox="111.031 0 289.938 512">
                                        <path d="M363.71,0H148.29c-20.617,0-37.259,16.717-37.259,37.296v437.445c0,20.561,16.643,37.259,37.259,37.259h215.42c20.579,0,37.259-16.698,37.259-37.259V37.296C400.969,16.717,384.289,0,363.71,0z M202.845,22.65h106.348c2.687,0,4.87,4.011,4.87,8.974s-2.184,8.993-4.87,8.993H202.845c-2.706,0-4.851-4.03-4.851-8.993S200.139,22.65,202.845,22.65zM256.019,475.188c-13.116,0-23.788-10.672-23.788-23.807s10.672-23.77,23.788-23.77c13.079,0,23.751,10.635,23.751,23.77   S269.098,475.188,256.019,475.188z M373.058,393.674H138.961V62.932h234.096V393.674L373.058,393.674z"/>
                                    </svg>(310) 625-1567</a><a class="button button_primary button_full button_high" href="#">contact us</a></div>
                            <form class="form-find-property">
                                <div class="form-find-property__title">Find property</div><input class="form-find-property__input" placeholder="Address, City or Zip code" type="text" /><select class="select-box"><option value="hide">All Status</option><option>All Status</option><option>All Status</option></select>
                                <select
                                        class="select-box">
                                    <option value="hide">All Property Types</option>
                                    <option>All Property Types</option>
                                    <option>All Property Types</option>
                                </select><select class="select-box"><option value="hide">Min Bedrooms</option><option>Min Bedrooms</option><option>Min Bedrooms</option></select><select class="select-box"><option value="hide">Min Bathrooms</option><option>Min Bathrooms</option><option>Min Bathrooms</option></select>
                                <select
                                        class="select-box">
                                    <option value="hide">Min Price</option>
                                    <option>Min Price</option>
                                    <option>Min Price</option>
                                </select><select class="select-box"><option value="hide">Max Price</option><option>Max Price</option><option>Max Price</option></select></form>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif;

        elseif( get_row_layout() == 'my_listings_page' ):
		$show = get_sub_field('show_hide');
		if ($show == "1"): ?>
            <section class="section-information" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/bg-main.png);">
                <div class="lines">
                    <div class="container">
                        <div class="row">
                            <div class="lines-items lines-items lines-items_white-light">
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="section-information__title">AVA Nob Hill</div>
                    <div class="section-information__address">123 Main street. Newport Beach CA 92660</div>
                    <div class="section-information__info"> <span>3 BD</span> | <span>3 BT</span> | <span>2,860 sq. ft.</span> | <span>$15,950,000</span></div>
                    <div class="button button_video click-modal-video"><svg width="15px" height="15px" viewbox="26.911 0 458.178 512"><path d="M457.726,208.497L108.994,7.181c-16.619-9.586-38.135-9.56-54.693,0   c-16.889,9.726-27.39,27.896-27.39,47.408v402.658c0,19.521,10.492,37.69,27.286,47.364c8.305,4.836,17.795,7.39,27.442,7.39   c9.621,0,19.103-2.545,27.373-7.347L457.743,303.32c16.871-9.752,27.346-27.913,27.346-47.398   C485.081,236.462,474.614,218.301,457.726,208.497z M431.582,258.057L82.824,459.398c-0.715,0.41-1.569,0.453-2.432-0.043   c-0.732-0.428-1.194-1.238-1.194-2.109V54.58c0-0.872,0.453-1.673,1.22-2.118c0.366-0.218,0.784-0.331,1.211-0.331   c0.444,0,0.862,0.113,1.237,0.331l348.671,201.29c0.775,0.453,1.264,1.281,1.264,2.17   C432.793,256.819,432.34,257.612,431.582,258.057z"></path></svg>watch
                        video</div>
                </div>
            </section>
            <section class="interior">
                <div class="lines">
                    <div class="container">
                        <div class="row">
                            <div class="lines-items lines-items lines-items_gray-light">
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="block-title">
                        <div class="block-title__title">Interior</div>
                        <div class="block-title__title-text">kristina morales</div>
                    </div>
                    <div class="list-slider-rooms tabs-js">
                        <div class="list-slider-rooms__tabs js-tabs-items">
                            <div class="list-slider-rooms__tabs-items js-tabs-item active"><span>Kitchen</span></div>
                            <div class="list-slider-rooms__tabs-items js-tabs-item"> <span>Bathrooms</span></div>
                            <div class="list-slider-rooms__tabs-items js-tabs-item"> <span>Bedrooms</span></div>
                            <div class="list-slider-rooms__tabs-items js-tabs-item"><span>Living Spaces </span></div>
                            <div class="list-slider-rooms__tabs-items js-tabs-item"><span>Entertainment</span></div>
                            <div class="list-slider-rooms__tabs-items js-tabs-item"><span>Recreation</span></div>
                        </div>
                        <div class="list-slider-rooms__content js-content">
                            <div class="list-slider-rooms__tabs-content js-tabs-content active">
                                <div class="col-xl-7 col-lg-6 col-md-6">
                                    <div class="slider-tabs-rooms">
                                    <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider.png" alt="" role="presentation" />
                                    <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider.png" alt="" role="presentation" />
                                    <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider.png" alt="" role="presentation" /></div>
                                </div>
                                <div class="col-xl-5 col-lg-6 col-md-6">
                                    <div class="list-slider-rooms__block-info">
                                        <div class="list-slider-rooms__title-tabs">Kitchen</div>
                                        <div class="list-slider-rooms__text">
                                            <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of
                                                the truth, the master-builder of human happiness. </p>
                                            <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful</p>
                                        </div>
                                        <div class="list-slider-rooms__nav-tabs">
                                            <div class="list-slider-rooms__left prev-tabs">
                                                <div class="nav-arrows__left"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="415.61,90 109.61,396 415.61,702 502.39,615.221 283.169,396 502.39,176.78"></polygon></svg></div>prev</div>
                                            <div
                                                class="list-slider-rooms__right next-tabs">next
                                                <div class="nav-arrows__right"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="196.39,702 502.39,396 196.39,90 109.61,176.779 328.83,396 109.61,615.22"></polygon></svg></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list-slider-rooms__tabs-content js-tabs-content">
                            <div class="col-xl-7 col-lg-6 col-md-6">
                                <div class="slider-tabs-rooms">
                                <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider.png" alt="" role="presentation" />
                                <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider.png" alt="" role="presentation" />
                                <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider.png" alt="" role="presentation" /></div>
                            </div>
                            <div class="col-xl-5 col-lg-6 col-md-6">
                                <div class="list-slider-rooms__block-info">
                                    <div class="list-slider-rooms__title-tabs">Bathrooms</div>
                                    <div class="list-slider-rooms__text">
                                        <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the
                                            truth, the master-builder of human happiness. </p>
                                        <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful</p>
                                    </div>
                                    <div class="list-slider-rooms__nav-tabs">
                                        <div class="list-slider-rooms__left prev-tabs">
                                            <div class="nav-arrows__left"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="415.61,90 109.61,396 415.61,702 502.39,615.221 283.169,396 502.39,176.78"></polygon></svg></div>prev</div>
                                        <div
                                            class="list-slider-rooms__right next-tabs">next
                                            <div class="nav-arrows__right"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="196.39,702 502.39,396 196.39,90 109.61,176.779 328.83,396 109.61,615.22"></polygon></svg></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="list-slider-rooms__tabs-content js-tabs-content">
                        <div class="col-xl-7 col-lg-6 col-md-6">
                            <div class="slider-tabs-rooms">
                            <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider.png" alt="" role="presentation" />
                            <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider.png" alt="" role="presentation" />
                            <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider.png" alt="" role="presentation" /></div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-6">
                            <div class="list-slider-rooms__block-info">
                                <div class="list-slider-rooms__title-tabs">Bedrooms</div>
                                <div class="list-slider-rooms__text">
                                    <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth,
                                        the master-builder of human happiness. </p>
                                    <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful</p>
                                </div>
                                <div class="list-slider-rooms__nav-tabs">
                                    <div class="list-slider-rooms__left prev-tabs">
                                        <div class="nav-arrows__left"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="415.61,90 109.61,396 415.61,702 502.39,615.221 283.169,396 502.39,176.78"></polygon></svg></div>prev</div>
                                    <div
                                        class="list-slider-rooms__right next-tabs">next
                                        <div class="nav-arrows__right"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="196.39,702 502.39,396 196.39,90 109.61,176.779 328.83,396 109.61,615.22"></polygon></svg></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="list-slider-rooms__tabs-content js-tabs-content">
                    <div class="col-xl-7 col-lg-6 col-md-6">
                        <div class="slider-tabs-rooms">
                        <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider.png" alt="" role="presentation" />
                        <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider.png" alt="" role="presentation" />
                        <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider.png" alt="" role="presentation" /></div>
                    </div>
                    <div class="col-xl-5 col-lg-6 col-md-6">
                        <div class="list-slider-rooms__block-info">
                            <div class="list-slider-rooms__title-tabs">Living Spaces</div>
                            <div class="list-slider-rooms__text">
                                <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the
                                    master-builder of human happiness. </p>
                                <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful</p>
                            </div>
                            <div class="list-slider-rooms__nav-tabs">
                                <div class="list-slider-rooms__left prev-tabs">
                                    <div class="nav-arrows__left"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="415.61,90 109.61,396 415.61,702 502.39,615.221 283.169,396 502.39,176.78"></polygon></svg></div>prev</div>
                                <div
                                    class="list-slider-rooms__right next-tabs">next
                                    <div class="nav-arrows__right"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="196.39,702 502.39,396 196.39,90 109.61,176.779 328.83,396 109.61,615.22"></polygon></svg></div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="list-slider-rooms__tabs-content js-tabs-content">
                    <div class="col-xl-7 col-lg-6 col-md-6">
                        <div class="slider-tabs-rooms">
                        <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider.png" alt="" role="presentation" />
                        <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider.png" alt="" role="presentation" />
                        <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider.png" alt="" role="presentation" /></div>
                    </div>
                    <div class="col-xl-5 col-lg-6 col-md-6">
                        <div class="list-slider-rooms__block-info">
                            <div class="list-slider-rooms__title-tabs">Entertainment</div>
                            <div class="list-slider-rooms__text">
                                <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the
                                    master-builder of human happiness. </p>
                                <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful</p>
                            </div>
                            <div class="list-slider-rooms__nav-tabs">
                                <div class="list-slider-rooms__left prev-tabs">
                                    <div class="nav-arrows__left"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="415.61,90 109.61,396 415.61,702 502.39,615.221 283.169,396 502.39,176.78"></polygon></svg></div>prev</div>
                                <div
                                    class="list-slider-rooms__right next-tabs">next
                                    <div class="nav-arrows__right"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="196.39,702 502.39,396 196.39,90 109.61,176.779 328.83,396 109.61,615.22"></polygon></svg></div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="list-slider-rooms__tabs-content js-tabs-content">
                    <div class="col-xl-7 col-lg-6 col-md-6">
                        <div class="slider-tabs-rooms">
                        <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider.png" alt="" role="presentation" />
                        <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider.png" alt="" role="presentation" />
                        <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider.png" alt="" role="presentation" /></div>
                    </div>
                    <div class="col-xl-5 col-lg-6 col-md-6">
                        <div class="list-slider-rooms__block-info">
                            <div class="list-slider-rooms__title-tabs">Recreation</div>
                            <div class="list-slider-rooms__text">
                                <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the
                                    master-builder of human happiness. </p>
                                <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful</p>
                            </div>
                            <div class="list-slider-rooms__nav-tabs">
                                <div class="list-slider-rooms__left prev-tabs">
                                    <div class="nav-arrows__left"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="415.61,90 109.61,396 415.61,702 502.39,615.221 283.169,396 502.39,176.78"></polygon></svg></div>prev</div>
                                <div
                                    class="list-slider-rooms__right next-tabs">next
                                    <div class="nav-arrows__right"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="196.39,702 502.39,396 196.39,90 109.61,176.779 328.83,396 109.61,615.22"></polygon></svg></div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                </div>
                </div>
                </div>
            </section>
            <section class="exterior">
                <div class="lines">
                    <div class="container">
                        <div class="row">
                            <div class="lines-items lines-items lines-items_gray-light">
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="block-title">
                        <div class="block-title__title">Exterior</div>
                        <div class="block-title__title-text" data-aos="fade-down" data-aos-duration="1500">kristina morales</div>
                    </div>
                    <div class="list-slider-rooms tabs-js">
                        <div class="list-slider-rooms__tabs js-tabs-items tabs-4">
                            <div class="list-slider-rooms__tabs-items js-tabs-item active"><span>Architecture</span></div>
                            <div class="list-slider-rooms__tabs-items js-tabs-item"> <span>Landscaping</span></div>
                            <div class="list-slider-rooms__tabs-items js-tabs-item"> <span>Recreation</span></div>
                            <div class="list-slider-rooms__tabs-items js-tabs-item"><span>Outdoor Living</span></div>
                        </div>
                        <div class="list-slider-rooms__content js-content left">
                            <div class="list-slider-rooms__tabs-content js-tabs-content active">
                                <div class="col-xl-7 col-lg-6 col-md-6">
                                    <div class="slider-tabs-rooms">
                                    <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-2.png" alt="" role="presentation" />
                                    <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-2.png" alt="" role="presentation" />
                                    <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-2.png" alt="" role="presentation" /></div>
                                </div>
                                <div class="col-xl-5 col-lg-6 col-md-6">
                                    <div class="list-slider-rooms__block-info white">
                                        <div class="list-slider-rooms__title-tabs">Architecture</div>
                                        <div class="list-slider-rooms__text">
                                            <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of
                                                the truth, the master-builder of human happiness. </p>
                                            <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful</p>
                                        </div>
                                        <div class="list-slider-rooms__nav-tabs">
                                            <div class="list-slider-rooms__left prev-tabs">
                                                <div class="nav-arrows__left"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="415.61,90 109.61,396 415.61,702 502.39,615.221 283.169,396 502.39,176.78"></polygon></svg></div>prev</div>
                                            <div
                                                class="list-slider-rooms__right next-tabs">next
                                                <div class="nav-arrows__right"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="196.39,702 502.39,396 196.39,90 109.61,176.779 328.83,396 109.61,615.22"></polygon></svg></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list-slider-rooms__tabs-content js-tabs-content">
                            <div class="col-xl-7 col-lg-6 col-md-6">
                                <div class="slider-tabs-rooms">
                                <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-2.png" alt="" role="presentation" />
                                <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-2.png" alt="" role="presentation" />
                                <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-2.png" alt="" role="presentation" /></div>
                            </div>
                            <div class="col-xl-5 col-lg-6 col-md-6">
                                <div class="list-slider-rooms__block-info white">
                                    <div class="list-slider-rooms__title-tabs">Landscaping</div>
                                    <div class="list-slider-rooms__text">
                                        <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the
                                            truth, the master-builder of human happiness. </p>
                                        <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful</p>
                                    </div>
                                    <div class="list-slider-rooms__nav-tabs">
                                        <div class="list-slider-rooms__left prev-tabs">
                                            <div class="nav-arrows__left"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="415.61,90 109.61,396 415.61,702 502.39,615.221 283.169,396 502.39,176.78"></polygon></svg></div>prev</div>
                                        <div
                                            class="list-slider-rooms__right next-tabs">next
                                            <div class="nav-arrows__right"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="196.39,702 502.39,396 196.39,90 109.61,176.779 328.83,396 109.61,615.22"></polygon></svg></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="list-slider-rooms__tabs-content js-tabs-content">
                        <div class="col-xl-7 col-lg-6 col-md-6">
                            <div class="slider-tabs-rooms">
                            <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-2.png" alt="" role="presentation" />
                            <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-2.png" alt="" role="presentation" />
                            <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-2.png" alt="" role="presentation" /></div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-6">
                            <div class="list-slider-rooms__block-info white">
                                <div class="list-slider-rooms__title-tabs">Recreation</div>
                                <div class="list-slider-rooms__text">
                                    <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth,
                                        the master-builder of human happiness. </p>
                                    <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful</p>
                                </div>
                                <div class="list-slider-rooms__nav-tabs">
                                    <div class="list-slider-rooms__left prev-tabs">
                                        <div class="nav-arrows__left"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="415.61,90 109.61,396 415.61,702 502.39,615.221 283.169,396 502.39,176.78"></polygon></svg></div>prev</div>
                                    <div
                                        class="list-slider-rooms__right next-tabs">next
                                        <div class="nav-arrows__right"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="196.39,702 502.39,396 196.39,90 109.61,176.779 328.83,396 109.61,615.22"></polygon></svg></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="list-slider-rooms__tabs-content js-tabs-content">
                    <div class="col-xl-7 col-lg-6 col-md-6">
                        <div class="slider-tabs-rooms">
                        <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-2.png" alt="" role="presentation" />
                        <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-2.png" alt="" role="presentation" />
                        <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-2.png" alt="" role="presentation" /></div>
                    </div>
                    <div class="col-xl-5 col-lg-6 col-md-6">
                        <div class="list-slider-rooms__block-info white">
                            <div class="list-slider-rooms__title-tabs">Outdoor Living</div>
                            <div class="list-slider-rooms__text">
                                <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the
                                    master-builder of human happiness. </p>
                                <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful</p>
                            </div>
                            <div class="list-slider-rooms__nav-tabs">
                                <div class="list-slider-rooms__left prev-tabs">
                                    <div class="nav-arrows__left"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="415.61,90 109.61,396 415.61,702 502.39,615.221 283.169,396 502.39,176.78"></polygon></svg></div>prev</div>
                                <div
                                    class="list-slider-rooms__right next-tabs">next
                                    <div class="nav-arrows__right"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="196.39,702 502.39,396 196.39,90 109.61,176.779 328.83,396 109.61,615.22"></polygon></svg></div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                </div>
                </div>
                </div>
            </section>
            <section class="property-highlights">
                <div class="lines">
                    <div class="container">
                        <div class="row">
                            <div class="lines-items lines-items lines-items_gray-light">
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                                <div class="lines-items__item"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="block-title">
                        <div class="block-title__title">Property Highlights</div>
                        <div class="block-title__title-text">kristina morales</div>
                    </div>
                    <div class="list-slider-rooms tabs-js">
                        <div class="list-slider-rooms__tabs js-tabs-items width-auto">
                            <div class="list-slider-rooms__tabs-items js-tabs-item active"><span>Infinity Pool</span></div>
                            <div class="list-slider-rooms__tabs-items js-tabs-item"> <span>Theater</span></div>
                            <div class="list-slider-rooms__tabs-items js-tabs-item"> <span>Elevator</span></div>
                            <div class="list-slider-rooms__tabs-items js-tabs-item"><span>Tennis Court</span></div>
                            <div class="list-slider-rooms__tabs-items js-tabs-item"><span>Private Library</span></div>
                            <div class="list-slider-rooms__tabs-items js-tabs-item"><span>Custom Imported Flooring</span></div>
                        </div>
                        <div class="list-slider-rooms__content js-content">
                            <div class="list-slider-rooms__tabs-content js-tabs-content active">
                                <div class="col-xl-7 col-lg-6 col-md-6">
                                    <div class="slider-tabs-rooms">
                                    <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-3.png" alt="" role="presentation" />
                                    <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-3.png" alt="" role="presentation" />
                                    <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-3.png" alt="" role="presentation" /></div>
                                </div>
                                <div class="col-xl-5 col-lg-6 col-md-6">
                                    <div class="list-slider-rooms__block-info">
                                        <div class="list-slider-rooms__title-tabs">Infinity Pool</div>
                                        <div class="list-slider-rooms__text">
                                            <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of
                                                the truth, the master-builder of human happiness. </p>
                                            <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful</p>
                                            <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful</p>
                                        </div>
                                        <div class="list-slider-rooms__nav-tabs">
                                            <div class="list-slider-rooms__left prev-tabs">
                                                <div class="nav-arrows__left"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="415.61,90 109.61,396 415.61,702 502.39,615.221 283.169,396 502.39,176.78"></polygon></svg></div>prev</div>
                                            <div
                                                class="list-slider-rooms__right next-tabs-1">next
                                                <div class="nav-arrows__right"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="196.39,702 502.39,396 196.39,90 109.61,176.779 328.83,396 109.61,615.22"></polygon></svg></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list-slider-rooms__tabs-content js-tabs-content">
                            <div class="col-xl-7 col-lg-6 col-md-6">
                                <div class="slider-tabs-rooms">
                                <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-3.png" alt="" role="presentation" />
                                <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-3.png" alt="" role="presentation" />
                                <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-3.png" alt="" role="presentation" /></div>
                            </div>
                            <div class="col-xl-5 col-lg-6 col-md-6">
                                <div class="list-slider-rooms__block-info">
                                    <div class="list-slider-rooms__title-tabs">Theater</div>
                                    <div class="list-slider-rooms__text">
                                        <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the
                                            truth, the master-builder of human happiness. </p>
                                        <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful</p>
                                        <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful</p>
                                    </div>
                                    <div class="list-slider-rooms__nav-tabs">
                                        <div class="list-slider-rooms__left prev-tabs">
                                            <div class="nav-arrows__left"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="415.61,90 109.61,396 415.61,702 502.39,615.221 283.169,396 502.39,176.78"></polygon></svg></div>prev</div>
                                        <div
                                            class="list-slider-rooms__right next-tabs-1">next
                                            <div class="nav-arrows__right"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="196.39,702 502.39,396 196.39,90 109.61,176.779 328.83,396 109.61,615.22"></polygon></svg></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="list-slider-rooms__tabs-content js-tabs-content">
                        <div class="col-xl-7 col-lg-6 col-md-6">
                            <div class="slider-tabs-rooms">
                            <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-3.png" alt="" role="presentation" />
                            <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-3.png" alt="" role="presentation" />
                            <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-3.png" alt="" role="presentation" /></div>
                        </div>
                        <div class="col-xl-5 col-lg-6 col-md-6">
                            <div class="list-slider-rooms__block-info">
                                <div class="list-slider-rooms__title-tabs">Elevator</div>
                                <div class="list-slider-rooms__text">
                                    <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth,
                                        the master-builder of human happiness. </p>
                                    <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful</p>
                                    <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful</p>
                                </div>
                                <div class="list-slider-rooms__nav-tabs">
                                    <div class="list-slider-rooms__left prev-tabs">
                                        <div class="nav-arrows__left"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="415.61,90 109.61,396 415.61,702 502.39,615.221 283.169,396 502.39,176.78"></polygon></svg></div>prev</div>
                                    <div
                                        class="list-slider-rooms__right next-tabs-1">next
                                        <div class="nav-arrows__right"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="196.39,702 502.39,396 196.39,90 109.61,176.779 328.83,396 109.61,615.22"></polygon></svg></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="list-slider-rooms__tabs-content js-tabs-content">
                    <div class="col-xl-7 col-lg-6 col-md-6">
                        <div class="slider-tabs-rooms">
                        <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-3.png" alt="" role="presentation" />
                        <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-3.png" alt="" role="presentation" />
                        <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-3.png" alt="" role="presentation" /></div>
                    </div>
                    <div class="col-xl-5 col-lg-6 col-md-6">
                        <div class="list-slider-rooms__block-info">
                            <div class="list-slider-rooms__title-tabs">Tennis Court</div>
                            <div class="list-slider-rooms__text">
                                <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the
                                    master-builder of human happiness. </p>
                                <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful</p>
                                <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful</p>
                            </div>
                            <div class="list-slider-rooms__nav-tabs">
                                <div class="list-slider-rooms__left prev-tabs">
                                    <div class="nav-arrows__left"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="415.61,90 109.61,396 415.61,702 502.39,615.221 283.169,396 502.39,176.78"></polygon></svg></div>prev</div>
                                <div
                                    class="list-slider-rooms__right next-tabs-1">next
                                    <div class="nav-arrows__right"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="196.39,702 502.39,396 196.39,90 109.61,176.779 328.83,396 109.61,615.22"></polygon></svg></div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="list-slider-rooms__tabs-content js-tabs-content">
                    <div class="col-xl-7 col-lg-6 col-md-6">
                        <div class="slider-tabs-rooms">
                        <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-3.png" alt="" role="presentation" />
                        <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-3.png" alt="" role="presentation" />
                        <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-3.png" alt="" role="presentation" /></div>
                    </div>
                    <div class="col-xl-5 col-lg-6 col-md-6">
                        <div class="list-slider-rooms__block-info">
                            <div class="list-slider-rooms__title-tabs">Private Library</div>
                            <div class="list-slider-rooms__text">
                                <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the
                                    master-builder of human happiness. </p>
                                <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful</p>
                                <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful</p>
                            </div>
                            <div class="list-slider-rooms__nav-tabs">
                                <div class="list-slider-rooms__left prev-tabs">
                                    <div class="nav-arrows__left"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="415.61,90 109.61,396 415.61,702 502.39,615.221 283.169,396 502.39,176.78"></polygon></svg></div>prev</div>
                                <div
                                    class="list-slider-rooms__right next-tabs-1">next
                                    <div class="nav-arrows__right"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="196.39,702 502.39,396 196.39,90 109.61,176.779 328.83,396 109.61,615.22"></polygon></svg></div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="list-slider-rooms__tabs-content js-tabs-content">
                    <div class="col-xl-7 col-lg-6 col-md-6">
                        <div class="slider-tabs-rooms">
                        <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-3.png" alt="" role="presentation" />
                        <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-3.png" alt="" role="presentation" />
                        <img class="slider-tabs-rooms__img" src="<?php echo get_template_directory_uri()?>/assets/img/list-interior-slider-3.png" alt="" role="presentation" /></div>
                    </div>
                    <div class="col-xl-5 col-lg-6 col-md-6">
                        <div class="list-slider-rooms__block-info">
                            <div class="list-slider-rooms__title-tabs">Custom Imported Flooring</div>
                            <div class="list-slider-rooms__text">
                                <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the
                                    master-builder of human happiness. </p>
                                <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful</p>
                                <p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful</p>
                            </div>
                            <div class="list-slider-rooms__nav-tabs">
                                <div class="list-slider-rooms__left prev-tabs">
                                    <div class="nav-arrows__left"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="415.61,90 109.61,396 415.61,702 502.39,615.221 283.169,396 502.39,176.78"></polygon></svg></div>prev</div>
                                <div
                                    class="list-slider-rooms__right next-tabs-1">next
                                    <div class="nav-arrows__right"><svg enable-background="new 109.61 90 392.779 612" viewbox="109.61 90 392.779 612" height="9px" width="6px"><polygon points="196.39,702 502.39,396 196.39,90 109.61,176.779 328.83,396 109.61,615.22"></polygon></svg></div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                </div>
                </div>
                </div>
            </section>
       <?php endif;
        endif;
    endwhile;
  endif;
?>