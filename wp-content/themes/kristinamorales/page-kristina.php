<?php
/*
 * Template Name: Kristina Motales
 */
?>

<?php get_header(); ?>
<body>

<!-- Posts Area -->
<main class="posts_area">
  <section class="breadcrumbs">
	<div class="lines">
	  <div class="container">
		<div class="row">
		  <div class="lines-items lines-items lines-items_gray-light">
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
		  </div>
		</div>
	  </div>
	</div>
	<div class="container">
	  <div class="breadcrumbs__wrapper">
		<div class="breadcrumbs__title-block">
		  <div class="breadcrumbs__title" data-aos="fade-down" data-aos-delay="800"><?php breadcrumbs(); ?></div>
		  <div class="breadcrumbs__sub-title" data-aos="fade-up" data-aos-delay="300">luxury real estate realtor</div>
		</div>
		<div class="breadcrumbs__nav">
		  <div class="breadcrumbs__item"><a href="<?php echo site_url(); ?>">home</a></div>
		  <div class="breadcrumbs__item"> <span><?php breadcrumbs(); ?></span></div>
		</div>
	  </div>
	</div>
  </section>
  <section class="block-watch-video" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/bg-main.png;);">
	<div class="button button_video click-modal-video" data-aos="flip-left" data-aos-delay="2000"><svg width="15px" height="15px" viewbox="26.911 0 458.178 512"><path d="M457.726,208.497L108.994,7.181c-16.619-9.586-38.135-9.56-54.693,0   c-16.889,9.726-27.39,27.896-27.39,47.408v402.658c0,19.521,10.492,37.69,27.286,47.364c8.305,4.836,17.795,7.39,27.442,7.39   c9.621,0,19.103-2.545,27.373-7.347L457.743,303.32c16.871-9.752,27.346-27.913,27.346-47.398   C485.081,236.462,474.614,218.301,457.726,208.497z M431.582,258.057L82.824,459.398c-0.715,0.41-1.569,0.453-2.432-0.043   c-0.732-0.428-1.194-1.238-1.194-2.109V54.58c0-0.872,0.453-1.673,1.22-2.118c0.366-0.218,0.784-0.331,1.211-0.331   c0.444,0,0.862,0.113,1.237,0.331l348.671,201.29c0.775,0.453,1.264,1.281,1.264,2.17   C432.793,256.819,432.34,257.612,431.582,258.057z"></path></svg>watch
	  video</div>
  </section>

  <section class="about-me bg-white">
	<div class="lines">
	  <div class="container">
		<div class="row">
		  <div class="lines-items">
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
		  </div>
		</div>
	  </div>
	</div>
	<div class="container">
	  <div class="row align-items-end">
		<div class="col-xl-6" data-aos="fade-right"><img class="about-me__icon" src="<?php echo get_template_directory_uri()?>/assets/img/about-me-icon.png" alt="" role="presentation" /></div>
		<div class="col-xl-6" data-aos="fade-left">
		  <div class="about-me__title"><?php the_field('meet_title'); ?></div>
		  <div class="about-me__title-text"><?php the_field('meet_text'); ?></div>
		  <?php the_field('text_description'); ?>
            <a class="button button_primary" href="#" data-aos="flip-left" data-aos-duration="1500">contact now</a>
		</div>
	  </div>
	</div>
  </section>
  <section class="my-team">
	<div class="lines">
	  <div class="container">
		<div class="row">
		  <div class="lines-items">
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
		  </div>
		</div>
	  </div>
	</div>
	<div class="container">
	  <div class="my-team__title">My Team</div>
	  <div class="my-team__title-text">the best real estate team</div>
	  <div class="list-slide-my-team row">

		<?php
		if( have_rows('meet_repeater') ):
		  while ( have_rows('meet_repeater') ) : the_row(); ?>

              <div class="col-xl-3 col-lg-4">
                  <div class="block-user-info"><img class="block-user-info__img" src="<?php the_sub_field('image'); ?>" alt="" role="presentation" />
                      <div class="block-user-info__name"><?php the_sub_field('name'); ?></div><a class="block-user-info__phone" href="tel:<?php the_sub_field('phone'); ?>call"> <svg width="10px" height="16px" viewBox="111.031 0 289.938 512">
                              <path d="M363.71,0H148.29c-20.617,0-37.259,16.717-37.259,37.296v437.445c0,20.561,16.643,37.259,37.259,37.259h215.42c20.579,0,37.259-16.698,37.259-37.259V37.296C400.969,16.717,384.289,0,363.71,0z M202.845,22.65h106.348c2.687,0,4.87,4.011,4.87,8.974s-2.184,8.993-4.87,8.993H202.845c-2.706,0-4.851-4.03-4.851-8.993S200.139,22.65,202.845,22.65zM256.019,475.188c-13.116,0-23.788-10.672-23.788-23.807s10.672-23.77,23.788-23.77c13.079,0,23.751,10.635,23.751,23.77   S269.098,475.188,256.019,475.188z M373.058,393.674H138.961V62.932h234.096V393.674L373.058,393.674z"/>
                          </svg><?php the_sub_field('phone'); ?></a><a class="button button_primary button_full button_high" href="#">contact us</a></div>
              </div>

		  <?php  endwhile;
		else :
		endif;
		?>

	  </div>
	</div>
  </section>
  <section class="tabs-information">
	<div class="lines">
	  <div class="container">
		<div class="row">
		  <div class="lines-items lines-items lines-items_gray-light">
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
		  </div>
		</div>
	  </div>
	</div>
	<div class="container">
	  <div class="tabs-information__list tabs-js tabs-js-accordion">
		<div class="tabs-information__tabs js-tabs-items">
		  <div class="tabs-information__tabs-items js-tabs-item active"><span>Designations</span></div>
		  <div class="tabs-information__tabs-items js-tabs-item"> <span>Licenses</span></div>
		  <div class="tabs-information__tabs-items js-tabs-item"> <span>Credentials</span></div>
		</div>
		<div class="tabs-information__content">
		  <div class="tabs-information__accordion js-accordion"><span>Designations</span></div>
		  <div class="tabs-information__tabs-content js-tabs-content active">
			<div class="tabs-information__text">
			  <?php the_field('designations'); ?>
			</div>
		  </div>
		  <div class="tabs-information__accordion js-accordion"><span>Licenses</span></div>
		  <div class="tabs-information__tabs-content js-tabs-content">
			<div class="tabs-information__text">
			  <?php the_field('licenses'); ?>
			</div>
		  </div>
		  <div class="tabs-information__accordion js-accordion"><span>Credentials</span></div>
		  <div class="tabs-information__tabs-content js-tabs-content">
			<div class="tabs-information__text">
			  <?php the_field('credentials'); ?>
			</div>
		  </div>
		</div>
	  </div>
	</div>
  </section>
  <section class="testimonials bg-white">
	<div class="lines">
	  <div class="container">
		<div class="row">
		  <div class="lines-items lines-items lines-items_light small-line">
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
		  </div>
		</div>
	  </div>
	</div>
	<div class="container">
	  <div class="block-title">
		<div class="block-title__title"><?php the_field('meet_testimonials_title'); ?></div>
		<div class="block-title__title-text" data-aos="fade-down" data-aos-duration="1500"><?php the_field('meet_testimonials_text'); ?></div>
	  </div>
	  <div class="list-slide-testimonials">

		<?php
		if( have_rows('meet_testimonials_repeater') ):
		  while ( have_rows('meet_testimonials_repeater') ) : the_row(); ?>

              <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 hover">
                  <div class="list-slide-testimonials__item" data-aos="fade-left" data-aos-duration="500">
                      <div class="list-slide-testimonials__icon" style="background-image: url(<?php the_sub_field('image'); ?>);"></div>
                      <div class="list-slide-testimonials__title"><?php the_sub_field('name'); ?></div>
                      <div class="list-slide-testimonials__text"><?php the_sub_field('description'); ?></div>
                      <svg width="22px" height="20px" viewbox="0 0 612 792"><path fill="#EDC77C" d="M580.563,125.859H389.791c-17.332,0-31.437,14.104-31.437,31.437v190.772                    c0,17.332,14.104,31.437,31.437,31.437h91.322c-1.195,49.964-12.79,89.888-34.783,120.009                    c-17.332,23.668-43.51,43.391-78.652,58.93c-16.137,7.052-23.069,26.177-15.539,42.074l22.592,47.693                    c7.291,15.3,25.341,21.994,40.88,15.3c41.597-17.93,76.619-40.641,105.307-68.252c34.903-33.708,58.81-71.719,71.719-114.152                    S612,380.819,612,307.308V157.296C612,139.964,597.896,125.859,580.563,125.859z"></path><path fill="#EDC77C" d="M66.579,663.392c40.999-17.93,75.902-40.641,104.709-68.133c35.262-33.708,59.288-71.6,72.197-113.675                    c12.909-42.074,19.364-100.167,19.364-174.157V157.296c0-17.332-14.104-31.437-31.437-31.437H40.641                    c-17.212,0-31.317,14.104-31.317,31.437v190.772c0,17.332,14.105,31.437,31.437,31.437h91.322                    c-1.195,49.964-12.79,89.888-34.784,120.009c-17.332,23.668-43.509,43.391-78.651,58.93C2.51,565.495-4.423,584.62,3.108,600.518                    l22.591,47.574C32.871,663.392,51.04,670.205,66.579,663.392z"></path></svg>
                  </div>
              </div>

		  <?php  endwhile;
		else :
		endif;
		?>

	  </div><a class="button button_primary" href="#" data-aos="flip-left" data-aos-duration="1500">more reviews</a></div>
  </section>
  <section class="map-contact">
	<div class="lines">
	  <div class="container">
		<div class="row">
		  <div class="lines-items lines-items lines-items_gray-light">
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
			<div class="lines-items__item"></div>
		  </div>
		</div>
	  </div>
	</div>
	<div class="container">
	  <div class="map-contact__title"><?php the_field('meet_google_maps_title'); ?></div>
	  <div class="map-contact__wrapper row">
		<div class="col-xl-9">
		  <div class="map" id="map"></div>
		  <script>
              var mapElement = document.getElementById('map');

              function mapinit() {
                  var zoom = 17;
                  var mapOptions = {
                      zoom: zoom,
                      center: new google.maps.LatLng(33.606848, -117.876609),
                      scrollwheel: false
                  };
                  var map = new google.maps.Map(mapElement, mapOptions);
                  var marker = new google.maps.Marker({
                      icon: {
                          url: 'http://wordpress-158890-481489.cloudwaysapps.com/wp-content/themes/kristinamorales/assets/img/maps-filled-point.png',
                          anchor: new google.maps.Point(70, 135)
                      },
                      position: {
                          lat: 33.606675,
                          lng: -117.876250
                      },
                      map: map
                  });
                  if (window.innerWidth >= 1024) {
                      map.panBy(150, -60);
                  }
                  if (window.innerWidth < 1024) {
                      map.panBy(-60, -40);
                  }
                  if (window.innerWidth < 768) {
                      map.panBy(30, -20);
                  }
              }
		  </script>
		  <script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD41ju8fEBULLIEGvSODoqTUIGcX5nQxA4&amp;callback=mapinit"></script>
		</div>
		<div class="wrapper-info">
		  <div class="wrapper-info__title">Contacts</div>
		  <div class="wrapper-info__info">
			<div class="wrapper-info__item phone">
			  <div class="wrapper-info__name">Mobile:</div>
			  <div class="wrapper-info__text"><a href="tel:<?php the_field('meet_mobile'); ?>call"><?php the_field('meet_mobile'); ?></a></div>
			</div>
			<div class="wrapper-info__item email">
			  <div class="wrapper-info__name">Email:</div>
			  <div class="wrapper-info__text"> <a href="mailto:<?php the_field('meet_email'); ?>"><?php the_field('meet_email'); ?></a></div>
			</div>
			<div class="wrapper-info__item address">
			  <div class="wrapper-info__name">Office Address:</div>
			  <div class="wrapper-info__text"><?php the_field('meet_office_address'); ?></div>
			</div>
		  </div>
		  <ul class="social">
			<li class="social__items"><a class="social__link" href="#"> <svg width="17px" height="17px" viewbox="0 0 512 512"><path d="M61.538,0.44C27.531,0.44,0.005,28.015,0,61.945c0,33.962,27.526,61.532,61.543,61.532                  c33.924,0,61.488-27.57,61.488-61.532C123.032,28.01,95.462,0.44,61.538,0.44z"></path><rect x="8.462" y="170.149" , width="106.114" height="341.41"></rect><path d="M384.715,161.66c-51.618,0-86.229,28.301-100.396,55.139h-1.42v-46.65H181.125h-0.005v341.405h106.021                  V342.659c0-44.526,8.479-87.655,63.684-87.655c54.413,0,55.139,50.926,55.139,90.511v166.034H512v-187.26                  C512,232.37,492.166,161.66,384.715,161.66z"></path></svg></a></li>
			<li
			  class="social__items"><a class="social__link" href="#"><svg width="17px" height="27px" viewbox="0 0 612 792"><path d="M459,90.12L379.631,90c-89.17,0-146.784,59.168-146.784,150.609v69.448H153                  c-6.933,0-12.431,5.618-12.431,12.431v100.646c0,6.933,5.618,12.431,12.431,12.431h79.847v253.885                  c0,6.933,5.618,12.432,12.432,12.432H349.39c6.933,0,12.432-5.618,12.432-12.432V435.685h93.354                  c6.933,0,12.432-5.618,12.432-12.432V322.608c0-3.347-1.315-6.455-3.706-8.845s-5.498-3.706-8.845-3.706h-93.354v-58.81                  c0-28.329,6.694-42.672,43.629-42.672h53.431c6.933,0,12.432-5.618,12.432-12.432v-93.593C471.432,95.737,465.813,90.12,459,90.12z"></path></svg></a></li>
			<li
			  class="social__items"><a class="social__link" href="#"><svg width="17px" height="17px" viewbox="0 0 154.333 154.333"><defs><rect id="instagram1" width="154.333" height="154.333"></rect></defs><clippath id="instagram2"><use xlink:href="#instagram1" overflow="visible"></use></clippath><path clip-path="url(#instagram2)" d="M77.166,48.979c-15.542,0-28.187,12.645-28.187,28.187                  c0,15.543,12.645,28.188,28.187,28.188c15.542,0,28.188-12.645,28.188-28.188C105.354,61.624,92.708,48.979,77.166,48.979"></path><path clip-path="url(#instagram2)" d="M108.333,0H46C20.595,0,0,20.595,0,46v62.333c0,25.404,20.595,46,46,46h62.333                  c25.405,0,46-20.596,46-46V46C154.333,20.595,133.738,0,108.333,0 M77.166,119.354c-23.262,0-42.187-18.925-42.187-42.187                  c0-23.263,18.925-42.188,42.187-42.188s42.188,18.925,42.188,42.188C119.354,100.429,100.428,119.354,77.166,119.354                  M123.38,39.604c-4.694,0-8.5-3.805-8.5-8.5c0-4.694,3.806-8.5,8.5-8.5s8.5,3.806,8.5,8.5                  C131.88,35.799,128.074,39.604,123.38,39.604"></path></svg></a></li>
		  </ul>
		</div>
	  </div>
	</div>
  </section>

  <?php $case = get_field('meet_contact_select'); ?>

  <?php if ($case == "standart") { ?>

      <!-- contact Standart-->
      <section class="contact-us" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/contact-us-bg.png);">
          <div class="lines">
              <div class="container">
                  <div class="row">
                      <div class="lines-items lines-items lines-items_white-light small-line">
                          <div class="lines-items__item"></div>
                          <div class="lines-items__item"></div>
                          <div class="lines-items__item"></div>
                          <div class="lines-items__item"></div>
                          <div class="lines-items__item"></div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="container">
              <div class="contact-us__sub-title" data-aos="fade-down" data-aos-duration="1500">have any questions?</div>
              <div class="contact-us__title" data-aos="fade-up" data-aos-duration="2000"><?php the_field('standart_title',option); ?></div>
              <div class="contact-us__title-text"><?php the_field('standart_description',option); ?></div>
              <form class="form-contact row">
                  <div class="col-xl-4"><input class="form-contact__input" placeholder="Name" type="text" /></div>
                  <div class="col-xl-4"><input class="form-contact__input" placeholder="E-mail" type="text" /></div>
                  <div class="col-xl-4"><input class="form-contact__input" placeholder="Phone" type="text" /></div>
                  <div class="col-xl-12"><textarea class="form-contact__input textarea" placeholder="Enter your massage..."></textarea></div>
                  <div class="col-12"><a class="button button_primary" href="#" data-aos="flip-left" data-aos-duration="1500">contact us</a></div>
              </form>
          </div>
      </section>

  <?php } ?>

  <?php if ($case == "custom") { ?>

      <!-- contact Custom -->
      <section class="contact-us" style="background-image: url(<?php echo get_template_directory_uri()?>/assets/img/contact-us-bg.png);">
          <div class="lines">
              <div class="container">
                  <div class="row">
                      <div class="lines-items lines-items lines-items_white-light small-line">
                          <div class="lines-items__item"></div>
                          <div class="lines-items__item"></div>
                          <div class="lines-items__item"></div>
                          <div class="lines-items__item"></div>
                          <div class="lines-items__item"></div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="container">
              <div class="contact-us__sub-title" data-aos="fade-down" data-aos-duration="1500">have any questions?</div>
              <div class="contact-us__title" data-aos="fade-up" data-aos-duration="2000"><?php the_field('custom_title',option); ?></div>
              <div class="contact-us__title-text"><?php the_field('custom_description',option); ?></div>
              <form class="form-contact row">
                  <div class="col-xl-4"><input class="form-contact__input" placeholder="Name" type="text" /></div>
                  <div class="col-xl-4"><input class="form-contact__input" placeholder="E-mail" type="text" /></div>
                  <div class="col-xl-4"><input class="form-contact__input" placeholder="Phone" type="text" /></div>
                  <div class="col-xl-12"><textarea class="form-contact__input textarea" placeholder="Enter your massage..."></textarea></div>
                  <div class="col-12"><a class="button button_primary" href="#" data-aos="flip-left" data-aos-duration="1500">contact us</a></div>
              </form>
          </div>
      </section>

  <?php } ?>

  <div class="modal-call-to-action">
	<div class="modal-call-to-action__wrapper">
	  <div class="modal-call-to-action__close">c l o s e</div>
	  <div class="modal-call-to-action__title">Call to action</div>
	  <div class="modal-call-to-action__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor </div>
	  <form class="form-call-to-action"><input class="form-call-to-action__input" placeholder="First name" type="text" /><input class="form-call-to-action__input" placeholder="Phone" type="text" /><input class="form-call-to-action__input" placeholder="E-mail" type="text" /><a class="button button_primary"
																																																																					 href="#">sumbit request</a></form>
	</div>
	<div class="bg-overlay"></div>
  </div>
  <div class="modal-video">
	<div class="modal-video__wrapper">
	  <div class="modal-video__close">c l o s e</div>
	  <div class="modal-video__iframe"><iframe width="100%" height="580" src="https://www.youtube.com/embed/pMxgov6_5TA?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe></div>
	</div>
	<div class="bg-overlay-video"></div>
  </div>
  <div class="contact-side">
	<div class="contact-side__btn button button_primary open-contact-side">have us contact you</div>
	<div class="form">
	  <input class="form__input" placeholder="First name" type="text" />
	  <input class="form__input" placeholder="Last name" type="text" />
	  <input class="form__input" placeholder="E-mail" type="text" />
	  <input class="form__input" placeholder="Phone" type="text"/>
	  <textarea class="form__input textarea" type="textarea" placeholder="I would liketo get more details about"></textarea>
	  <a class="button button_primary" href="#">sumbit request</a></div>
  </div>
</main>

<!-- Footer -->
<?php get_footer(); ?>

</body>
</html>