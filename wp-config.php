<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'kristinamorales');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'v}F!V$H.:<hXAg|t%2S:#DxNO2]=ax6KRV|DUU6?Sx<DX~;:XeTHYf!]7>BN9[PE');
define('SECURE_AUTH_KEY',  '9/|K1:/jz2{eL_x 0<IXk[56;(diM;g^`ENt_lmy(-N|TO>/0!p#<gW]|/RZn2*&');
define('LOGGED_IN_KEY',    'PtCJ/a)kB3+U+wnUor%!t?:$W0N/df*Lg53O8 MyYc8&E!~X2SV4XcPBoZ+!f-vz');
define('NONCE_KEY',        '9qsI?Un&R~oOjg},`GSIjh>X9.sI(*WUKo<U6K2l}VZ!{7}6`->Hmo)?QmDfn.)6');
define('AUTH_SALT',        '^p@0.8Rq.>i5WgL:rTdzO&Q$v:U~8V^@~Uh:1h<}rRBN+IHN(e|Biz#j[[2,nW<W');
define('SECURE_AUTH_SALT', '|gLUz.cIx{!%dKX.[zD_(@w{^u#tOuxx!GTyidB!Em1l&}K7PM$dg?xEifbB6dTM');
define('LOGGED_IN_SALT',   'u%onK%p}E`_LIjm4}ywIY6V 6[0|jIJ^hU)DeWQS;@c?g$;.}{`0C7M +5asiOnx');
define('NONCE_SALT',       'Z[Fkh.pp]dhOk]KCX*8(I/w7VgW*;to`GlX+Jl;9%4y*VMjEH@~M]D}?iN=c$zGq');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
